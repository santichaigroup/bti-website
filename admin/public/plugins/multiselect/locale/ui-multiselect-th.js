/**
 * Localization strings for the UI Multiselect widget
 *
 * @locale en, en-US
 */

$.extend($.ui.multiselect.locale, {
	addAll:'เพิ่มทั้งหมด',
	removeAll:'ลบทั้งหมด',
	itemsCount:'สินค้าที่เลือก'
});
