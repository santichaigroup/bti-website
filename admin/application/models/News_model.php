<?php 
class News_model extends CI_Model {

	public function dataTable($lang_id="TH", $limit=null, $start=null, $is_order = array(), $is_search = array())
	{
		$this->db->select('*');
		$this->db->from('news_id', 'news_content');

		$this->db->join("news_content","news_content.main_id = news_id.main_id", "left");

		// Search from Datatable
			if(!empty($is_search)) {
				foreach ($is_search as $key => $search) {

					switch ($key) {
						case 'post_date_format':
							$dateFillter = explode(' - ', $search);

							$this->db->where("news_id.post_date >=", $dateFillter[0]);
							$this->db->where("news_id.post_date <=", $dateFillter[1]);
							$this->db->where("news_content.lang_id", $lang_id);
							break;

						default:
							// Check Column name
							if($this->db->field_exists($key, 'news_id')) {

								$this->db->where("news_id.".$key, $search);
								$this->db->where("news_content.lang_id", $lang_id);
							}
							// Check Column name
							if($this->db->field_exists($key, 'news_content')) {

								$this->db->like("news_content.".$key, $search);
								$this->db->where("news_content.lang_id", $lang_id);
							}
							break;
					}
				}

				$this->db->where("news_id.main_status <>", "deleted");
				$this->db->where("news_content.content_status <>", "deleted");
			} else {

				$this->db->where("news_content.lang_id", $lang_id);
				$this->db->where("news_id.main_status <>", "deleted");
				$this->db->where("news_content.content_status <>", "deleted");
			}

		// Sorting from Datatable
			if(!empty($is_order)) {
				foreach ($is_order as $key => $order) {
					// Check Column name
						if($this->db->field_exists($key, 'news_id')) {

							$this->db->order_by("news_id.".$key, $order);
						}
					// Check Column name
						if($this->db->field_exists($key, 'news_content')) {

							$this->db->order_by("news_content.".$key, $order);
						}
				}
			} else {
				$this->db->order_by("news_id.sequence", "DESC");
			}

		// Limit Start Filtered page
			if($limit || $start) {
				$this->db->limit($limit, $start);

				return $this->db->get();
			} else {

				return $this->db->count_all_results();
			}
	}

	public function getAllContent()
	{
		$this->db->select('main_id');
		$this->db->where('main_status <>','deleted');
		$this->db->order_by('main_id','DESC');

		return $this->db->get('news_id');
	}

	public function getDetail($news_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('news_id', 'news_content');

		if($lang_id){
			$this->db->join("news_content","news_content.main_id = news_id.main_id AND news_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("news_content","news_content.main_id = news_id.main_id AND news_content.content_id = news_id.default_main_id");
		}

		$this->db->where("news_id.main_id",$news_id);
		$this->db->where("news_id.main_status <>",'deleted');

		return  $this->db->get()->row_array();
	}

	public function addData()
	{
		$this->db->select("count(*) AS countSequence");
		$this->db->where("main_status <>","deleted");
		$countRow = $this->db->get("news_id")->row_array();

		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->set("sequence", ($countRow['countSequence']+1) );
		$this->db->insert("news_id");

		$news_id = $this->db->insert_id();
		if(!$news_id) {
			show_error("Cannot create news id");
		}

		return $news_id;
	}

	public function setDate($news_id)
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("main_id",$news_id);

		return $this->db->update("news_id");
	}

	public function setStatus($main_id,$lang_id,$status)
	{
		$this->db->set("content_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("content_id",$main_id);
		$this->db->where("lang_id",$lang_id);

		return $this->db->update("news_content");
	}

	public function setDefaultContent($news_id,$lang_id,$content_id)
	{
		$this->db->set("default_main_id",$content_id);
		$this->db->where("main_id",$news_id);

		return $this->db->update("news_id");
	}

	public function addLanguage($news_id,$lang_id)
	{
		$this->db->where("main_id",$news_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("news_content");

		if($has->num_rows() == 0) {
			$this->db->set("main_id",$news_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("news_content");

			return $this->db->insert_id();
		} else {
			$r= $has->row_array();

			return 	@$r['content_id'];
		}
	}

	public function updateContent(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('main_id',$data['main_id']);
		$this->db->where('lang_id',$data['lang_id']);

		$update_content = $this->db->update('news_content', $data);

		return $update_content;
	}

	public function deleteContent(array $data = array())
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->set("main_status","deleted");
		$this->db->set("sequence",NULL);
		$this->db->where("main_id",$data['main_id']);

		return $this->db->update("news_id");
	}

	public function getThumbnail_img($content_thumbnail)
	{
		$this->db->where('news_attachment.attachment_id', $content_thumbnail);
		$this->db->limit(1);

		return $this->db->get('news_attachment');
	}

	public function getDetail_img($main_id, $lang_id=NULL)
	{
		$type_file = array('gif','jpg','png','jpeg');

		$this->db->select('*');
		$this->db->from('news_id');

		$this->db->join("news_content","news_content.main_id = news_id.main_id AND news_content.content_id = news_id.default_main_id");
		$this->db->join("news_attachment","news_attachment.default_main_id = news_id.main_id ","right");

		$this->db->where("news_id.main_id",$main_id);
		$this->db->where_in("news_attachment.attachment_type", $type_file);
		$this->db->where("news_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("news_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("news_attachment.sequence","ASC");
		$this->db->limit(1000);

		return  $this->db->get();
	}

	public function getDetail_file($main_id,$lang_id=NULL)
	{
		$type_file = array('pdf','doc','docx','rar','zip');

		$this->db->select('*');
		$this->db->from('news_id');

		$this->db->join("news_content","news_content.main_id = news_id.main_id AND news_content.content_id = news_id.default_main_id");
		$this->db->join("news_attachment","news_attachment.default_main_id = news_id.main_id ","right");

		$this->db->where("news_id.main_id",$main_id);
		$this->db->where_in("news_attachment.attachment_type", $type_file);
		$this->db->where("news_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("news_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("news_attachment.sequence","ASC");
		$this->db->limit(1000);

		return  $this->db->get();
	}

	public function insertContent($news_id,$attachment_name,$lang_id=NULL)
	{
		if($attachment_name) {
			$attachment_type = str_replace('.','',strstr($attachment_name,'.'));

			$this->db->set("attachment_name",$attachment_name);
			$this->db->set("attachment_name_main",$attachment_name);
			$this->db->set("attachment_type",$attachment_type);
		}
		$this->db->set("default_main_id",$news_id);
		$this->db->set("lang_id",$lang_id);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());

		return $this->db->insert("news_attachment");
	}

	public function updateAttachmentContent(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('attachment_id',$data['attachment_id']);
		// $this->db->where('lang_id',$data['lang_id']);

		return $this->db->update('news_attachment', $data);
	}

	public function deleteImage($img_id)
	{
		$this->db->where("attachment_id",$img_id);

		return $this->db->delete("news_attachment");
	}

	public function setDefaultImg($default_main_id,$attachment_id,$lang_id)
	{
		$this->db->set("content_thumbnail",$attachment_id);

		$this->db->where("content_id",$default_main_id);
		$this->db->where("lang_id",$lang_id);

		return $this->db->update("news_content");
	}

	public function setSequenceAttachment($id ,$i)
	{
		$this->db->set("sequence",$i);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("attachment_id",$id);

		return $this->db->update("news_attachment");
	}

	public function setSequence($main_id,$new_sequence)
	{
		$this->db->set("sequence",$new_sequence);
		$this->db->where("main_id",$main_id);

		return $this->db->update("news_id");
	}
} 
?>