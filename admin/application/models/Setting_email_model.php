<?php
class Setting_email_model extends CI_Model{
	public function dataTable($lang_id)
	{
		$this->db->select('*');
		$this->db->from('setting_email_id', 'setting_email_content');

		$this->db->join("setting_email_content","setting_email_content.email_id = setting_email_id.email_id");

		$this->db->where("setting_email_content.lang_id",$lang_id);
		$this->db->where("setting_email_id.email_status <>","deleted");
		$this->db->where("setting_email_content.content_status <>","deleted");
		$this->db->order_by("setting_email_id.email_id","desc");

		return $this->db->get();
	}
	public function checkExistst($email_id)
	{
		$this->db->where("email_id",$email_id);
		$this->db->where("setting_email_id.email_status <>","deleted");
		return $this->db->count_all_results("setting_email_id");
	}
	public function getDetail($email_id,$lang_id=NULL,$cate_id=null,$country_id=null)
	{
		$this->db->select('*');
		$this->db->from('setting_email_id', 'setting_email_content');
		
		if($lang_id){
			$this->db->join("setting_email_content","setting_email_content.email_id = setting_email_id.email_id AND setting_email_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("setting_email_content","setting_email_content.email_id = setting_email_id.email_id AND setting_email_content.content_id = setting_email_id.default_content_id");
		}
		
		if($email_id) {
			$this->db->where("setting_email_id.email_id",$email_id);
		} else{
			$this->db->where("setting_email_content.cate_id",$cate_id);
			$this->db->where("setting_email_content.country_id",$country_id);
		}

		$this->db->where("setting_email_id.email_status <>",'deleted');
		$this->db->where("setting_email_content.content_status <>",'deleted');
		$this->db->order_by("setting_email_id.email_id","desc");
		$this->db->limit(1);
		return  $this->db->get()->row_array();
	}
	public function addData()
	{
		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->insert("setting_email_id");
		$email_id = $this->db->insert_id();
		if(!$email_id){
			show_error("Cannot create  email id");	
		}
		return $email_id;
	}
	public function setDate($email_id,$email_date)
	{
		$email_date = date("Y-m-d",strtotime($email_date));
		$this->db->set("email_date",$email_date);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		return $this->db->update("setting_email_id");	
	}
	public function setStatus($email_id,$status)
	{
		$this->db->set("email_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		return $this->db->update("setting_email_id");	
	}
	public function setDefaultContent($email_id,$content_id)
	{
		$this->db->set("default_content_id",$content_id);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		return $this->db->update("setting_email_id");
	}
	public function addLanguage($email_id,$lang_id)
	{
		$this->db->where("email_id",$email_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("setting_email_content");
		
		if($has->num_rows() == 0){
			$this->db->set("email_id",$email_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("setting_email_content");
			return $this->db->insert_id();
		}else{
			$r= $has->row_array();
			return 	@$r['content_id'];
		}
	}
	function updateContent($email_id,$lang_id,$cate_id,$country_id,$subject,$email,$email_cc)
	{
		$this->db->set("cate_id",$cate_id);
		$this->db->set("country_id",$country_id);
		$this->db->set("content_subject",$subject);
		$this->db->set("content_email",$email);
		$this->db->set("content_email_cc",$email_cc);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("setting_email_content");
	}
	function updateContentStatus($email_id,$lang_id,$status)
	{
		$this->db->set("content_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("setting_email_content");
	}
	public function dataTableCategory($lang_id, $status=null, $is_order=null)
	{
		$this->db->select('*');
		$this->db->from('setting_email_category_id', 'setting_email_category_content');

		$this->db->join("setting_email_category_content","setting_email_category_content.email_id = setting_email_category_id.email_id");

		$this->db->where("setting_email_category_content.lang_id",$lang_id);
		if($status) {
			$this->db->where("setting_email_category_id.email_status","active");
			$this->db->where("setting_email_category_content.content_status","active");
		} else {
			$this->db->where("setting_email_category_id.email_status <>","deleted");
		}

		if(!empty($is_order)) {
			foreach ($is_order as $key => $order) {
				// Check Column name
					if($this->db->field_exists($key, 'setting_email_category_id')) {

						$this->db->order_by("setting_email_category_id.".$key, $order);
					}
				// Check Column name
					if($this->db->field_exists($key, 'setting_email_category_content')) {

						$this->db->order_by("setting_email_category_content.".$key, $order);
					}
			}
		} else {
			$this->db->order_by("setting_email_category_id.email_id","desc");
		}

		return $this->db->get();
	}
	public function dataTableCategoryCountry($main_id,$lang_id,$country_id=null)
	{
		$this->db->select('*');
		$this->db->from('setting_email_category_id', 'setting_email_category_country');

		$this->db->join("setting_email_category_country","setting_email_category_country.main_id = setting_email_category_id.email_id");

		if($country_id) {
			$this->db->where("setting_email_category_country.country_id",$country_id);
		} else {
			$this->db->where("setting_email_category_country.main_id",$main_id);
			$this->db->where("setting_email_category_country.lang_id",$lang_id);
		}
		$this->db->where("setting_email_category_id.email_status <>","deleted");
		$this->db->order_by("setting_email_category_country.seq,setting_email_category_country.content_country","ASC");

		return $this->db->get();
	}
	public function addDataCategory()
	{
		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->insert("setting_email_category_id");
		$email_id = $this->db->insert_id();
		if(!$email_id){
			show_error("Cannot create  email id");	
		}
		return $email_id;
	}
	public function addLanguageCategory($email_id,$lang_id)
	{
		$this->db->where("email_id",$email_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("setting_email_category_content");
		
		if($has->num_rows() == 0){
			$this->db->set("email_id",$email_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("setting_email_category_content");
			return $this->db->insert_id();
		}else{
			$r= $has->row_array();
			return 	@$r['content_id'];
		}
	}
	public function setDefaultContentCategory($email_id,$content_id)
	{
		$this->db->set("default_content_id",$content_id);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		return $this->db->update("setting_email_category_id");
	}
	public function setDateCategory($email_id,$email_date)
	{
		$email_date = date("Y-m-d",strtotime($email_date));
		$this->db->set("email_date",$email_date);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		return $this->db->update("setting_email_category_id");	
	}
	function updateContentCategory($email_id,$lang_id,$subject)
	{
		$this->db->set("content_subject",$subject);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("setting_email_category_content");
	}
	function updateContentStatusCategory($email_id,$lang_id,$status)
	{
		$this->db->set("content_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("email_id",$email_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("setting_email_category_content");
	}
	public function getDetailCategory($email_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('setting_email_category_id', 'setting_email_category_content');
		
		if($lang_id){
			$this->db->join("setting_email_category_content","setting_email_category_content.email_id = setting_email_category_id.email_id AND setting_email_category_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("setting_email_category_content","setting_email_category_content.email_id = setting_email_category_id.email_id AND setting_email_category_content.content_id = setting_email_category_id.default_content_id");
		}
		
		$this->db->where("setting_email_category_id.email_id",$email_id);
		$this->db->where("setting_email_category_id.email_status <>",'deleted');
		$this->db->order_by("setting_email_category_id.email_id","desc");
		$this->db->limit(1);
		return  $this->db->get()->row_array();
	}
}