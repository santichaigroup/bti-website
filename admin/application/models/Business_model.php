<?php 
class Business_model extends CI_Model {

	public function dataTable($cate_id, $lang_id="TH", $limit=null, $start=null, $is_order = array(), $is_search = array())
	{
		$this->db->select('*');
		$this->db->from('business_id', 'business_content');

		$this->db->join("business_content","business_content.main_id = business_id.main_id", "left");

		// Search from Datatable
			if(!empty($is_search)) {
				foreach ($is_search as $key => $search) {

					switch ($key) {
						case 'post_date_format':
							$dateFillter = explode(' - ', $search);

							$this->db->where("business_id.post_date >=", $dateFillter[0]);
							$this->db->where("business_id.post_date <=", $dateFillter[1]);
							$this->db->where("business_content.lang_id", $lang_id);
							break;

						default:
							// Check Column name
							if($this->db->field_exists($key, 'business_id')) {

								$this->db->where("business_id.".$key, $search);
								$this->db->where("business_content.lang_id", $lang_id);
							}
							// Check Column name
							if($this->db->field_exists($key, 'business_content')) {

								$this->db->like("business_content.".$key, $search);
								$this->db->where("business_content.lang_id", $lang_id);
							}
							break;
					}
				}

				$this->db->where("business_id.cate_id", $cate_id);
				$this->db->where("business_id.main_status <>", "deleted");
				$this->db->where("business_content.content_status <>", "deleted");
			} else {

				$this->db->where("business_id.cate_id", $cate_id);
				$this->db->where("business_content.lang_id", $lang_id);
				$this->db->where("business_id.main_status <>", "deleted");
				$this->db->where("business_content.content_status <>", "deleted");
			}

		// Sorting from Datatable
			if(!empty($is_order)) {
				foreach ($is_order as $key => $order) {
					// Check Column name
						if($this->db->field_exists($key, 'business_id')) {

							$this->db->order_by("business_id.".$key, $order);
						}
					// Check Column name
						if($this->db->field_exists($key, 'business_content')) {

							$this->db->order_by("business_content.".$key, $order);
						}
				}
			} else {
				$this->db->order_by("business_id.sequence", "asc");
			}

		// Limit Start Filtered page
			if($limit || $start) {
				$this->db->limit($limit, $start);

				return $this->db->get();
			} else {

				return $this->db->count_all_results();
			}
	}

	public function getAllContent()
	{
		$this->db->select('main_id');
		$this->db->where('main_status <>','deleted');
		$this->db->order_by('main_id','DESC');

		return $this->db->get('business_id');
	}

	public function getDetail($business_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('business_id', 'business_content');

		if($lang_id){
			$this->db->join("business_content","business_content.main_id = business_id.main_id AND business_content.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("business_content","business_content.main_id = business_id.main_id AND business_content.content_id = business_id.default_main_id");
		}

		$this->db->where("business_id.main_id",$business_id);
		$this->db->where("business_id.main_status <>",'deleted');

		return  $this->db->get()->row_array();
	}

	public function addData($cate_id)
	{
		$this->db->select("count(*) AS countSequence");
		$this->db->where("cate_id", $cate_id);
		$this->db->where("main_status <>","deleted");
		$countRow = $this->db->get("business_id")->row_array();

		$this->db->set("cate_id",$cate_id);
		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->set("sequence", ($countRow['countSequence']+1) );
		$this->db->insert("business_id");

		$business_id = $this->db->insert_id();
		if(!$business_id) {
			show_error("Cannot create business id");
		}

		return $business_id;
	}

	public function setDate($business_id)
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("main_id",$business_id);

		return $this->db->update("business_id");
	}

	public function setStatus($main_id,$lang_id,$status)
	{
		$this->db->set("content_status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("content_id",$main_id);
		$this->db->where("lang_id",$lang_id);

		return $this->db->update("business_content");
	}

	public function setDefaultContent($business_id,$lang_id,$content_id)
	{
		$this->db->set("default_main_id",$content_id);
		$this->db->where("main_id",$business_id);

		return $this->db->update("business_id");
	}

	public function addLanguage($business_id,$lang_id)
	{
		$this->db->where("main_id",$business_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("business_content");

		if($has->num_rows() == 0) {
			$this->db->set("main_id",$business_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("business_content");

			return $this->db->insert_id();
		} else {
			$r= $has->row_array();

			return 	@$r['content_id'];
		}
	}

	public function updateContent(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('main_id',$data['main_id']);
		$this->db->where('lang_id',$data['lang_id']);

		$update_content = $this->db->update('business_content', $data);

		return $update_content;
	}

	public function deleteContent(array $data = array())
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->set("main_status","deleted");
		$this->db->set("sequence",NULL);
		$this->db->where("main_id",$data['main_id']);

		return $this->db->update("business_id");
	}

	public function getThumbnail_img($content_thumbnail)
	{
		$this->db->where('business_attachment.attachment_id', $content_thumbnail);
		$this->db->limit(1);

		return $this->db->get('business_attachment');
	}

	public function getDetail_img($main_id, $cate_id="0", $lang_id=NULL)
	{
		$type_file = array('gif','jpg','png','jpeg','svg');

		$this->db->select('*');
		$this->db->from('business_id');

		$this->db->join("business_content","business_content.main_id = business_id.main_id AND business_content.content_id = business_id.default_main_id");
		$this->db->join("business_attachment","business_attachment.default_main_id = business_id.main_id ","right");

		$this->db->where("business_id.main_id",$main_id);
		$this->db->where("business_attachment.cate_id",$cate_id);
		$this->db->where_in("business_attachment.attachment_type", $type_file);
		$this->db->where("business_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("business_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("business_attachment.sequence","ASC");
		$this->db->limit(1000);

		return  $this->db->get();
	}

	public function getDetail_file($main_id,$lang_id=NULL)
	{
		$type_file = array('pdf','doc','docx','rar','zip');

		$this->db->select('*');
		$this->db->from('business_id');

		$this->db->join("business_content","business_content.main_id = business_id.main_id AND business_content.content_id = business_id.default_main_id");
		$this->db->join("business_attachment","business_attachment.default_main_id = business_id.main_id ","right");

		$this->db->where("business_id.main_id",$main_id);
		$this->db->where_in("business_attachment.attachment_type", $type_file);
		$this->db->where("business_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("business_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("business_attachment.sequence","ASC");
		$this->db->limit(1000);

		return  $this->db->get();
	}

	public function insertContent($business_id,$attachment_name,$cate_id,$lang_id=NULL)
	{
		if($attachment_name) {
			$attachment_type = str_replace('.','',strstr($attachment_name,'.'));

			$this->db->set("attachment_name",$attachment_name);
			$this->db->set("attachment_name_main",$attachment_name);
			$this->db->set("attachment_type",$attachment_type);
		}
		$this->db->set("default_main_id",$business_id);
		$this->db->set("lang_id",$lang_id);
		$this->db->set("cate_id",$cate_id);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());

		return $this->db->insert("business_attachment");
	}

	public function updateAttachmentContent(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('attachment_id',$data['attachment_id']);
		// $this->db->where('lang_id',$data['lang_id']);

		return $this->db->update('business_attachment', $data);
	}

	public function deleteImage($img_id)
	{
		$this->db->where("attachment_id",$img_id);

		return $this->db->delete("business_attachment");
	}

	public function setDefaultImg($default_main_id,$attachment_id,$lang_id)
	{
		$this->db->set("content_thumbnail",$attachment_id);

		$this->db->where("content_id",$default_main_id);
		$this->db->where("lang_id",$lang_id);

		return $this->db->update("business_content");
	}

	public function setSequenceAttachment($id ,$i)
	{
		$this->db->set("sequence",$i);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("attachment_id",$id);

		return $this->db->update("business_attachment");
	}

	public function setSequence($main_id,$new_sequence)
	{
		$this->db->set("sequence",$new_sequence);
		$this->db->where("main_id",$main_id);

		return $this->db->update("business_id");
	}

	// ################### Country Add & Update ######################

	public function dataTableCountry($main_id, $lang_id="TH", $limit=null, $start=null, $is_order = array(), $is_search = array())
	{
		$this->db->select('*');
		$this->db->from('business_id', 'business_country');

		$this->db->join("business_country","business_country.main_id = business_id.main_id", "left");

		// Search from Datatable
			if(!empty($is_search)) {
				foreach ($is_search as $key => $search) {

					switch ($key) {
						case 'post_date_format':
							$dateFillter = explode(' - ', $search);

							$this->db->where("business_id.post_date >=", $dateFillter[0]);
							$this->db->where("business_id.post_date <=", $dateFillter[1]);
							$this->db->where("business_country.lang_id", $lang_id);
							break;

						default:
							// Check Column name
							if($this->db->field_exists($key, 'business_id')) {

								$this->db->where("business_id.".$key, $search);
								$this->db->where("business_country.lang_id", $lang_id);
							}
							// Check Column name
							if($this->db->field_exists($key, 'business_country')) {

								$this->db->like("business_country.".$key, $search);
								$this->db->where("business_country.lang_id", $lang_id);
							}
							break;
					}
				}

				if($main_id) {
					$this->db->where("business_id.main_id", $main_id);
				}
				$this->db->where("business_id.main_status <>", "deleted");
				$this->db->where("business_country.content_status <>", "deleted");
			} else {

				if($main_id) {
					$this->db->where("business_id.main_id", $main_id);
				}
				$this->db->where("business_country.lang_id", $lang_id);
				$this->db->where("business_id.main_status <>", "deleted");
				$this->db->where("business_country.content_status <>", "deleted");
			}

		// Sorting from Datatable
			if(!empty($is_order)) {
				foreach ($is_order as $key => $order) {
					// Check Column name
						if($this->db->field_exists($key, 'business_id')) {

							$this->db->order_by("business_id.".$key, $order);
						}
					// Check Column name
						if($this->db->field_exists($key, 'business_country')) {

							$this->db->order_by("business_country.".$key, $order);
						}
				}
			} else {
				$this->db->order_by("business_country.content_subject", "asc");
				$this->db->order_by("business_country.sequence", "asc");
			}

		// Limit Start Filtered page
			if($limit || $start) {
				$this->db->limit($limit, $start);

				return $this->db->get();
			} else {

				return $this->db->count_all_results();
			}
	}

	public function insertContentCountry($business_id,$attachment_name,$lang_id=NULL)
	{
		if($attachment_name) {
			$attachment_type = str_replace('.','',strstr($attachment_name,'.'));

			$this->db->set("country_name",$attachment_name);
			$this->db->set("country_type",$attachment_type);
		}
		$this->db->set("default_main_id",$business_id);
		$this->db->set("lang_id",$lang_id);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());

		return $this->db->insert("business_country_attachment");
	}

	public function updateContentCountry(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('content_id',$data['content_id']);

		$update_content = $this->db->update('business_country', $data);

		return $update_content;
	}

	public function addLanguageCountry($main_id,$lang_id,$content_id=null)
	{
		$this->db->where("content_id",$content_id);
		$has = $this->db->get("business_country");

		if($has->num_rows() == 0) {
			$this->db->set("main_id",$main_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("business_country");

			return $this->db->insert_id();
		} else {
			$r= $has->row_array();

			return 	@$r['content_id'];
		}
	}

	public function getDetailCountry($business_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('business_id', 'business_country');

		if($lang_id){
			$this->db->join("business_country","business_country.main_id = business_id.main_id AND business_country.lang_id = '".$lang_id."'");
		}else{
			$this->db->join("business_country","business_country.main_id = business_id.main_id");
		}

		$this->db->where("business_country.content_id",$business_id);
		$this->db->where("business_id.main_status <>",'deleted');

		return  $this->db->get()->row_array();
	}

	public function getDetailCountry_img($main_id, $lang_id=NULL)
	{
		$type_file = array('gif','jpg','png','jpeg','svg');

		$this->db->select('*');
		$this->db->from('business_id');

		$this->db->join("business_country","business_country.main_id = business_id.main_id");
		$this->db->join("business_country_attachment","business_country_attachment.default_main_id = business_country.content_id ","right");

		$this->db->where("business_country.content_id",$main_id);
		$this->db->where_in("business_country_attachment.country_type", $type_file);
		$this->db->where("business_id.main_status <>",'deleted');
		if($lang_id) {
			$this->db->where("business_country_attachment.lang_id",$lang_id);
		}
		$this->db->order_by("business_country_attachment.sequence","ASC");
		$this->db->limit(1000);

		return  $this->db->get();
	}

	public function setSequenceCountry($main_id,$new_sequence)
	{
		$this->db->set("sequence",$new_sequence);
		$this->db->where("content_id",$main_id);

		return $this->db->update("business_country");
	}

	public function deleteContentCountry($content_id=null)
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->set("content_status","deleted");
		$this->db->set("sequence",NULL);
		$this->db->where("content_id",$content_id);

		return $this->db->update("business_country");
	}

	public function getAllContentCountry()
	{
		$this->db->select('content_id');
		$this->db->where('content_status <>','deleted');
		$this->db->order_by('content_id','DESC');

		return $this->db->get('business_country');
	}

	public function deleteImageCountry($img_id)
	{
		$this->db->where("country_id",$img_id);

		return $this->db->delete("business_country_attachment");
	}

	public function updateAttachmentContentCountry(array $data = array())
	{
		$this->db->set('update_by',$this->admin_library->userdata('user_id'));
		$this->db->set('update_date','NOW()',false);
		$this->db->set('update_ip',$this->input->ip_address());

		$this->db->where('country_id',$data['country_id']);
		// $this->db->where('lang_id',$data['lang_id']);

		return $this->db->update('business_country_attachment', $data);
	}

	public function setSequenceAttachmentCountry($id ,$i)
	{
		$this->db->set("sequence",$i);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("country_id",$id);

		return $this->db->update("business_country_attachment");
	}
}
?>