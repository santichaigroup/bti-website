<?php
require_once('config/panel.php');

session_start();
if (!isset($_SESSION['script_user'])) {
    header('Location:index.php');
    exit();
}

require_once('Connections/conn.php');

require_once('site/php/translated_panel_strings.php');

$_SESSION['csrf_d'] = md5(uniqid());

if (isset($_POST['name'])
    && trim($_POST['name']) != ''
    && isset($_POST['language_code'])
    && trim($_POST['language_code']) != ''
    && isset($_POST['csrf_i'])
    && isset($_SESSION['csrf_i'])
    && $_POST['csrf_i'] == $_SESSION['csrf_i']
) {
    $name          = prepare_for_db($_POST['name']);
    $language_code = prepare_for_db($_POST['language_code']);
    $display_order = (intval($_POST['display_order']) <= 255 ? intval($_POST['display_order']) : 0);
    $is_default    = (isset($_POST['is_default']) ? 1 : 0);

    $sql = <<<EOF
INSERT INTO {$panel_strings_table_prefix}languages
(
name,
language_code,
display_order,
is_default
)
VALUES
(
'{$name}',
'{$language_code}',
'{$display_order}',
'{$is_default}'
)
EOF;

    //SQL
    mysqli_query($conn, $sql) or die(mysqli_error($conn));

    //if this is marked as "default", update other records
    if ($is_default) {
        $language_id = mysqli_insert_id($conn);

        $sql = <<<EOF
UPDATE {$panel_strings_table_prefix}languages
SET
is_default = 0
WHERE
NOT id = '{$language_id}'
EOF;
        mysqli_query($conn, $sql) or die(mysqli_error($conn));
    }

    unset($_SESSION['csrf_i']);

    header("Location: panel_languages.php");
    exit();
}

$_SESSION['csrf_i'] = md5(uniqid());

$query_Recordset_List = <<<EOF
SELECT
id,
name,
language_code,
display_order,
is_default
FROM {$panel_strings_table_prefix}languages
WHERE
is_deleted = 0
ORDER BY display_order ASC, name ASC
EOF;
$Recordset_List = mysqli_query($conn, $query_Recordset_List) or die(mysqli_error($conn));
$row_Recordset_List = mysqli_fetch_assoc($Recordset_List);
$totalRows_Recordset_List = mysqli_num_rows($Recordset_List);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $translated_panel_strings['Languages']; ?></title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<?php include('menu_section.php'); ?>
<div class="container">
    <div class="row" style="margin-top: 10px;">
        <p><strong><?php echo $translated_panel_strings['NewLanguage']; ?></strong></p>

        <form action="" method="post" enctype="multipart/form-data" role="form">
            <table class="table table-bordered" style="width: 600px;">
                <tr>
                    <td>
                        <?php echo $translated_panel_strings['Language']; ?>
                    </td>
                    <td>
                        <input type="text" name="name" style="width:160px;"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $translated_panel_strings['LanguageCode']; ?>
                    </td>
                    <td>
                        <input type="text" name="language_code" value="" style="width:160px;"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $translated_panel_strings['DisplayOrder']; ?>
                    </td>
                    <td>
                        <input type="text" name="display_order" value="0" style="width:160px;"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo $translated_panel_strings['IsDefault']; ?>
                    </td>
                    <td>
                        <label>
                            <input type="checkbox" name="is_default" id="is_default">
                            <?php echo $translated_panel_strings['Yes']; ?></label>
                    </td>
                </tr>
                <tr>
                    <td><input type="hidden" name="csrf_i" value="<?php echo $_SESSION['csrf_i']; ?>"/></td>
                    <td>
                        <input type="submit" name="Submit" value="<?php echo $translated_panel_strings['Insert']; ?>"/>
                    </td>
                </tr>
            </table>
        </form>
        <?php
        if ($totalRows_Recordset_List != 0) {
            ?>
            <p><strong><?php echo $translated_panel_strings['Languages']; ?></strong></p>
            <table class="table table-bordered table-striped" style="width: 600px;">
                <tr class="info">
                    <th><?php echo $translated_panel_strings['Language']; ?></th>
                    <th><?php echo $translated_panel_strings['LanguageCode']; ?></th>
                    <th><?php echo $translated_panel_strings['DisplayOrder']; ?></th>
                    <th><?php echo $translated_panel_strings['IsDefault']; ?></th>
                    <th><?php echo $translated_panel_strings['Edit']; ?></th>
                    <th><?php echo $translated_panel_strings['Delete']; ?></th>
                </tr>
                <?php
                do {
                    ?>
                    <tr>
                        <td><?php echo htmlspecialchars($row_Recordset_List['name']); ?></td>
                        <td><?php echo htmlspecialchars($row_Recordset_List['language_code']); ?></td>
                        <td><?php echo htmlspecialchars($row_Recordset_List['display_order']); ?></td>
                        <td><?php echo($row_Recordset_List['is_default'] ? $translated_panel_strings['Yes'] :
                                $translated_panel_strings['No']); ?></td>
                        <td><a href="edit_panel_language.php?id=<?php echo $row_Recordset_List['id']; ?>"><?php echo
                                $translated_panel_strings['Edit']; ?></a></td>
                        <td>
                            <form id="d_l_<?php echo $row_Recordset_List['id']; ?>"
                                  action="<?php echo 'delete_panel_language.php?id=' . $row_Recordset_List['id']; ?>"
                                  method="post" role="form">
                                <input type="hidden" name="csrf_d" value="<?php echo $_SESSION['csrf_d']; ?>"/>
                            </form>
                            <a href="#" onclick="<?php echo
"if (confirm('{$translated_panel_strings['AreYouSure']}')) {" .
" document.getElementById('d_l_{$row_Recordset_List['id']}').submit(); } return false;";
                            ?>"><?php echo $translated_panel_strings['Delete']; ?></a>
                        </td>
                    </tr>
                <?php
                } while ($row_Recordset_List = mysqli_fetch_assoc($Recordset_List));
                ?>
            </table>
        <?php } ?>
    </div>
</div>
</body>
</html>