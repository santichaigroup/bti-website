<nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->

  <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a> -->

  <!-- Navbar Right Menu -->
	  <div class="navbar-custom-menu">
	    <ul class="nav navbar-nav">
	      <!-- Messages: style can be found in dropdown.less-->
	      <?php
	      /*
	      ?>
	      <li class="dropdown messages-menu">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	          <i class="glyphicon glyphicon-envelope"></i>
	          <span class="label label-success">4</span>
	        </a>
	        <ul class="dropdown-menu">
	          <li class="header">You have 4 messages</li>
	          <li>
	            <!-- inner menu: contains the actual data -->
	            <ul class="menu">
	              <li><!-- start message -->
	                <a href="#">
	                  <div class="pull-left">
	                    <img src="images/adminLTE/user2-160x160.jpg" class="img-circle" alt="User Image">
	                  </div>
	                  <h4>
	                    Support Team
	                    <small><i class="glyphicon glyphicon-clock-o"></i> 5 mins</small>
	                  </h4>
	                  <p>Why not buy a new awesome theme?</p>
	                </a>
	              </li><!-- end message -->
	              <li>
	                <a href="#">
	                  <div class="pull-left">
	                    <img src="images/adminLTE/user3-128x128.jpg" class="img-circle" alt="User Image">
	                  </div>
	                  <h4>
	                    AdminLTE Design Team
	                    <small><i class="glyphicon glyphicon-clock-o"></i> 2 hours</small>
	                  </h4>
	                  <p>Why not buy a new awesome theme?</p>
	                </a>
	              </li>
	              <li>
	                <a href="#">
	                  <div class="pull-left">
	                    <img src="images/adminLTE/user4-128x128.jpg" class="img-circle" alt="User Image">
	                  </div>
	                  <h4>
	                    Developers
	                    <small><i class="glyphicon glyphicon-clock-o"></i> Today</small>
	                  </h4>
	                  <p>Why not buy a new awesome theme?</p>
	                </a>
	              </li>
	              <li>
	                <a href="#">
	                  <div class="pull-left">
	                    <img src="images/adminLTE/user3-128x128.jpg" class="img-circle" alt="User Image">
	                  </div>
	                  <h4>
	                    Sales Department
	                    <small><i class="glyphicon glyphicon-clock-o"></i> Yesterday</small>
	                  </h4>
	                  <p>Why not buy a new awesome theme?</p>
	                </a>
	              </li>
	              <li>
	                <a href="#">
	                  <div class="pull-left">
	                    <img src="images/adminLTE/user4-128x128.jpg" class="img-circle" alt="User Image">
	                  </div>
	                  <h4>
	                    Reviewers
	                    <small><i class="glyphicon glyphicon-clock-o"></i> 2 days</small>
	                  </h4>
	                  <p>Why not buy a new awesome theme?</p>
	                </a>
	              </li>
	            </ul>
	          </li>
	          <li class="footer"><a href="#">See All Messages</a></li>
	        </ul>
	      </li>
	      <?php
			*/
	      ?>
	      <!-- Notifications: style can be found in dropdown.less -->
	      <?php
	      /*
	      ?>
	      <li class="dropdown notifications-menu">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	          <i class="glyphicon glyphicon-bell"></i>
	          <span class="label label-warning">10</span>
	        </a>
	        <ul class="dropdown-menu">
	          <li class="header">You have 10 notifications</li>
	          <li>
	            <!-- inner menu: contains the actual data -->
	            <ul class="menu">
	              <li>
	                <a href="#">
	                  <i class="glyphicon glyphicon-users text-aqua"></i> 5 new members joined today
	                </a>
	              </li>
	              <li>
	                <a href="#">
	                  <i class="glyphicon glyphicon-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
	                </a>
	              </li>
	              <li>
	                <a href="#">
	                  <i class="glyphicon glyphicon-users text-red"></i> 5 new members joined
	                </a>
	              </li>
	              <li>
	                <a href="#">
	                  <i class="glyphicon glyphicon-shopping-cart text-green"></i> 25 sales made
	                </a>
	              </li>
	              <li>
	                <a href="#">
	                  <i class="glyphicon glyphicon-user text-red"></i> You changed your username
	                </a>
	              </li>
	            </ul>
	          </li>
	          <li class="footer"><a href="#">View all</a></li>
	        </ul>
	      </li>
	      <?php
			*/
	      ?>
	      <!-- Tasks: style can be found in dropdown.less -->
	      <?php
	      /*
	      ?>
	      <li class="dropdown tasks-menu">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	          <i class="glyphicon glyphicon-flag"></i>
	          <span class="label label-danger">9</span>
	        </a>
	        <ul class="dropdown-menu">
	          <li class="header">You have 9 tasks</li>
	          <li>
	            <!-- inner menu: contains the actual data -->
	            <ul class="menu">
	              <li><!-- Task item -->
	                <a href="#">
	                  <h3>
	                    Design some buttons
	                    <small class="pull-right">20%</small>
	                  </h3>
	                  <div class="progress xs">
	                    <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
	                      <span class="sr-only">20% Complete</span>
	                    </div>
	                  </div>
	                </a>
	              </li><!-- end task item -->
	              <li><!-- Task item -->
	                <a href="#">
	                  <h3>
	                    Create a nice theme
	                    <small class="pull-right">40%</small>
	                  </h3>
	                  <div class="progress xs">
	                    <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
	                      <span class="sr-only">40% Complete</span>
	                    </div>
	                  </div>
	                </a>
	              </li><!-- end task item -->
	              <li><!-- Task item -->
	                <a href="#">
	                  <h3>
	                    Some task I need to do
	                    <small class="pull-right">60%</small>
	                  </h3>
	                  <div class="progress xs">
	                    <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
	                      <span class="sr-only">60% Complete</span>
	                    </div>
	                  </div>
	                </a>
	              </li><!-- end task item -->
	              <li><!-- Task item -->
	                <a href="#">
	                  <h3>
	                    Make beautiful transitions
	                    <small class="pull-right">80%</small>
	                  </h3>
	                  <div class="progress xs">
	                    <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
	                      <span class="sr-only">80% Complete</span>
	                    </div>
	                  </div>
	                </a>
	              </li><!-- end task item -->
	            </ul>
	          </li>
	          <li class="footer">
	            <a href="#">View all tasks</a>
	          </li>
	        </ul>
	      </li>
	      <?php
			*/
	      ?>
	      <!-- User Account: style can be found in dropdown.less -->
	      <li class="dropdown user user-menu">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	        <?php
	        if($user_info['user_thumbnail']) {
	        ?>
	          	<img src="uploads/system_users/images/<?php echo $user_info['user_thumbnail']; ?>" class="user-image" alt="User Image">
	        <?php 
	    	} else {
	    	?>
	    		<img src="images/thumbnail-default.jpg" class="user-image" alt="User Image">
	    	<?php
	    	}
	        ?>
	          <span class="hidden-xs"><?php echo $user_info['user_fullname']; ?></span>
	        </a>
	        <ul class="dropdown-menu">
	          <!-- User image -->
	          <li class="user-header">
	        <?php
	        if($user_info['user_thumbnail']) {
	        ?>
	            <img src="uploads/system_users/images/<?php echo $user_info['user_thumbnail']; ?>" class="img-circle" alt="User Image">
	        <?php 
	    	} else {
	    	?>
	            <img src="images/thumbnail-default.jpg" class="img-circle" alt="User Image">
	    	<?php
	    	}
	        ?>
	            <!-- <p>
	              Alexander Pierce - Web Developer
	              <small>Member since Nov. 2012</small>
	            </p> -->
	          </li>
	          <!-- Menu Footer-->
	          <li class="user-footer">
	            <div class="pull-left">
	              <a href="<?php echo admin_url(); ?>/syssetting/useredit/<?php echo $user_info['user_id']; ?>" class="btn btn-block btn-primary"><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;Profile</a>
	            </div>
	            <div class="pull-right">
	              <a href="<?php echo admin_url(); ?>/logout" class="btn btn-block btn-danger"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;&nbsp;Sign out</a>
	            </div>
	          </li>
	        </ul>
	      </li>
	      <!-- Control Sidebar Toggle Button -->
	      <!-- <li>
	        <a href="javascript:;" data-toggle="control-sidebar"><i class="glyphicon glyphicon-cog"></i></a>
	      </li> -->
	    </ul>
	  </div>

</nav>