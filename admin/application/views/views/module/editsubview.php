<div class="row">
  <div class="col-md-8">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Menu Box</h3>
          <i class="toggle-icon glyphicon glyphicon-triangle-bottom"></i>

      </div>
      <div class="box-body form-horizontal toggle-box">

          <!-- <form name="optionform" id="optionform" method="post" action="" enctype="multipart/form-data" role="form"> -->
            
              <div class="form-group">
                <label for="menu_label" class="col-sm-3 control-label">Menu Label</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_label" class="form-control" id="menu_label" placeholder="Menu Label" disabled>
                </div>
              </div>

              <div class="form-group">
                <label for="menu_title" class="col-sm-3 control-label">Menu Detail</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_title" class="form-control" id="menu_title" placeholder="Menu Detail" disabled>
                </div>
              </div>

              <div class="form-group">
                <label for="menu_label" class="col-sm-3 control-label">Menu Icon</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_icon" class="form-control" id="menu_icon" placeholder="Menu Icon" disabled>
                </div>
              </div>

              <div class="form-group">
                <label for="menu_label" class="col-sm-3 control-label">Menu Link</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_link" class="form-control" id="menu_link" placeholder="Menu Link" disabled>
                </div>
              </div>

              <div class="form-group">
                <label for="menu_label" class="col-sm-3 control-label">Menu SEO Url</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_seo" class="form-control" id="menu_seo" placeholder="Menu SEO Url" disabled>
                </div>
              </div>

              <div class="form-group">
                <label for="menu_label" class="col-sm-3 control-label">Menu Sequent</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_sequent" class="form-control" id="menu_sequent" placeholder="Menu Sequent" disabled>
                </div>
              </div>

          <!-- </form> -->

      </div>
    </div>

  </div>

<?php
/************************************************** Sub Menu **************************************************/
?>

  <div class="col-md-8">

    <div class="box box-info">
      <div class="box-header with-border toggle-sub-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Sub Menu Box</h3>
          <i class="toggle-sub-icon glyphicon glyphicon-triangle-bottom"></i>

      </div>
      <div class="box-body form-horizontal toggle-sub-box">

          <!--  Error Alert  -->
          <h4></h4>
          <?php if(@$success_message!=NULL){ ?>
          <div class="alert alert-success">
            <button class="close" data-dismiss="alert">×</button>
            <strong>Success !</strong> <?php echo $success_message; ?>
          </div>
          <?php } ?>
          <!--  Error Alert  -->

          <form name="optionform" id="optionform" method="post" action="" enctype="multipart/form-data" role="form">
            
              <div class="form-group">
                <label for="menu_label" class="col-sm-3 control-label">Menu Label</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_label" class="form-control" id="menu_label" placeholder="Menu Label">
                </div>
              </div>

              <div class="form-group">
                <label for="menu_title" class="col-sm-3 control-label">Menu Detail</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_title" class="form-control" id="menu_title" placeholder="Menu Detail">
                </div>
              </div>

              <div class="form-group">
                <label for="menu_label" class="col-sm-3 control-label">Menu Icon</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_icon" class="form-control" id="menu_icon" placeholder="Menu Icon">
                </div>
              </div>

              <div class="form-group">
                <label for="menu_label" class="col-sm-3 control-label">Menu Link</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_link" class="form-control" id="menu_link" placeholder="Menu Link">
                </div>
              </div>

              <div class="form-group">
                <label for="menu_label" class="col-sm-3 control-label">Menu SEO Url</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_seo" class="form-control" id="menu_seo" placeholder="Menu SEO Url">
                </div>
              </div>

              <div class="form-group">
                <label for="menu_label" class="col-sm-3 control-label">Menu Sequent</label>
                <div class="col-sm-8">
                    <input type="text" name="menu_sequent" class="form-control" id="menu_sequent" placeholder="Menu Sequent">
                </div>
              </div>

          </form>

      </div>
    </div>

  </div>

<?php
/************************************************** Tools Box **************************************************/
?>

  <div class="col-md-4">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>
      <div class="box-body form-horizontal">

          <!-- <div class="form-group">
              <label for="create_by" class="col-sm-4 control-label">เขียนโดย</label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="create_by" disabled>
              </div>
          </div>

          <div class="form-group">
              <label for="create_date" class="col-sm-4 control-label">เขียนเมื่อ</label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="create_date" disabled>
              </div>
          </div>

          <div class="form-group">
              <label for="upadte_by" class="col-sm-4 control-label">แก้ไขโดย</label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="upadte_by" disabled>
              </div>
          </div>

          <div class="form-group">
              <label for="update_date" class="col-sm-4 control-label">แก้ไขเมื่อ</label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="update_date">
              </div>
          </div> -->

          <div class="form-group">
              <label for="menu_status" class="col-sm-4 control-label">สถานะ</label>
              <div class="col-sm-8">

                  <select name="menu_status" id="menu_status" class="form-control">
                    <option value="active">แสดงข้อมูล</option>
                    <option value="pending">ไม่แสดงข้อมูล</option>
                    <option value="deleted">ลบข้อมูล</option>
                  </select>

              </div>
          </div>

      </div>
      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right">บันทึกข้อมูล</button>
          <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/list_menu" class="btn btn-danger">ย้อนกลับ</a>
      </div>
    </div>

  </div>

<?php
/************************************************** CMS Menu **************************************************/
?>

  <div class="col-md-12">

    <div class="box box-warning">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">CMS Box</h3>

      </div>
      <div class="box-body form-horizontal">
            
          

      </div>
    </div>

  </div>

</div>

<script type="text/javascript">
  
  $(document).ready(function() {

    $(".toggle-click").click(function() {

        $(".toggle-box").toggle("normal");
        $(".toggle-icon").toggleClass("glyphicon-triangle-top");
        $(".toggle-icon").toggleClass("glyphicon-triangle-bottom");
        
    }).trigger("click");

    $(".toggle-sub-click").click(function() {

        $(".toggle-sub-box").toggle("normal");
        $(".toggle-sub-icon").toggleClass("glyphicon-triangle-top");
        $(".toggle-sub-icon").toggleClass("glyphicon-triangle-bottom");
        
    }).trigger("click");

  });

</script>