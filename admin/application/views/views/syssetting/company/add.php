<form method="post">
<div class="span12"> 
  <!-- BEGIN RECENT ORDERS PORTLET-->
  <div class="widget">
    <div class="widget-title widget-user">
      <h4><i class="icon-user"></i> Information</h4> 
      <span class="tools">
      		<a href="javascript:;" class="icon-chevron-down"></a>
      </span>
      </div>
    <div class="widget-body">
							<?php echo validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>'); ?>
            				<div class="control-group">
                                 <label class="control-label" for="input1">Group Name :</label>
                                 <div class="controls">
                                    <input type="text" class="span3" id="group_name" name="group_name" value="<?php echo set_value("group_name"); ?>" />
                                    
                                 </div>
                              </div>
                              <div class="control-group">
                                 <label class="control-label" >Superadmin Group :</label>
                                 <div class="controls">
                                    <select class="span1" data-placeholder="This is Superadmin Group" tabindex="1" name="group_superadmin" id="group_superadmin">
                                       <option value="yes" <?php echo (set_value("group_superadmin")=="yes")?'selected="selected"':''; ?>>Yes</option>
                                       <option value="no" <?php echo (set_value("group_superadmin")!="yes")?'selected="selected"':''; ?> >No</option>
                                    </select>
                                    <p class="help-block">Superadmin is can access all menu.</p>
                                 </div>
                                 
                              </div>
                              <div class="control-group">
                                 <label class="control-label" >Company :</label>
                                 <div class="controls">
                                    <select class="span3 chosen" data-placeholder="Choose a Company" tabindex="1" name="company_id">
                                       <option value=""></option>
                                       <?php foreach($this->admin_library->getAllCompany()->result_array() as  $company){ ?>
                                       <option value="<?php echo $company['company_id']; ?>" <?php echo ($company['company_id']==set_value("company_id"))?'selected="selected"':''; ?> ><?php echo $company['company_name']; ?></option>
                                       <?php } ?>
                                    </select>
                                 </div>
                              </div>
                              <p></p>
                               <div class="control-group">
                               		<div class="controls">
                                 		<button type="submit" class="btn btn-primary">Save</button>
                                 		<button type="button" class="btn btn-info" onclick="$.getLocation('<?php echo admin_url('syssetting/usergroup'); ?>');"><i class="icon-ban-circle icon-white"></i> Cancel</button> 
                                 	</div>
                              </div>
            
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
  <div class="widget access_level">
    <div class="widget-title widget-user">
      <h4><i class="icon-list"></i> Access Level</h4> 
      <span class="tools">
      	
      </span>
      </div>
    <div class="widget-body">
    			
        		<div class="control-group">
                 <label class="control-label" >Select Menu to allow access : </label>
                 <div class="controls">
                 	<?php foreach($this->admin_library->getAllMenu() as $menu){ ?>
                    <?php 
					if(set_value("group_superadmin","no")=="no"){ 
						$checked =(@$_POST['menu'][$menu['id']]==$menu['id'])?true:false;
					}else{
						$checked = true;
					}
					 ?>
                    <label class="checkbox">
                    <input type="checkbox" name="menu[<?php echo $menu['id']; ?>]" value="<?php echo $menu['id']; ?>"  class="menugroup_<?php echo $menu['id']; ?>" data-set=".menu_id_<?php echo $menu['id']; ?>" <?php if($checked){ ?> checked="checked" <?php } ?>  /> <?php echo $menu['label']; ?>
                    </label>
                    <?php foreach($menu['submenu_entry'] as $submenu){ ?>
						<?php 
                        if(set_value("group_superadmin","no")=="no"){ 
                            $schecked =(@$_POST['submenu'][$menu['id']][$submenu['id']]==$submenu['id'])?true:false;
                        }else{
                            $schecked = true;
                        }
                         ?>
                     	<label class="checkbox">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="submenu[<?php echo $menu['id']; ?>][<?php echo $submenu['id']; ?>]" value="<?php echo $submenu['id']; ?>" class="menu_id_<?php echo $menu['id']; ?>" <?php if($schecked){ ?> checked="checked" <?php } ?>  /> <?php echo $submenu['label']; ?>
                        </label>
                    
                    <?php } ?>
                    <script type="text/javascript">
				$(document).ready(function(e) {
					$(".menugroup_<?php echo $menu['id']; ?>").handleCheckAll();
				});
				</script>
                    <?php } ?>
                 </div>
              </div>
        
    </div>
  </div>
</div> 

</form>
<script type="text/javascript">
function show_access_level()
{
	if($('#group_superadmin').val()=="yes"){
		$(".access_level").hide();	
		
		
	}else{
		$(".access_level").show(); 
		
		
	}
	
}
show_access_level();
	//show_access_level();
$("#group_superadmin").change(show_access_level);

</script>