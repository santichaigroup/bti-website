<style type="text/css">
    
</style>

<div class="row">
  <?php echo form_open('', 'name="user_listform" id="user_listform"'); ?>
    <input type="hidden" name="main_date" id="main_date" value="<?php echo set_value("main_date",date("d-m-Y")); ?>" />
    <input type="hidden" name="user_status" id="user_status" value="<?php echo set_value("user_status"); ?>" />

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Information Box</h3>
        </div>
        <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                <button class="close" data-dismiss="alert">×</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
            </div>
          <?php }?>
          <!--  Error Alert  -->

          <div class="col-md-12">
            <div class="col-md-9">
              <div class="form-group">
                <label for="group_name" class="control-label">Group Name: &nbsp;<span style="color:#F00;">*</span></label>
                <input type="text" name="group_name" class="form-control" id="group_name" placeholder="Group Name" value="<?php echo set_value("group_name"); ?>" />
              </div>

              <div class="form-group">
                <label for="group_superadmin" class="control-label">Superadmin Group: </label>
                <div class="controls">
                    <select tabindex="1" name="group_superadmin" id="group_superadmin" class="form-control">
                       <option value="yes" <?php echo (set_value("group_superadmin")=="yes")?'selected="selected"':''; ?>>Yes</option>
                       <option value="no" <?php echo (set_value("group_superadmin")!="yes")?'selected="selected"':''; ?> >No</option>
                    </select>
                </div>
                <p class="help-block">Superadmin is can access all menu.</p>
              </div>

              <div class="form-group">
                <label for="company_id" class="control-label">Company Name: &nbsp;<span style="color:#F00;">*</span></label>
                <div class="controls">
                    <select tabindex="1" name="company_id" class="form-control">
                       <option value="">กรุณาเลือก Company Name</option>
                       <?php foreach($this->admin_library->getAllCompany()->result_array() as  $company){ ?>
                       <option value="<?php echo $company['company_id']; ?>" <?php echo ($company['company_id']==set_value("company_id"))?'selected="selected"':''; ?> ><?php echo $company['company_name']; ?></option>
                       <?php } ?>
                    </select>
                </div>
              </div>

              <div class="form-group access_level">
                   <label class="control-label" for="user_group">Access Level: &nbsp;<span style="color:#F00;">*</span></label>
                   <p class="help-block">Select Menu to allow access</p>
                   <div class="checkbox">

                    <?php foreach($this->admin_library->getAllMenu() as $menu){ ?>
                          <?php 
                              if(set_value("group_superadmin","no")=="no"){ 
                                $checked =(@$_POST['menu'][$menu['id']]==$menu['id'])?true:false;
                              }else{
                                $checked = true;
                              }
                          ?>
                                <label class="">
                                <input type="checkbox" name="menu[<?php echo $menu['id']; ?>]" value="<?php echo $menu['id']; ?>"  class="menugroup_<?php echo $menu['id']; ?>" data-set=".menu_id_<?php echo $menu['id']; ?>" <?php if($checked){ ?> checked="checked" <?php } ?>  /> <b><?php echo $menu['label']; ?></b>
                                </label>
                                <br />
                          <?php foreach($menu['submenu_entry'] as $submenu){ ?>
                          <?php 
                                    if(set_value("group_superadmin","no")=="no"){ 
                                        $schecked =(@$_POST['submenu'][$menu['id']][$submenu['id']]==$submenu['id'])?true:false;
                                    }else{
                                        $schecked = true;
                                    }
                          ?>
                                  <label class="">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="submenu[<?php echo $menu['id']; ?>][<?php echo $submenu['id']; ?>]" value="<?php echo $submenu['id']; ?>" class="menu_id_<?php echo $menu['id']; ?>" <?php if($schecked){ ?> checked="checked" <?php } ?>  /> <?php echo $submenu['label']; ?>
                                    </label>
                                <br />
                        <?php } ?>
                        <br />
                        <script type="text/javascript">
                          $(document).ready(function(e) {
                            $(".menugroup_<?php echo $menu['id']; ?>").handleCheckAll();
                          });
                        </script>
                    <?php } ?>

                   </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label for="post_date" class="control-label">Join Date: </label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="glyphicon glyphicon-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right active date-picker" name="post_date" id="post_date" value="<?php echo set_value("post_date",date("d-m-Y")); ?>"  readonly="readonly">
                </div>
              </div>

              <div class="form-group">
                <label for="group_status" class="control-label">User Status: </label>

                <select name="group_status" id="group_status" class="form-control">
                  <option value="active" <?php if(set_value("group_status")=="active") { ?>selected="selected" <?php } ?>>อนุมัติใช้งาน</option>
                  <option value="pending" <?php if(set_value("group_status")=="pending") { ?>selected="selected" <?php } ?>>ไม่อนุมัติใช้งาน</option>
                </select>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <a href="<?php echo admin_url($_menu_link."/usergroup"); ?>" class="btn btn-block btn-danger">
                      <i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;&nbsp;Back
                    </a>
                  </div>
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-block btn-primary pull-right">
                      <i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

  <?php echo form_close(); ?>
</div>

<script src="plugins/validate/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(function () {

    $("#user_listform").validate({
        rules: {
          content_subject: "required",
        },

        messages: {
          content_subject: "กรุณากรอกชื่อ-นามสกุล",
        }
      });

  });

  function show_access_level()
  {
      if($('#group_superadmin').val()=="yes"){
        $(".access_level").hide();
      }else{
        $(".access_level").show();
      }
  }

  show_access_level();

  $("#group_superadmin").change(show_access_level);
</script>