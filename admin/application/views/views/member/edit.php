<div class="row">
  <form name="optionform" id="optionform" method="post" enctype="multipart/form-data" role="form" class="form-horizontal">

  <div class="col-md-6">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Profile Box</h3>

      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                <button class="close" data-dismiss="alert">×</button>
                <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
            </div>
          <?php }?>
          <!--  Error Alert  -->

          <?php

            $member_id        = $row['member_id'];
            $member_class     = $row['class_name'];
            $email            = $row['email'];
            $telephone        = $row['telephone'];
            $fullname         = $row['firstname']." ".$row['lastname'];
            $gender           = $row['gender'];
            $birthdate        = $row['birthdate'];
            $is_enews         = $row['is_enews'];
            $post_by          = $row['post_by'];
            $post_date        = $row['post_date'];
            $status           = $row['status'];
            $sequence         = $row['sequence'];

          ?>

            <input type="hidden" name="update_date" id="update_date" value="<?php echo set_value("update_date", $row['update_date']); ?>">
            <input type="hidden" name="member_id" id="member_id" value="<?php echo $row['member_id']; ?>">
          
            <div class="form-group">
              <label for="content_subject" class="col-sm-3 control-label">หมายเลขสมาชิก:</label>
              <div class="col-sm-7">
                <span class="form-control"><?php echo ($member_id ? $member_id : '-' ); ?></span>
              </div>
            </div>

            <div class="form-group">
              <label for="email" class="col-sm-3 control-label">ระดับสมาชิก:</label>
              <div class="col-sm-7">
                <span class="form-control"><?php echo ($member_class ? $member_class : '-' ); ?></span>
              </div>
            </div>

            <div class="form-group">
              <label for="email" class="col-sm-3 control-label">อีเมล:</label>
              <div class="col-sm-7">
                <span class="form-control"><?php echo ($email ? $email : '-' ); ?></span>
              </div>
            </div>

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">เบอร์โทรศัพท์:</label>
              <div class="col-sm-7">
                <span class="form-control"><?php echo ($telephone ? $telephone : '-' ); ?></span>
              </div>
            </div>

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">ชื่อ - นามสกุล:</label>
              <div class="col-sm-7">
                <span class="form-control"><?php echo ($fullname ? $fullname : '-' ); ?></span>
              </div>
            </div>

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">เพศ:</label>
              <div class="col-sm-7">
              <?php
              switch ($gender) {
                case 'M':
                  $gender = "ชาย";
                  break;
                case 'F':
                  $gender = "หญิง";
                  break;

                default:
                  $gender = "ไม่ระบุ";
                  break;
              }
              ?>
                <span class="form-control"><?php echo $gender; ?></span>
              </div>
            </div>

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">วัน/เดือน/ปี เกิด:</label>
              <div class="col-sm-7">
                <span class="form-control"><?php echo ($birthdate ? $birthdate : '-' ); ?></span>
              </div>
            </div>

<!--             <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">รับข่าวสาร:</label>
              <div class="col-sm-7">
                <span class="form-control"><?php echo ($is_enews=="Y" ? "รับข่าวสาร" : 'ไม่รับข่าวสาร' ); ?></span>
              </div>
            </div> -->

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">ลงทะเบียนเมื่อ:</label>
              <div class="col-sm-7">
                <span class="form-control"><?php echo ($post_date ? $post_date : '-' ); ?></span>
              </div>
            </div>

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">ลงทะเบียนโดย:</label>
              <div class="col-sm-7">
                <span class="form-control"><?php echo ($post_by=="0" ? "สมาชิก" : 'เจ้าหน้าที่ <a href="'.admin_url("syssetting/useredit/".$row['post_by']).'" title="คลิกเพื่อดู">'.$this->admin_library->getUser($row['post_by']).'</a>' ); ?></span>
              </div>
            </div>

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">สถานะสมาชิก:</label>
              <div class="col-sm-7">
                  <div class="btn-group checktools">
                     <button type="button" class="btn checkable-dropdown" data-toggle="dropdown" style="border-radius: 3px;"> 
                        <?php echo ( $status == "active" ? "อนุมัติสมาชิก" : "รอการอนุมัติ" ); ?> 
                        <span class="icon-caret-down"></span>
                      </button>&nbsp;
                     <ul class="dropdown-menu">
                        <li><a href="javascript:;" onclick="selectdata_unsuspend();">อนุมัติสมาชิก</a></li>
                        <li><a href="javascript:;" onclick="selectdata_suspend();">รอการอนุมัติ</a></li>
                     </ul>
                  </div>
              </div>
            </div>

      </div>
    </div>

  </div>

<?php
/************************************************** Address Box **************************************************/
?>

  <div class="col-md-6">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Address Box</h3>

      </div>
      <div class="box-body">

      <?php

          $i=1;
          foreach ($address->result_array() as $row_address) {

      ?>

              <div class="form-group">
                <label class="col-sm-3 control-label" style="text-align: left;">Address <?php echo $i; ?></label>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">บ้านเลขที่/ หมู่บ้าน/ ชื่ออาคาร/ ชั้น:</label>
                <div class="col-sm-7">
                  <span class="form-control"><?php echo $row_address['address_no']; ?></span>
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">ซอย:</label>
                <div class="col-sm-7">
                  <span class="form-control"><?php echo $row_address['address_soi']; ?></span>
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">ถนน:</label>
                <div class="col-sm-7">
                  <span class="form-control"><?php echo $row_address['address_road']; ?></span>
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">จังหวัด:</label>
                <div class="col-sm-7">
                  <span class="form-control"><?php echo $row_address['province_name']; ?></span>
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">อำเภอ / เขต:</label>
                <div class="col-sm-7">
                  <span class="form-control"><?php echo $row_address['district_name']; ?></span>
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">ตำบล / เเขวง:</label>
                <div class="col-sm-7">
                  <span class="form-control"><?php echo $row_address['sub_district_name']; ?></span>
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">รหัสไปรษณีย์:</label>
                <div class="col-sm-7">
                  <span class="form-control"><?php echo $row_address['postal_code']; ?></span>
                </div>
              </div>

      <?php
            $i++;
          }
      ?>

      </div>
      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
          <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/index/<?php echo $lang_id; ?>" class="btn btn-danger">ย้อนกลับ</a>
      </div>
    </div>

  </div>

  </form>
</div>

<script type="text/javascript">
  function save_form()
  {
    $("form#optionform").submit();
  }

  function selectdata_suspend()
  {
    $("#usergroup_listform").attr("action",admin_url+"member/handle_suspend_member");
    $("#usergroup_listform").submit();
  }
  function selectdata_unsuspend()
  {
    $("#usergroup_listform").attr("action",admin_url+"member/handle_unsuspend_member");
    $("#usergroup_listform").submit();
  }
</script>