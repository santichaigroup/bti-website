<div class="row">
  <?php echo form_open('', 'name="optionform" id="optionform"'); ?>
    <div class="col-md-12">

      <div class="box box-primary">
        <div class="box-header with-border toggle-click">
          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Form Box</h3>
        </div>
        <div class="box-body">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <h4><i class="icon fa fa-ban"></i> Error!</h4> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
            <?php }?>
            <!--  Error Alert  -->

            <div class="row">
              <div class="col-md-9">
                <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>" />
              
                <div class="form-group">
                    <label for="content_subject" class="control-label">ประเภทสินค้า: &nbsp;<span style="color:#F00;">*</span></label>

                    <?php
                    if(empty($row['content_subject'])) {

                      echo '<input type="text" name="content_subject" class="form-control" id="content_subject" placeholder="" value="'.set_value('content_subject', $row['content_subject']).'">';

                    } else {
                    ?>
                        <select name="content_all_menu" id="content_all_menu" class="form-control" onchange="if (this.value) window.location.href=this.value">
                          <?php
                          foreach ($row_all->result_array() as $value) {

                            if($row['email_id']==$value['email_id']) {
                              $selected = "selected";
                            } else {
                              $selected = "";
                            }
                            
                            echo "<option value='".admin_url('contact/edit_mail_category/'.$value['email_id']."/".$lang_id)."' ".$selected.">".$value['content_subject']."</option>";
                          }
                          ?>
                        </select>
                    <?php

                      echo '<input type="hidden" name="content_subject" class="form-control" id="content_subject" placeholder="" value="'.set_value('content_subject', $row['content_subject']).'">';
                    }
                    ?>
                </div>

                <div class="form-group">
                  <label for="content_detail" class="control-label">ชื่อประเทศ:</label>
                    <select name="content_detail[]" id="content_detail" class="form-control" multiple="true">
                      <?php
                        foreach ($countries as $country) {

                          if(is_int(array_search($country['content_country'], array_column($row['countries'], 'content_country')))) {

                            echo "<option value='".$country['content_country']."' selected>".$country['content_country']."</option>";
                          } else {

                            echo "<option value='".$country['content_country']."'>".$country['content_country']."</option>";
                          }
                        }
                      ?>
                    </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                    <label for="content_status" class="control-label">การแสดงผล: </label>

                    <select name="content_status" id="content_status" class="form-control">
                      <option value="active" <?php if(set_value("content_status", $row['content_status'])=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                      <option value="pending" <?php if(set_value("content_status", $row['content_status'])=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
                      <option value="deleted" <?php if(set_value("content_status", $row['content_status'])=="deleted"){ ?>selected="selected" <?php } ?>>ลบข้อมูล</option>
                    </select>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <a href="<?php echo admin_url('contact/add_email_category/'.$lang_id); ?>" class="btn btn-block btn-success">
                        <i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;เพิ่มประเภทสินค้า
                      </a>
                    </div>
                    <div class="col-md-6">
                      <a href="javascript:;" class="btn btn-block btn-primary pull-right" onclick="save_form();">
                        <i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>

      </div>
    </div>
  <?php echo form_close(); ?>
</div>

<script type="text/javascript">
  $(function() {
    $("#content_detail").select2({
      placeholder: "Please enter country name.",
      tags: true
    });
  }); 

  function save_form()
  {
    $("form#optionform").submit();  
  }
</script>