<style type="text/css">
    .image_box .image_tools_mb {
      opacity: 0;
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      overflow: hidden;
      width: 100%;
      height: 100%;
      cursor: pointer;
    }
      .image_box .image_tools_mb:hover {
        opacity: 1;
      }
</style>

<div class="row">
  <?php echo form_open_multipart('', 'name="optionform" id="optionform"'); ?>
    <input type="hidden" name="lang_id" id="lang_id" value="<?php echo $lang_id; ?>" />
    <input type="hidden" name="images" id="images" value="" />
    <input type="hidden" name="images_mb" id="images_mb" value="" />

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border toggle-click">
            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Infomation Box</h3>
        </div>
        <div class="box-body">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <h4><i class="icon fa fa-ban"></i> Error!</h4> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
            <?php }?>
            <!--  Error Alert  -->

          <?php
          /************************* Infomation Box *************************/
          ?>
            <div class="col-md-12">
              <div class="col-md-9">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-7">
                      <label for="content_subject" class="control-label">Title: &nbsp;<span style="color:#F00;">*</span></label>
                      <input type="text" name="content_subject" class="form-control" id="content_subject" placeholder="" value="<?php echo set_value("content_subject"); ?>">
                    </div>
                    <div class="col-md-5">
                      <label>&nbsp;</label>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="subject_status" id="subject_status" value="1" checked> Display Title
                        </label>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="content_url" class="control-label">Site Url: </label>
                  <input type="text" name="content_url" class="form-control" id="content_url" placeholder="Example: https://" value="<?php echo set_value('content_url'); ?>">
                </div>

                <div class="form-group">
                  <label for="content_detail" class="control-label">Description: </label>
                    <textarea class="form-control" name="content_detail" id="content_detail" rows="6" cols="80" placeholder="รายละเอียดเพิ่มเติม"><?php echo set_value('content_detail'); ?></textarea>
                </div>

                <div class="form-group">
                  <label for="content_url" class="control-label">Set Time:</label>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="content_startdate" class="control-label">Start Date: </label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="glyphicon glyphicon-calendar"></i>
                          </div>
                          <input type="text" class="form-control column_filter" name="content_startdate" id="content_startdate" value="<?php echo set_value("content_startdate", date("Y-m-d 00:00:00")); ?>"  readonly="readonly">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="content_enddate" class="control-label">End Date: </label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="glyphicon glyphicon-calendar"></i>
                          </div>
                          <input type="text" class="form-control column_filter" name="content_enddate" id="content_enddate" value="<?php echo set_value("content_enddate", date("Y-m-d 23:59:59", strtotime('+1 year'))); ?>"  readonly="readonly">
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

              <div class="col-md-3">
                <!-- <div class="form-group">
                  <label for="menu_status" class="control-label">Language: </label><br>
                  <div class="controls btn-group">
                    <button class="btn dropdown-toggle" data-toggle="dropdown">
                      <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
                        <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
                       <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                      <?php foreach($this->admin_library->getLanguageList() as $lang){
                        if($lang_id <> $lang['lang_id']){
                      ?>
                        <li>
                          <a href="<?php echo admin_url($this->menu['menu_link'].$this->submenu['menu_link']."/add/".$lang['lang_id']); ?>"><img src="images/flags/<?php echo $lang['lang_flag']; ?>">&nbsp;<?php echo $lang['lang_name']; ?></a>
                        </li>
                      <?php }} ?>
                    </ul>
                  </div>
                </div> -->

                <!-- <div class="form-group">
                    <label for="create_by" class="control-label">เขียนโดย: </label>
                    <input type="text" class="form-control" name="post_by" id="post_by" disabled>
                </div> -->

                <div class="form-group">
                  <label for="post_date" class="control-label">Create date: </label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="glyphicon glyphicon-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right active date-picker" name="post_date" id="post_date" value="<?php echo set_value("post_date", date("d-m-Y")); ?>"  readonly="readonly">
                  </div>
                </div>

                <!-- <div class="form-group">
                    <label for="upadte_by" class="control-label">แก้ไขโดย: </label>
                    <input type="text" class="form-control" name="upadte_by" id="upadte_by" disabled>
                </div>

                <div class="form-group">
                    <label for="update_date" class="control-label">แก้ไขเมื่อ: </label>
                    <input type="text" class="form-control" name="update_date" id="update_date">
                </div> -->

                <div class="form-group">
                  <label for="content_status" class="control-label">Status: </label>

                  <select name="content_status" id="content_status" class="form-control">
                    <option value="active" <?php if(set_value("content_status")=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                    <option value="pending" <?php if(set_value("content_status")=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
                    <option value="deleted">ลบข้อมูล</option>
                  </select>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <a href="<?php echo admin_url($_menu_link."/index/".$lang_id); ?>" class="btn btn-block btn-danger">
                        <i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;&nbsp;Back
                      </a>
                    </div>
                    <div class="col-md-6">
                      <button type="submit" class="btn btn-block btn-primary pull-right">
                        <i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          <?php
          /************************* Images & Files upload Box *************************/
          ?>
            <div class="col-md-12">
              <div class="col-md-12">
                  <div class="form-group">
                    <label for="photo" class="control-label">Upload Pictures Desktop:</label>
                    <div class="input-group" style="width: 40%;">
                        <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Browse&hellip; <input type="file" style="display: none;" name="image_thumb[]" id="image_thumb" accept="image/*" multiple>
                            </span>
                        </label>
                        <input type="text" class="form-control btn btn-block image_count" readonly>
                    </div>
                    <p class="help-block">*ขนาดรูป 2000x1600 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif จำนวน 1 รูป</p>
                    <div class="row image_thumb"></div>
                  </div>

                  <div class="form-group">
                    <label for="photo" class="control-label">Upload Pictures Mobile:</label>
                    <div class="input-group" style="width: 40%;">
                        <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Browse&hellip; <input type="file" style="display: none;" name="image_thumb_mb[]" id="image_thumb_mb" accept="image/*" multiple>
                            </span>
                        </label>
                        <input type="text" class="form-control btn btn-block image_count_mb" readonly>
                    </div>
                    <p class="help-block">*ขนาดรูป 640x926 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif จำนวน 1 รูป</p>
                    <div class="row image_thumb_mb"></div>
                  </div>

                  <!-- <div class="form-group">
                    <label for="content_subject" class="control-label">Upload Files Attachment: </label>
                      <div class="input-group" style="width: 40%;">
                          <label class="input-group-btn">
                              <span class="btn btn-primary">
                                  Browse&hellip; <input type="file" style="display: none;" name="file_thumb[]" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf" multiple>
                              </span>
                          </label>
                          <input type="text" class="form-control btn btn-block" readonly>
                      </div>
                      <p class="help-block">*ไฟล์ขนาดไม่เกิน 20 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .pdf, doc, docx, rar, zip จำนวน 1 ไฟล์</p>
                  </div> -->
                </div>
              </div>

        </div>
      </div>
    </div>

  <?php
  /************************************************** SEO Box **************************************************/
  ?>

    <!-- <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">

            <i class="glyphicon glyphicon-check"></i>
            <h3 class="box-title">SEO Box</h3>

        </div>
        <div class="box-body">

            <div class="form-group">
              <label for="content_title" class="control-label">Meta Title: </label>
              <input type="text" name="content_title" class="form-control" id="content_title" placeholder="หัวข้อเว็บไซต์" value="<?php echo set_value('content_title'); ?>">
            </div>

            <div class="form-group">
              <label for="content_description" class="control-label">Meta Description: </label>
              <textarea name="content_description" class="form-control" id="content_description" rows="3" placeholder="รายละเอียดเว็บไซต์"><?php echo set_value('content_description'); ?></textarea>
            </div>

            <div class="form-group">
              <label for="content_keyword" class="control-label">Meta Keywords: </label>
              <input type="text" name="content_keyword" class="form-control" id="content_keyword" placeholder="คำค้นหา สำหรับ SEO" value="<?php echo set_value('content_keyword'); ?>">
              <p class="help-block">ใช้ , คั่นระหว่างคำ</p>
            </div>
              
        </div>
      </div>
    </div> -->

  <?php echo form_close(); ?>
</div>

<script>
  var storedFiles = [],
      items       = [];

  $(document).ready(function() {

    $(document).on('change', ':file', function() {

      var input     = $(this),
          numFiles  = input.get(0).files ? input.get(0).files.length : 1,
          label     = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });

    $(':file').on('fileselect', function(event, numFiles, label) {

      var input     = $(this).parents('.input-group').find(':text'),
          thumbnail = $('.image_thumb').find('.image_select').length,
          log       = numFiles > 1 ? (thumbnail + numFiles) + ' files selected' : label;
      if( input.length ) {
          input.val(log);
      } else {
          if( log ) alert(log);
      }
    });

    $("#image_thumb").on("change", handleFileSelect);
    $("#image_thumb_mb").on("change", handleFileSelect_mb);
    $("body").on("click", ".image_tools", removeFile);
    $("body").on("click", ".image_tools_mb", removeFile_mb);

    // DateRang Picker
      $('#content_startdate').daterangepicker({
            "drops": "down",
            "autoApply": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "singleDatePicker": true,
            locale: {
              format: 'YYYY-MM-DD HH:mm:ss'
            }
        });

      $('#content_enddate').daterangepicker({
            "drops": "down",
            "autoApply": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "singleDatePicker": true,
            locale: {
              format: 'YYYY-MM-DD HH:mm:ss'
            }<?php if(set_value('content_startdate')) { ?>,"minDate" : '<?php echo set_value('content_startdate'); ?>' <?php } ?>
        });

          $('#content_startdate').on('showCalendar.daterangepicker', function(ev, picker) {

            $('#content_enddate').daterangepicker({
                  "drops": "down",
                  "autoApply": true,
                  "timePicker": true,
                  "timePicker24Hour": true,
                  "singleDatePicker": true,
                  locale: {
                    format: 'YYYY-MM-DD HH:mm:ss'
                  },
                  "minDate": picker.startDate.format('YYYY-MM-DD HH:mm:ss')
            });
          });
      // DateRang Picker

      // Checkbox display
        $('#subject_status').change(function() {

          if($('#subject_status').is(':checked')) {
            $('#subject_status').val('1');
          } else {
            $('#subject_status').val('0');
          }
        });
    });
    
  function handleFileSelect(e) {

    $(".image_thumb").html('');
    $('#images').val('');
    storedFiles   = [];
    items         = [];
    var files     = e.target.files;
    var filesArr  = Array.prototype.slice.call(files);
    filesArr.forEach(function(f) {

      if(!f.type.match("image.*")) {
        return;
      }
      
      storedFiles.push(f);
      
      var reader    = new FileReader();
      reader.onload = function (e) {

        $(".image_thumb").append('<div class="col-md-3 col-sm-3 col-xs-3 image_select">'
          +'<div class="thumbnail image_box"><a href="javascript:;"><img src="'+e.target.result+'"></a>'
          +'<div class="image_tools flex-center hover-opacity" data-file="'+f.name+'"><i class="fa fa-remove text-red"></i></div>'
          +'</div></div>');
      }
      reader.readAsDataURL(f);
    });
  }

  function handleFileSelect_mb(e) {

    $(".image_thumb_mb").html('');
    $('#images_mb').val('');
    storedFiles   = [];
    items         = [];
    var files     = e.target.files;
    var filesArr  = Array.prototype.slice.call(files);
    filesArr.forEach(function(f) {

      if(!f.type.match("image.*")) {
        return;
      }
      
      storedFiles.push(f);
      
      var reader    = new FileReader();
      reader.onload = function (e) {

        $(".image_thumb_mb").append('<div class="col-md-3 col-sm-3 col-xs-3 image_select_mb">'
          +'<div class="thumbnail image_box"><a href="javascript:;"><img src="'+e.target.result+'"></a>'
          +'<div class="image_tools_mb flex-center hover-opacity" data-file="'+f.name+'"><i class="fa fa-remove text-red"></i></div>'
          +'</div></div>');
      }
      reader.readAsDataURL(f);
    });
  }

  function removeFile(e) {

    var file  = $(this).data("file");
    var i     = storedFiles.map(function (x) { return x.name; }).indexOf(file);

    items.push(i);
    $('#images').val(items);

    var thumbnail = $('.image_thumb').find('.image_select').length,
          log     = thumbnail > 0 ? (thumbnail-1) + ' files selected' : '';
          $('.image_count').val(log);

    $(this).parent().parent().remove();
  }

  function removeFile_mb(e) {

    var file  = $(this).data("file");
    var i     = storedFiles.map(function (x) { return x.name; }).indexOf(file);

    items.push(i);
    $('#images_mb').val(items);

    var thumbnail = $('.image_thumb_mb').find('.image_select_mb').length,
          log     = thumbnail > 0 ? (thumbnail-1) + ' files selected' : '';
          $('.image_count_mb').val(log);

    $(this).parent().parent().remove();
  }
</script>