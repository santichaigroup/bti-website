<?php 
class Product extends CI_Controller {

	var $_data = array(),
		$_menu_name,
		$menu,
		$submenu;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('admin_library');
		$this->admin_library->forceLogin();
		$this->load->model('Product_model');
		$this->load->library('form_validation');

		$this->path 	= 	$this->uri->ruri_string();
		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(1));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(1), $this->uri->segment(2));
	}

	public function index($cate_id, $lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['cate_id'] 		= $cate_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');

		// ---------- Tools Sequent ---------- //
			$seq 		= $this->input->post('seq');
			$content_id = $this->input->post('content_id');
			if($content_id) {

				foreach ($content_id as $key => $value) {

					$this->Product_model->setSequence($key, $value);
				}
			}

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->menu['menu_icon']);
		$this->admin_library->setDetail($this->menu['menu_title']);
		$this->admin_library->view("product/listview",$this->_data);
		$this->admin_library->output($this->path);
	}

	public function load_datatable($cate_id, $lang_id="TH")
	{
		$this->form_validation->set_rules("length","Length","trim|required|integer");
		$this->form_validation->set_rules("start","Start","trim|required|integer");

		if($this->form_validation->run()===false) {

			echo json_encode([]);
		} else {
		
			$limit 			= $this->input->post('length');
			$start 			= $this->input->post('start');
			$is_order 		= array();
			$is_search 		= array();
			$result_array 	= array();

			// ####### Column all ####### //
				if(!empty($this->input->post('columns'))) {

					foreach ($this->input->post('columns') as $key => $column) {

						$is_order[$key] = $column['data'];

						if($column['search']['value']) {

							$is_search[$column['data']] 	= $column['search']['value'];
						}
					}
				}

			// ####### Orderable ####### //
				if(!empty($this->input->post('order')[0]['column'])) {

					$is_order = array($is_order[$this->input->post('order')[0]['column']] => $this->input->post('order')[0]['dir']);
				} else {

					$is_order = array();
				}

			// ####### Searchable ####### //
				if(empty($is_search)) {

					$all_data 		= $this->Product_model->dataTable($cate_id, $lang_id);
		            $query_data 	= $this->Product_model->dataTable($cate_id, $lang_id, $limit, $start, $is_order)
		            															->result_array();
		        } else {

					$all_data 		= $this->Product_model->dataTable($cate_id, $lang_id, null, null, null, $is_search);
					$query_data 	= $this->Product_model->dataTable($cate_id, $lang_id, $limit, $start, $is_order, $is_search)
																				->result_array();
				}

			if(!empty($query_data)) {

				foreach ($query_data as $key => $value) {
					$result_array[$key] = $value;
					$result_array[$key]['no'] = ($key+1)+$start;
					$result_array[$key]['post_date_format'] = date_format_convert_shortdatetime($value['post_date']);
				}
			}

			$output = array(
							"draw"            	=> intval($this->input->post('draw')),
							"recordsFiltered"   => intval($all_data),
							"recordsTotal" 		=> count($result_array),
							"data"            	=> $result_array
						);

			echo json_encode($output);
		}
	}

	public function add($cate_id, $lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id']=$lang_id;
		$this->_data['cate_id']=$cate_id;

		$this->form_validation->set_rules("content_subject","Title","trim|required|max_length[255]");
		$this->form_validation->set_rules("content_detail","Description","trim");

		// เช็คอัพโหลดไฟล์
		$this->form_validation->set_rules("image_thumb[]","Pictures","callback_fileupload_images");
		$this->form_validation->set_rules("file_thumb[]","Files Attachment","callback_fileupload_files");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']			= "Add ".$this->menu['menu_label'];
			$this->_data['_menu_icon']			= "glyphicon-plus-sign";
			$this->_data['_menu_title']			= " เพิ่ม".$this->menu['menu_title'];
			$this->_data['_menu_link']  		= $this->menu['menu_link'];

			$this->_data['validation_errors'] 	= validation_errors();
			$this->_data['error_message'] 		= $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("product/add",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$main_id 		= $this->Product_model->addData($cate_id);
			$content_id 	= $this->Product_model->addLanguage($main_id,$lang_id);

			$this->Product_model->setDefaultContent($main_id,$lang_id,$content_id);
			$this->Product_model->setDate($main_id);

			$data = array(
						'main_id'				=> $main_id,
						'lang_id'				=> $lang_id,
						'content_subject' 		=> $this->input->post("content_subject"),
						'content_short_subject'	=> $this->input->post("content_short_subject"),
						'content_youtube'		=> $this->input->post("content_youtube"),
						'content_detail'		=> preg_replace('/(<font[^>]*>)|(<\/font>)/','',$this->input->post("content_detail")),
						'content_short_detail'	=> $this->input->post("content_short_detail"),
						'content_title'			=> ($this->input->post("content_title") ? 
														$this->input->post("content_title") : 
															$this->input->post("content_subject")),
						'content_keyword'		=> $this->input->post("content_keyword"),
						'content_description'	=> $this->input->post("content_description"),
						'content_status'		=> $this->input->post("content_status"),
						'content_seo'			=> $this->input->post("content_subject"),
						'content_rewrite_id'	=> md5($this->menu['menu_id'].$main_id.time())
					);

			$checkUpdate = $this->Product_model->updateContent($data);

			// ############# Images Upload ############# //
				if($this->_data['img_thumbnail']) {

					$data['img_thumbnail'] = $this->_data['img_thumbnail'];
					if($data['img_thumbnail'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail'] as $thumbnail)
						{
							$this->Product_model->insertContent(
								$main_id,
								$thumbnail
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail'] as $thumbnail)
						// {
						// 	$this->Product_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

			// ############# Files Upload ############# //
				if($this->_data['file_thumbnail']) {

					$data['file_thumb'] = $this->_data['file_thumbnail'];
					if($data['file_thumb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['file_thumb'] as $thumbnail )
						{
							$this->Product_model->insertContent(
								$main_id,
								$thumbnail
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['file_thumb'] as $thumbnail )
						// {
						// 	$this->Product_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

			if(!$checkUpdate) {

				$this->session->set_flashdata("success_message","Content can't create.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content can't create",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->menu_backend_log($logs);

			} else {

				$this->session->set_flashdata("success_message","Content has been create.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content has been create",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->menu_backend_log($logs);

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					$seo = array(
								'lang_id'				=> '',//$data['lang_id'],
								'menu_link' 			=> $this->menu['menu_link'],
								'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
								'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
								'content_subject'		=> 'products/'.$data['content_seo'],
								'content_rewrite_id'	=> $data['content_rewrite_id']
							);

					$this->seo_library->rewrite_update($seo);

				admin_redirect($this->menu['menu_link']."/edit/".$main_id."/".$lang_id);
			}
		}
	}

	public function edit($main_id,$lang_id=NULL)
	{
		(empty($lang_id))?$lang_id 	= "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 	= $lang_id;
		$this->_data['main_id'] 	= $main_id;

		$this->_data['row'] = $this->Product_model->getDetail($main_id,$lang_id);

		if(!$this->_data['row']) {
			$this->Product_model->addLanguage($main_id,$lang_id);
			$this->_data['row'] = $this->Product_model->getDetail($main_id,$lang_id);
		}

		if(!$this->_data['row']) {
			show_error("ไม่พบข้อมูลในระบบ");
		}

		if($this->_data['row']['content_thumbnail']) {
			$this->_data['row']['content_thumbnail'] = $this->Product_model->getThumbnail_img($this->_data['row']['content_thumbnail'])
																			->row_array();
		}

		$this->admin_library->add_breadcrumb(
												$this->admin_library->getLanguagename($this->_data['row']['lang_id']),
												"Product/edit/".$this->_data['row']['main_id']."/".$this->_data['row']['lang_id'],
												"icon-globe"
											);

		$this->load->library('form_validation');
		$this->form_validation->set_rules("content_subject","Title","trim|required|max_length[255]");
		$this->form_validation->set_rules("content_detail","Description","trim");

		// เช็คอัพโหลดไฟล์
		$this->form_validation->set_rules("image_thumb[]","รูปภาพ","callback_fileupload_images");
		$this->form_validation->set_rules("file_thumb[]","อัพโหลดไฟล์","callback_fileupload_files");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']	= "Edit ".$this->menu['menu_label'];
			$this->_data['_menu_icon']	= "glyphicon-plus-sign";
			$this->_data['_menu_title']	= " แก้ไข".$this->menu['menu_title'];
			$this->_data['_menu_link']  = $this->menu['menu_link'];

			$this->_data['rs_img'] 	= $this->Product_model->getDetail_img($main_id);
			$this->_data['rs_file'] = $this->Product_model->getDetail_file($main_id);

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("product/edit",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$this->Product_model->setDate($main_id);

			$data = array(
						'main_id'				=> $main_id,
						'lang_id'				=> $lang_id,
						'content_subject' 		=> $this->input->post("content_subject"),
						'content_short_subject'	=> $this->input->post("content_short_subject"),
						'content_youtube'		=> $this->input->post("content_youtube"),
						'content_detail'		=> preg_replace('/(<font[^>]*>)|(<\/font>)/','',$this->input->post("content_detail")),
						'content_short_detail'	=> $this->input->post("content_short_detail"),
						'content_title'			=> $this->input->post("content_title"),
						'content_keyword'		=> $this->input->post("content_keyword"),
						'content_description'	=> $this->input->post("content_description"),
						'content_status'		=> $this->input->post("content_status"),
						'content_seo'			=> ($this->input->post("content_seo") ? 
														$this->input->post("content_seo") : 
															$this->input->post("content_subject")),
						'content_rewrite_id'	=> ($this->_data['row']['content_rewrite_id'] ? 
														$this->_data['row']['content_rewrite_id'] :
															md5($this->menu['menu_id'].$main_id.time()))
					);

			$checkUpdate = $this->Product_model->updateContent($data);

			// ############# Images Upload ############# //
				if($this->_data['img_thumbnail']) {

					$data['img_thumbnail'] = $this->_data['img_thumbnail'];
					if($data['img_thumbnail'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail'] as $thumbnail )
						{
							$this->Product_model->insertContent(
								$main_id,
								$thumbnail
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail'] as $thumbnail )
						// {
						// 	$this->Product_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				// Set Sequence Images
					$img_id 				= $this->input->post('attachment_id');
					$attachment_name 		= $this->input->post('attachment_name');
					$attachment_name_old 	= $this->input->post('attachment_name_old');
					$attachment_name_main 	= $this->input->post('attachment_name_main');
					$attachment_detail 		= $this->input->post('attachment_detail');

					if(is_array($img_id) && count($img_id)>0) {

						$i=0;
						foreach($img_id as $id) {

							$data_update = array(
								'attachment_id'		=> $id,
								'attachment_name' 	=> (!empty($attachment_name[$id]) ?
															($attachment_name_old[$id] ?
																$attachment_name[$id] :
															 		$attachment_name[$id]."_".$attachment_name_main[$id]) :
																		$attachment_name_main[$id]
														),
								'attachment_detail'	=> $attachment_detail[$id],
								// 'lang_id'			=> $lang_id
							);

							// Change Image Name
								if(!empty($attachment_name[$id]) || !empty($attachment_name_old[$id])) {

									$data_change_path_file = array(

										'attachment_name' 	=> (!empty($attachment_name[$id]) ?
																	($attachment_name_old[$id] ?
																		$attachment_name[$id] :
																	 		$attachment_name[$id]."_".$attachment_name_main[$id]) :
																				$attachment_name_main[$id]
																),
										'attachment_name_old'	=> $attachment_name_old[$id],
										'attachment_type'		=> $attachment_type[$id],
										'attachment_name_main'	=> $attachment_name_main[$id]
									);

									if( $this->change_path_file($data_change_path_file, 'images') ) {

										$this->Product_model->updateAttachmentContent($data_update);
									}
								} else {

									$this->Product_model->updateAttachmentContent($data_update);
								}

							$i++;
							$this->Product_model->setSequenceAttachment($id,$i);
						}
					}

			// ############# Files Upload ############# //
				if($this->_data['file_thumbnail']) {

					$data['file_thumb'] = $this->_data['file_thumbnail'];
					if($data['file_thumb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['file_thumb'] as $thumbnail )
						{
							$this->Product_model->insertContent(
								$main_id,
								$thumbnail
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['file_thumb'] as $thumbnail )
						// {
						// 	$this->Product_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

			if(!$checkUpdate) {

				$this->session->set_flashdata("success_message","Content can't delete.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content can't delete",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->menu_backend_log($logs);

			} else {

				$this->session->set_flashdata("success_message","Content has been delete.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content has been update",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->menu_backend_log($logs);

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					$seo = array(
								'lang_id'				=> '',// $data['lang_id'],
								'menu_link' 			=> $this->menu['menu_link'],
								'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
								'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
								'content_subject'		=> $data['content_seo'],
								'content_rewrite_id'	=> $data['content_rewrite_id']
							);

					$this->seo_library->rewrite_update($seo);

				admin_redirect($this->menu['menu_link']."/edit/".$main_id.'/'.$lang_id);
			}
		}
	}

	public function delete($main_id,$cate_id,$lang_id=null)
	{
		$data 			= $this->Product_model->getDetail($main_id);
		$checkUpdate 	= $this->Product_model->deleteContent($data);

		// Re-set Sequence after Delete content
			$queryAll 		= $this->Product_model->getAllContent($cate_id);
			$queryResult 	= $this->Product_model->getAllContent($cate_id)->result_array();

			for($i=0; $i<$queryAll->num_rows(); $i++) {

				$this->Product_model->setSequence($queryResult[$i]['main_id'], ($i+1));
			}

		if(!$checkUpdate) {

			$this->session->set_flashdata("success_message","Cant' delete centent.");

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Cant' delete centent",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->menu_backend_log($logs);

		} else {

			$this->session->set_flashdata("success_message","Delete centent success.");

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Delete centent success",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->menu_backend_log($logs);

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

				$seo = array(
							'lang_id'				=> '',//$data['lang_id'],
							'menu_link' 			=> $this->menu['menu_link'],
							'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
							'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
							'content_subject'		=> $data['content_seo'],
							'content_rewrite_id'	=> $data['content_rewrite_id']
						);

				$this->seo_library->rewrite_delete($seo);

			admin_redirect($this->menu['menu_link']."/index/".$cate_id."/".$lang_id);
		}
	}

	public function handle_delete($cate_id,$lang_id=null)
	{
		$data = array(
					'main_id' 	=> $this->input->post("main_id")
				);

		foreach($data['main_id'] as $id) {

			$data 			= $this->Product_model->getDetail($id);
			$this->Product_model->deleteContent($data);

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'delete',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Delete content success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

			$seo = array(
						'lang_id'				=> '',//$data['lang_id'],
						'menu_link' 			=> $this->menu['menu_link'],
						'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
						'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
						'content_subject'		=> $data['content_seo'],
						'content_rewrite_id'	=> $data['content_rewrite_id']
					);

			$this->seo_library->rewrite_delete($seo);
		}

		// Re-set Sequence after Delete content
			$queryAll = $this->Product_model->getAllContent($cate_id);
			$queryResult = $this->Product_model->getAllContent($cate_id)->result_array();

			for($i=0; $i<$queryAll->num_rows(); $i++) {

				$this->Product_model->setSequence($queryResult[$i]['main_id'], ($i+1));
			}

			$this->session->set_flashdata("success_message","Delete centent success.");

		admin_redirect($this->menu['menu_link']."/index/".$cate_id."/".$lang_id);
	}

	public function handle_suspend($cate_id,$lang_id=null)
	{
		$data = array(
					'main_id' 	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);

		foreach($data['main_id'] as $id) {
			$this->Product_model->setStatus($id,$data['lang_id'],"pending");
		}

		$this->session->set_flashdata("success_message","Update status success.");

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'update',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Update status success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);

		admin_redirect($this->menu['menu_link']."/index/".$cate_id."/".$lang_id);
	}

	public function handle_unsuspend($cate_id,$lang_id=null)
	{
		$data = array(
					'main_id'	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);

		foreach($data['main_id'] as $id) {
			$this->Product_model->setStatus($id,$data['lang_id'],"active");
		}

		$this->session->set_flashdata("success_message","Update status success.");

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'update',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Update status success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);

		admin_redirect($this->menu['menu_link']."/index/".$cate_id."/".$lang_id);
	}

	public function delete_img($attachment_id,$default_main_id,$attachment_name,$lang_id,$type_name)
	{
		$data = array(
					'attachment_id'		=> $attachment_id,
					'default_main_id'	=> $default_main_id,
					'attachment_name'	=> $attachment_name,
					'lang_id'			=> $lang_id,
					'type_name'			=> $type_name
				);

		if($type_name == "images") {
			$path = './public/uploads/product/images/'.$attachment_name;
		} else {
			$path = './public/uploads/product/files/'.$attachment_name;
		}

		$checkImage = $this->Product_model->deleteImage($attachment_id);

		if(!$checkImage) {

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Can't delete image",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->menu_backend_log($logs);

		} else {

			if(unlink($path)) {
				$this->session->set_flashdata("success_message","Delete file in Server success.");
			} else {
				$this->session->set_flashdata("error_message","Error! Can't delete file in Server.");
			}

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Delete image success",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->menu_backend_log($logs);

			admin_redirect($this->menu['menu_link']."/edit/".$default_main_id."/".$lang_id);
		}
	}

	public function set_default_img($default_main_id,$content_id,$attachment_id,$lang_id)
	{
		$this->Product_model->setDefaultImg($content_id,$attachment_id,$lang_id);
		admin_redirect($this->menu['menu_link']."/edit/".$default_main_id."/".$lang_id);
	}

	// ################### Category Setting ######################

	public function category($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= "Category";
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');

		// ---------- Tools Sequent ---------- //
			$seq 		= $this->input->post('seq');
			$content_id = $this->input->post('content_id');
			if($content_id) {

				foreach ($content_id as $key => $value) {

					$this->Product_model->setSequenceCategory($key, $value);
				}
			}

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->menu['menu_icon']);
		$this->admin_library->setDetail($this->menu['menu_title']);
		$this->admin_library->view("product/listview_category",$this->_data);
		$this->admin_library->output($this->path);
	}

	public function load_datatable_category($lang_id="TH")
	{
		$this->form_validation->set_rules("length","Length","trim|required|integer");
		$this->form_validation->set_rules("start","Start","trim|required|integer");

		if($this->form_validation->run()===false) {

			echo json_encode([]);
		} else {
		
			$limit 			= $this->input->post('length');
			$start 			= $this->input->post('start');
			$is_order 		= array();
			$is_search 		= array();
			$result_array 	= array();

			// ####### Column all ####### //
				if(!empty($this->input->post('columns'))) {

					foreach ($this->input->post('columns') as $key => $column) {

						$is_order[$key] = $column['data'];

						if($column['search']['value']) {

							$is_search[$column['data']] 	= $column['search']['value'];
						}
					}
				}

			// ####### Orderable ####### //
				if(!empty($this->input->post('order')[0]['column'])) {

					$is_order = array($is_order[$this->input->post('order')[0]['column']] => $this->input->post('order')[0]['dir']);
				} else {

					$is_order = array();
				}

			// ####### Searchable ####### //
				if(empty($is_search)) {

					$all_data 		= $this->Product_model->dataTableCategory($lang_id);
		            $query_data 	= $this->Product_model->dataTableCategory($lang_id, $limit, $start, $is_order)
		            															->result_array();
		        } else {

					$all_data 		= $this->Product_model->dataTableCategory($lang_id, null, null, null, $is_search);
					$query_data 	= $this->Product_model->dataTableCategory($lang_id, $limit, $start, $is_order, $is_search)
																				->result_array();
				}

			if(!empty($query_data)) {

				foreach ($query_data as $key => $value) {
					$result_array[$key] = $value;
					$result_array[$key]['no'] = ($key+1);
					$result_array[$key]['post_date_format'] = date_format_convert_shortdatetime($value['post_date']);
				}
			}

			$output = array(
							"draw"            	=> intval($this->input->post('draw')),
							"recordsFiltered"   => intval($all_data),
							"recordsTotal" 		=> count($result_array),
							"data"            	=> $result_array
						);

			echo json_encode($output);
		}
	}

	public function add_category($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id']=$lang_id;

		$this->form_validation->set_rules("content_subject","Title","trim|required|max_length[255]");
		// $this->form_validation->set_rules("content_detail","Description","trim|required");

		// เช็คอัพโหลดไฟล์
		$this->form_validation->set_rules("image_thumb[]","Pictures","callback_fileupload_images");
		$this->form_validation->set_rules("image_thumb_mb[]","Pictures Mobile","callback_fileupload_images_mb");
		$this->form_validation->set_rules("image_thumb_hl[]","Pictures Highlight","callback_fileupload_images_hl");
		// $this->form_validation->set_rules("file_thumb[]","Files Attachment","callback_fileupload_files");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']			= "Add ".$this->menu['menu_label'];
			$this->_data['_menu_icon']			= "glyphicon-plus-sign";
			$this->_data['_menu_title']			= " เพิ่ม".$this->menu['menu_title'];
			$this->_data['_menu_link']  		= $this->menu['menu_link'];

			$this->_data['validation_errors'] 	= validation_errors();
			$this->_data['error_message'] 		= $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("product/add_category",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$main_id 		= $this->Product_model->addDataCategory();
			$content_id 	= $this->Product_model->addLanguageCategory($main_id,$lang_id);

			$this->Product_model->setDefaultContentCategory($main_id,$lang_id,$content_id);
			$this->Product_model->setDateCategory($main_id);

			$data = array(
						'main_id'				=> $main_id,
						'lang_id'				=> $lang_id,
						'content_subject' 		=> $this->input->post("content_subject"),
						'content_detail'		=> $this->input->post("content_detail"),
						'content_title'			=> ($this->input->post("content_title") ? 
														$this->input->post("content_title") : 
															$this->input->post("content_subject")),
						'content_keyword'		=> $this->input->post("content_keyword"),
						'content_description'	=> $this->input->post("content_description"),
						'content_status'		=> $this->input->post("content_status"),
						'content_seo'			=> $this->input->post("content_subject"),
						'content_rewrite_id'	=> md5($this->menu['menu_id'].$main_id.time())
					);

			$checkUpdate = $this->Product_model->updateContentCategory($data);

			// ############# Images Upload ############# //
				if($this->_data['img_thumbnail']) {

					$data['img_thumbnail'] = $this->_data['img_thumbnail'];
					if($data['img_thumbnail'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail'] as $thumbnail)
						{
							$this->Product_model->insertContentCategory(
								$main_id,
								$thumbnail,
								'pc'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail'] as $thumbnail)
						// {
						// 	$this->Product_model->insertContentCategory(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				if($this->_data['img_thumbnail_mb']) {

					$data['img_thumbnail_mb'] = $this->_data['img_thumbnail_mb'];
					if($data['img_thumbnail_mb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail_mb'] as $thumbnail)
						{
							$this->Product_model->insertContentCategory(
								$main_id,
								$thumbnail,
								'mb'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail'] as $thumbnail)
						// {
						// 	$this->Product_model->insertContentCategory(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				if($this->_data['img_thumbnail_hl']) {

					$data['img_thumbnail_hl'] = $this->_data['img_thumbnail_hl'];
					if($data['img_thumbnail_hl'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail_hl'] as $thumbnail)
						{
							$this->Product_model->insertContentCategory(
								$main_id,
								$thumbnail,
								'hl'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail'] as $thumbnail)
						// {
						// 	$this->Product_model->insertContentCategory(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

			// ############# Files Upload ############# //
				// if($this->_data['file_thumbnail']) {

				// 	$data['file_thumb'] = $this->_data['file_thumbnail'];
				// 	if($data['file_thumb'])
				// 	{
				// 		// อัพโหลดครั้งเดียวทุกภาษา
				// 		foreach($data['file_thumb'] as $thumbnail )
				// 		{
				// 			$this->Product_model->insertContentCategory(
				// 				$main_id,
				// 				$thumbnail
				// 			);
				// 		}
				// 		// อัพโหลดภาษาละครั้ง
				// 		// foreach($data['file_thumb'] as $thumbnail )
				// 		// {
				// 		// 	$this->Product_model->insertContentCategory(
				// 		// 		$main_id,
				// 		// 		$thumbnail,
				// 		// 		$lang_id
				// 		// 	);
				// 		// }
				// 	}
				// }

			if(!$checkUpdate) {

				$this->session->set_flashdata("success_message","Content can't create.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content can't create",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->menu_backend_log($logs);

			} else {

				$this->session->set_flashdata("success_message","Content has been create.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content has been create",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->menu_backend_log($logs);

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					$seo = array(
								'lang_id'				=> '',//$data['lang_id'],
								'menu_link' 			=> $this->menu['menu_link']."_category",
								'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_category",
								'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
								'content_subject'		=> 'products/'.$data['content_seo'],
								'content_rewrite_id'	=> $data['content_rewrite_id']
							);

					$this->seo_library->rewrite_update($seo);

				admin_redirect($this->menu['menu_link']."/edit_category/".$main_id."/".$lang_id);
			}
		}
	}

	public function edit_category($main_id,$lang_id=NULL)
	{
		(empty($lang_id))?$lang_id 	= "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 	= $lang_id;
		$this->_data['main_id'] 	= $main_id;

		$this->_data['row'] = $this->Product_model->getDetailCategory($main_id,$lang_id);

		if(!$this->_data['row']) {
			$this->Product_model->addLanguageCategory($main_id,$lang_id);
			$this->_data['row'] = $this->Product_model->getDetailCategory($main_id,$lang_id);
		}

		if(!$this->_data['row']) {
			show_error("ไม่พบข้อมูลในระบบ");
		}

		if($this->_data['row']['content_thumbnail']) {
			$this->_data['row']['content_thumbnail'] = $this->Product_model->getThumbnailCategory_img($this->_data['row']['content_thumbnail'])
																			->row_array();
		}

		$this->admin_library->add_breadcrumb(
												$this->admin_library->getLanguagename($this->_data['row']['lang_id']),
												"Product/edit_category/".$this->_data['row']['main_id']."/".$this->_data['row']['lang_id'],
												"icon-globe"
											);

		$this->load->library('form_validation');
		$this->form_validation->set_rules("content_subject","Title","trim|required|max_length[255]");
		// $this->form_validation->set_rules("content_detail","รายละเอียด","trim|required");

		// เช็คอัพโหลดไฟล์
		$this->form_validation->set_rules("image_thumb[]","Pictures","callback_fileupload_images");
		$this->form_validation->set_rules("image_thumb_mb[]","Pictures Mobile","callback_fileupload_images_mb");
		$this->form_validation->set_rules("image_thumb_hl[]","Pictures Highlight","callback_fileupload_images_hl");
		// $this->form_validation->set_rules("file_thumb[]","อัพโหลดไฟล์","callback_fileupload_files");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']	= "Edit ".$this->menu['menu_label'];
			$this->_data['_menu_icon']	= "glyphicon-plus-sign";
			$this->_data['_menu_title']	= " แก้ไข".$this->menu['menu_title'];
			$this->_data['_menu_link']  = $this->menu['menu_link'];

			$this->_data['rs_img'] 	= $this->Product_model->getDetailCategory_img($main_id);
			$this->_data['rs_file'] = $this->Product_model->getDetailCategory_file($main_id);

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("product/edit_category",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$this->Product_model->setDateCategory($main_id);

			$data = array(
						'main_id'				=> $main_id,
						'lang_id'				=> $lang_id,
						'content_subject' 		=> $this->input->post("content_subject"),
						'content_detail'		=> $this->input->post("content_detail"),
						'content_title'			=> $this->input->post("content_title"),
						'content_keyword'		=> $this->input->post("content_keyword"),
						'content_description'	=> $this->input->post("content_description"),
						'content_status'		=> $this->input->post("content_status"),
						'content_seo'			=> ($this->input->post("content_seo") ? 
														$this->input->post("content_seo") : 
															$this->input->post("content_subject")),
						'content_rewrite_id'	=> ($this->_data['row']['content_rewrite_id'] ? 
														$this->_data['row']['content_rewrite_id'] :
															md5($this->menu['menu_id'].$main_id.time()))
					);

			$checkUpdate = $this->Product_model->updateContentCategory($data);

			// ############# Images Upload ############# //
				if($this->_data['img_thumbnail']) {

					$data['img_thumbnail'] = $this->_data['img_thumbnail'];
					if($data['img_thumbnail'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail'] as $thumbnail )
						{
							$this->Product_model->insertContentCategory(
								$main_id,
								$thumbnail,
								'pc'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail'] as $thumbnail )
						// {
						// 	$this->Product_model->insertContentCategory(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				if($this->_data['img_thumbnail_mb']) {

					$data['img_thumbnail_mb'] = $this->_data['img_thumbnail_mb'];
					if($data['img_thumbnail_mb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail_mb'] as $thumbnail )
						{
							$this->Product_model->insertContentCategory(
								$main_id,
								$thumbnail,
								'mb'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail_mb'] as $thumbnail )
						// {
						// 	$this->Product_model->insertContentCategory(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				if($this->_data['img_thumbnail_hl']) {

					$data['img_thumbnail_hl'] = $this->_data['img_thumbnail_hl'];
					if($data['img_thumbnail_hl'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail_hl'] as $thumbnail )
						{
							$this->Product_model->insertContentCategory(
								$main_id,
								$thumbnail,
								'hl'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail_mb'] as $thumbnail )
						// {
						// 	$this->Product_model->insertContentCategory(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				// Set Sequence Images
					$img_id 				= $this->input->post('attachment_id');
					$attachment_name 		= $this->input->post('attachment_name');
					$attachment_name_old 	= $this->input->post('attachment_name_old');
					$attachment_name_main 	= $this->input->post('attachment_name_main');
					$attachment_detail 		= $this->input->post('attachment_detail');

					if(is_array($img_id) && count($img_id)>0) {

						$i=0;
						foreach($img_id as $id) {

							$data_update = array(
								'attachment_id'		=> $id,
								'attachment_name' 	=> (!empty($attachment_name[$id]) ?
															($attachment_name_old[$id] ?
																$attachment_name[$id] :
															 		$attachment_name[$id]."_".$attachment_name_main[$id]) :
																		$attachment_name_main[$id]
														),
								'attachment_detail'	=> $attachment_detail[$id],
								// 'lang_id'			=> $lang_id
							);

							// Change Image Name
								if(!empty($attachment_name[$id]) || !empty($attachment_name_old[$id])) {

									$data_change_path_file = array(

										'attachment_name' 	=> (!empty($attachment_name[$id]) ?
																	($attachment_name_old[$id] ?
																		$attachment_name[$id] :
																	 		$attachment_name[$id]."_".$attachment_name_main[$id]) :
																				$attachment_name_main[$id]
																),
										'attachment_name_old'	=> $attachment_name_old[$id],
										'attachment_type'		=> $attachment_type[$id],
										'attachment_name_main'	=> $attachment_name_main[$id]
									);

									if( $this->change_path_file($data_change_path_file, 'images') ) {

										$this->Product_model->updateAttachmentContentCategory($data_update);
									}
								} else {

									$this->Product_model->updateAttachmentContentCategory($data_update);
								}

							$i++;
							$this->Product_model->setSequenceAttachmentCategory($id,$i);
						}
					}

			// ############# Files Upload ############# //
				// if($this->_data['file_thumbnail']) {

				// 	$data['file_thumb'] = $this->_data['file_thumbnail'];
				// 	if($data['file_thumb'])
				// 	{
				// 		// อัพโหลดครั้งเดียวทุกภาษา
				// 		foreach($data['file_thumb'] as $thumbnail )
				// 		{
				// 			$this->Product_model->insertContentCategory(
				// 				$main_id,
				// 				$thumbnail
				// 			);
				// 		}
				// 		// อัพโหลดภาษาละครั้ง
				// 		// foreach($data['file_thumb'] as $thumbnail )
				// 		// {
				// 		// 	$this->Product_model->insertContentCategory(
				// 		// 		$main_id,
				// 		// 		$thumbnail,
				// 		// 		$lang_id
				// 		// 	);
				// 		// }
				// 	}
				// }

			if(!$checkUpdate) {

				$this->session->set_flashdata("success_message","Content can't delete.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content can't delete",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->menu_backend_log($logs);

			} else {

				$this->session->set_flashdata("success_message","Content has been delete.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content has been update",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->menu_backend_log($logs);

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					$seo = array(
								'lang_id'				=> '',//$data['lang_id'],
								'menu_link' 			=> $this->menu['menu_link']."_category",
								'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_category",
								'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
								'content_subject'		=> $data['content_seo'],
								'content_rewrite_id'	=> $data['content_rewrite_id']
							);

					$this->seo_library->rewrite_update($seo);

				admin_redirect($this->menu['menu_link']."/edit_category/".$main_id.'/'.$lang_id);
			}
		}
	}

	public function delete_category($main_id,$lang_id=null)
	{
		$data 			= $this->Product_model->getDetailCategory($main_id);
		$checkUpdate 	= $this->Product_model->deleteContentCategory($data);

		// Re-set Sequence after Delete content
			$queryAll 		= $this->Product_model->getAllContentCategory();
			$queryResult 	= $this->Product_model->getAllContentCategory()->result_array();

			for($i=0; $i<$queryAll->num_rows(); $i++) {

				$this->Product_model->setSequenceCategory($queryResult[$i]['main_id'], ($i+1));
			}

		if(!$checkUpdate) {

			$this->session->set_flashdata("success_message","Cant' delete centent.");

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Cant' delete centent",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->menu_backend_log($logs);

		} else {

			$this->session->set_flashdata("success_message","Delete centent success.");

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Delete centent success",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->menu_backend_log($logs);

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

				$seo = array(
							'lang_id'				=> '',//$data['lang_id'],
							'menu_link' 			=> $this->menu['menu_link']."_category",
							'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_category",
							'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
							'content_subject'		=> $data['content_seo'],
							'content_rewrite_id'	=> $data['content_rewrite_id']
						);

				$this->seo_library->rewrite_delete($seo);

			admin_redirect($this->menu['menu_link']."/category/".$lang_id);
		}
	}

	public function handle_delete_category($lang_id=null)
	{
		$data = array(
					'main_id' 	=> $this->input->post("main_id")
				);

		foreach($data['main_id'] as $id) {

			$data 			= $this->Product_model->getDetailCategory($id);
			$this->Product_model->deleteContentCategory($data);

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'delete',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Delete content success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

			$seo = array(
						'lang_id'				=> '',//$data['lang_id'],
						'menu_link' 			=> $this->menu['menu_link']."_category",
						'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_category",
						'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
						'content_subject'		=> $data['content_seo'],
						'content_rewrite_id'	=> $data['content_rewrite_id']
					);

			$this->seo_library->rewrite_delete($seo);
		}

		// Re-set Sequence after Delete content
			$queryAll = $this->Product_model->getAllContentCategory();
			$queryResult = $this->Product_model->getAllContentCategory()->result_array();

			for($i=0; $i<$queryAll->num_rows(); $i++) {

				$this->Product_model->setSequenceCategory($queryResult[$i]['main_id'], ($i+1));
			}

			$this->session->set_flashdata("success_message","Delete centent success.");

		admin_redirect($this->menu['menu_link']."/category/".$lang_id);
	}

	public function handle_suspend_category($lang_id=null)
	{
		$data = array(
					'main_id' 	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);

		foreach($data['main_id'] as $id) {
			$this->Product_model->setStatusCategory($id,$data['lang_id'],"pending");
		}

		$this->session->set_flashdata("success_message","Update status success.");

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'update',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Update status success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);

		admin_redirect($this->menu['menu_link']."/category/".$lang_id);
	}

	public function handle_unsuspend_category($lang_id=null)
	{
		$data = array(
					'main_id'	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);

		foreach($data['main_id'] as $id) {
			$this->Product_model->setStatusCategory($id,$data['lang_id'],"active");
		}

		$this->session->set_flashdata("success_message","Update status success.");

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'update',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Update status success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);

		admin_redirect($this->menu['menu_link']."/category/".$lang_id);
	}

	public function delete_img_category($attachment_id,$default_main_id,$attachment_name,$lang_id,$type_name)
	{
		$data = array(
					'attachment_id'		=> $attachment_id,
					'default_main_id'	=> $default_main_id,
					'attachment_name'	=> $attachment_name,
					'lang_id'			=> $lang_id,
					'type_name'			=> $type_name
				);

		if($type_name == "images") {
			$path = './public/uploads/product/images/'.$attachment_name;
		} else {
			$path = './public/uploads/product/files/'.$attachment_name;
		}

		$checkImage = $this->Product_model->deleteImageCategory($attachment_id);

		if(!$checkImage) {

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Can't delete image",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->menu_backend_log($logs);

		} else {

			if(unlink($path)) {
				$this->session->set_flashdata("success_message","Delete file in Server success.");
			} else {
				$this->session->set_flashdata("error_message","Error! Can't delete file in Server.");
			}

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Delete image success",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->menu_backend_log($logs);

			admin_redirect($this->menu['menu_link']."/edit_category/".$default_main_id."/".$lang_id);
		}
	}

	// ################### Check uploads Images. ######################

	public function fileupload_images()
	{
		$this->_data['img_thumbnail'] = array();
		$number_of_files 	= sizeof($_FILES['image_thumb']['tmp_name']);
		$remove_of_files 	= ( @$this->input->post("images") ? explode(',', $this->input->post("images")) : [] );
		$files 				= $_FILES['image_thumb'];
		$files_limit 		= 12;
		$main_id 			= @$this->_data['main_id'];
		$lang_id 			= $this->_data['lang_id'];

		// ถ้าต้องการเช็คอัพโหลดรูป
		// if($files['tmp_name']) {
		//     for($i=0;$i<$number_of_files;$i++)
		//     {
		//       if($_FILES['image_thumb']['error'][$i] != 0)
		//       {
		//         $this->form_validation->set_message('fileupload_images', 'The Upload Pictures field is required.');
		//         return FALSE;
		//       }
		//     }
		// }

		// ถ้าไม่ต้องการเช็คอัพโหลดรูป
		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		} else {

			return TRUE;
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/product/images';
		$config['encrypt_name']		= true;
		$config['allowed_types'] 	= 'gif|jpg|jpeg|png';
		$config['max_size']			= '5024';
		$config['max_width']  		= '2000';
		$config['max_height']  		= '1600';

		if($main_id) {
			$count_images = $this->Product_model->getDetailCategory_img($main_id,'pc')->num_rows();
		} else {
			$count_images = 0;
		}

		for ($i = 0; $i < $number_of_files; $i++) {

			if(!in_array($i, $remove_of_files)) {

				$_FILES['image_thumb']['name'] 		= $files['name'][$i];
				$_FILES['image_thumb']['type'] 		= $files['type'][$i];
				$_FILES['image_thumb']['tmp_name'] 	= $files['tmp_name'][$i];
				$_FILES['image_thumb']['error'] 	= $files['error'][$i];
				$_FILES['image_thumb']['size'] 		= $files['size'][$i];

				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_thumb')) {

					$data  								= $this->upload->data();
					$this->_data['img_thumbnail'][] 	= $data['file_name'];

					// เช็คจำนวนอัพโหลดมากสุด
					if((count($this->_data['img_thumbnail'])+$count_images) > $files_limit) {

						$this->form_validation->set_message('fileupload_images', 'The Upload Pictures limit '.$files_limit);
						return FALSE;
					}

				} else {

					$this->form_validation->set_message('fileupload_images', $this->upload->display_errors());
					return FALSE;
				}
			}
		}

		return TRUE;
	}

	public function fileupload_images_mb()
	{
		$this->_data['img_thumbnail_mb'] = array();
		$number_of_files 	= sizeof($_FILES['image_thumb_mb']['tmp_name']);
		$remove_of_files 	= ( @$this->input->post("images_mb") ? explode(',', $this->input->post("images_mb")) : [] );
		$files 				= $_FILES['image_thumb_mb'];
		$files_limit 		= 1;
		$main_id 			= @$this->_data['main_id'];
		$lang_id 			= $this->_data['lang_id'];

		// ถ้าต้องการเช็คอัพโหลดรูป
		// if($files['tmp_name']) {
		//     for($i=0;$i<$number_of_files;$i++)
		//     {
		//       if($_FILES['image_thumb_mb']['error'][$i] != 0)
		//       {
		//         $this->form_validation->set_message('fileupload_images_mb', 'The Upload Pictures field is required.');
		//         return FALSE;
		//       }
		//     }
		// }

		// ถ้าไม่ต้องการเช็คอัพโหลดรูป
		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		} else {

			return TRUE;
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/product/images';
		$config['encrypt_name']		= true;
		$config['allowed_types'] 	= 'gif|jpg|jpeg|png';
		$config['max_size']			= '5024';
		$config['max_width']  		= '640';
		$config['max_height']  		= '926';

		if($main_id) {
			$count_images = $this->Product_model->getDetailCategory_img($main_id,'mb')->num_rows();
		} else {
			$count_images = 0;
		}

		for ($i = 0; $i < $number_of_files; $i++) {

			if(!in_array($i, $remove_of_files)) {

				$_FILES['image_thumb_mb']['name'] 		= $files['name'][$i];
				$_FILES['image_thumb_mb']['type'] 		= $files['type'][$i];
				$_FILES['image_thumb_mb']['tmp_name'] 	= $files['tmp_name'][$i];
				$_FILES['image_thumb_mb']['error'] 	= $files['error'][$i];
				$_FILES['image_thumb_mb']['size'] 		= $files['size'][$i];

				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_thumb_mb')) {

					$data  								= $this->upload->data();
					$this->_data['img_thumbnail_mb'][] 	= $data['file_name'];

					// เช็คจำนวนอัพโหลดมากสุด
					if((count($this->_data['img_thumbnail_mb'])+$count_images) > $files_limit) {

						$this->form_validation->set_message('fileupload_images_mb', 'The Upload Pictures Mobile limit '.$files_limit);
						return FALSE;
					}

				} else {

					$this->form_validation->set_message('fileupload_images_mb', $this->upload->display_errors());
					return FALSE;
				}
			}
		}

		return TRUE;
	}

	public function fileupload_images_hl()
	{
		$this->_data['img_thumbnail_hl'] = array();
		$number_of_files 	= sizeof($_FILES['image_thumb_hl']['tmp_name']);
		$remove_of_files 	= ( @$this->input->post("images_hl") ? explode(',', $this->input->post("images_hl")) : [] );
		$files 				= $_FILES['image_thumb_hl'];
		$files_limit 		= 1;
		$main_id 			= @$this->_data['main_id'];
		$lang_id 			= $this->_data['lang_id'];

		// ถ้าต้องการเช็คอัพโหลดรูป
		// if($files['tmp_name']) {
		//     for($i=0;$i<$number_of_files;$i++)
		//     {
		//       if($_FILES['image_thumb_hl']['error'][$i] != 0)
		//       {
		//         $this->form_validation->set_message('fileupload_images_hl', 'The Upload Pictures field is required.');
		//         return FALSE;
		//       }
		//     }
		// }

		// ถ้าไม่ต้องการเช็คอัพโหลดรูป
		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		} else {

			return TRUE;
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/product/images';
		$config['encrypt_name']		= true;
		$config['allowed_types'] 	= 'gif|jpg|jpeg|png';
		$config['max_size']			= '5024';
		$config['max_width']  		= '760';
		$config['max_height']  		= '480';

		if($main_id) {
			$count_images = $this->Product_model->getDetailCategory_img($main_id,'mb')->num_rows();
		} else {
			$count_images = 0;
		}

		for ($i = 0; $i < $number_of_files; $i++) {

			if(!in_array($i, $remove_of_files)) {

				$_FILES['image_thumb_hl']['name'] 		= $files['name'][$i];
				$_FILES['image_thumb_hl']['type'] 		= $files['type'][$i];
				$_FILES['image_thumb_hl']['tmp_name'] 	= $files['tmp_name'][$i];
				$_FILES['image_thumb_hl']['error'] 	= $files['error'][$i];
				$_FILES['image_thumb_hl']['size'] 		= $files['size'][$i];

				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_thumb_hl')) {

					$data  								= $this->upload->data();
					$this->_data['img_thumbnail_hl'][] 	= $data['file_name'];

					// เช็คจำนวนอัพโหลดมากสุด
					if((count($this->_data['img_thumbnail_hl'])+$count_images) > $files_limit) {

						$this->form_validation->set_message('fileupload_images_hl', 'The Upload Pictures Mobile limit '.$files_limit);
						return FALSE;
					}

				} else {

					$this->form_validation->set_message('fileupload_images_hl', $this->upload->display_errors());
					return FALSE;
				}
			}
		}

		return TRUE;
	}

	// ################### Check uploads Images. ######################

	public function fileupload_files()
	{
		$this->_data['file_thumbnail'] 	= array();
		$number_of_files 				= sizeof($_FILES['file_thumb']['tmp_name']);
		$files 							= $_FILES['file_thumb'];

	    // ถ้าต้องการเช็คอัพโหลดรูป
		// if($files['tmp_name']) {
		//     for($i=0;$i<$number_of_files;$i++)
		//     {
		//       if($_FILES['file_thumb']['error'][$i] != 0)
		//       {
		//         $this->form_validation->set_message('fileupload_files', 'The Upload Files Attachment field is required.');
		//         return FALSE;
		//       }
		//     }
		// }

		// ถ้าไม่ต้องการเช็คอัพโหลดไฟล์
		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		} else {

			return TRUE;
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/product/files';
		$config['allowed_types'] 	= 'pdf|doc|docx|rar|zip|';
		$config['max_size']			= 204800;
		$config['encrypt_name'] 	= TRUE;

		for ($i = 0; $i < $number_of_files; $i++) {

			$_FILES['file_thumb']['name'] 		= $files['name'][$i];
			$_FILES['file_thumb']['type'] 		= $files['type'][$i];
			$_FILES['file_thumb']['tmp_name'] 	= $files['tmp_name'][$i];
			$_FILES['file_thumb']['error'] 		= $files['error'][$i];
			$_FILES['file_thumb']['size'] 		= $files['size'][$i];

			$this->upload->initialize($config);
			if ($this->upload->do_upload('file_thumb')) {

				$data  								= $this->upload->data();
				$this->_data['file_thumbnail'][] 	= $data['file_name'];

		      	// เช็คจำนวนอัพโหลดมากสุด
				if(count($this->_data['file_thumbnail']) > 1) {

					$this->form_validation->set_message('fileupload_files', 'The Upload Files Attachment limit 1');
					return FALSE;
				}
			} else {

				$this->form_validation->set_message('fileupload_files', $this->upload->display_errors());
				return FALSE;
			}
		}

	    return TRUE;
	}

	// ################### Change path & name File. ######################

	private function change_path_file(array $data = array(), $type="images")
	{
		$attachment_new 	= ($data['attachment_name'] ? 
										$data['attachment_name'] : 
											$data['attachment_name_old']);
		$attachment_old 	= ($data['attachment_name_old'] ? 
										$data['attachment_name_old'] : 
											$data['attachment_name_main']);

		$upload_path_new 	= './public/uploads/product/'.$type.'/'.$attachment_new;
		$upload_path_main 	= './public/uploads/product/'.$type.'/'.$attachment_old;

		if(file_exists($upload_path_main)) {

			return rename($upload_path_main, $upload_path_new);
		} else {

			return false;
		}
	}

	// ################### SEO Setting View & Update ######################

	public function seo_setting($action=null,$main_id=null,$lang_id=null)
	{
		(empty($lang_id))?$lang_id 	= "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 	= $lang_id;
		$this->_data['main_id'] 	= $main_id;

		$this->_data['row'] = $this->language_setting_model->getDetail($main_id)->row_array();

		if(!$this->_data['row']) {
			show_error("ไม่พบข้อมูลในระบบ");
		}

		$list_language = $this->language_setting_model->action_language('list');
		if(!empty($list_language)) {
			$this->_data['row']['language'] = $list_language->result_array();
		} else {
			$this->_data['row']['language'] = null;
		}

		$data = ['label' => strtolower($this->_data['row']['seo_name'])];
		$list_translate = $this->language_setting_model->action_translate('list', $data);

		if(!empty($list_translate)) {
			$this->_data['row']['translate'] = $list_translate['translation_keys'];
		} else {
			$this->_data['row']['translate'] = null;
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules("seo_name","Menu name","trim|required|max_length[255]");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']	= "Edit ".$this->menu['menu_label'];
			$this->_data['_menu_icon']	= "glyphicon-plus-sign";
			$this->_data['_menu_title']	= " แก้ไข".$this->menu['menu_title'];
			$this->_data['_menu_link']  = $this->menu['menu_link'];

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("seosetting/edit",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			// Update SEO content
				$data = [
							'id' 				=> $main_id,
							'seo_name' 			=> $this->input->post('seo_name'),
							'seo_tracking_id' 	=> $this->input->post('seo_tracking_id'),
							'seo_tracking_code' => $this->input->post('seo_tracking_code'),
							'seo_code' 			=> $this->input->post('seo_code'),
							'seo_status' 		=> $this->input->post('seo_status')
						];

				$this->language_setting_model->updateContent($data);

			// Setting Key language
				if($this->input->post('label')) {

					$data_translate = [
								'label'			=> strtolower($this->input->post('seo_name'))."_".$this->input->post('label'),
								'language_id'	=> $this->input->post('language_id'),
								'value'			=> $this->input->post('value')
							];

					$this->language_setting_model->action_translate('insert', $data_translate);
				}

			// Update Value language
				if(!empty($this->input->post('label_update'))) {

					foreach ($this->input->post('label_update') as $key => $value) {
						
						$data_update[$key] = [
									'label' 		=> $value,
									'value' 		=> $this->input->post('value_update')
						];
					}

					$this->language_setting_model->action_translate('update', $data_update);
				}

			admin_redirect($this->menu['menu_link']."/seo_setting/".$action."/".$main_id.'/'.$lang_id);
		}
	}

	public function delete_key($main_id=null, $key_id=null, $lang_id="TH")
	{
		if($main_id && $key_id) {

			$data = [ 'id' => $key_id ];
			$this->language_setting_model->action_translate('delete', $data);

			$this->session->set_flashdata("success_message","Delete centent success.");
			admin_redirect($this->menu['menu_link']."/seo_setting/edit/".$main_id.'/'.$lang_id);
		}
	}
} 
?>