<?php 
class Age_verification extends CI_Controller {

	var $_data = array(),
		$_menu_name,
		$menu,
		$submenu;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('admin_library');
		$this->admin_library->forceLogin();
		$this->load->model('Age_verification_model');
		$this->load->library('form_validation');

		$this->path 	= 	$this->uri->ruri_string();
		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(1));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(1), $this->uri->segment(2));
	}

	public function index($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');

		// ---------- Tools Sequent ---------- //
			$seq 		= $this->input->post('seq');
			$content_id = $this->input->post('content_id');
			if($content_id) {

				foreach ($content_id as $key => $value) {

					$this->Age_verification_model->setSequence($key, $value);
				}
			}

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->menu['menu_icon']);
		$this->admin_library->setDetail($this->menu['menu_title']);
		$this->admin_library->view("age_verification/listview",$this->_data);
		$this->admin_library->output($this->path);
	}

	public function load_datatable($lang_id="TH")
	{
		$this->form_validation->set_rules("length","Length","trim|required|integer");
		$this->form_validation->set_rules("start","Start","trim|required|integer");

		if($this->form_validation->run()===false) {

			echo json_encode([]);
		} else {
		
			$limit 			= $this->input->post('length');
			$start 			= $this->input->post('start');
			$is_order 		= array();
			$is_search 		= array();
			$result_array 	= array();

			// ####### Column all ####### //
				if(!empty($this->input->post('columns'))) {

					foreach ($this->input->post('columns') as $key => $column) {

						$is_order[$key] = $column['data'];

						if($column['search']['value']) {

							$is_search[$column['data']] 	= $column['search']['value'];
						}
					}
				}

			// ####### Orderable ####### //
				if(!empty($this->input->post('order')[0]['column'])) {

					$is_order = array($is_order[$this->input->post('order')[0]['column']] => $this->input->post('order')[0]['dir']);
				} else {

					$is_order = array();
				}

			// ####### Searchable ####### //
				if(empty($is_search)) {

					$all_data 		= $this->Age_verification_model->dataTable($lang_id);
		            $query_data 	= $this->Age_verification_model->dataTable($lang_id, $limit, $start, $is_order)
		            															->result_array();
		        } else {

					$all_data 		= $this->Age_verification_model->dataTable($lang_id, null, null, null, $is_search);
					$query_data 	= $this->Age_verification_model->dataTable($lang_id, $limit, $start, $is_order, $is_search)
																				->result_array();
				}

			if(!empty($query_data)) {

				foreach ($query_data as $key => $value) {
					$result_array[$key] = $value;
					$result_array[$key]['no'] = ($key+1)+$start;
					$timestamp = date('Y-m-d H:i:s', $value['timestamp']);
					$result_array[$key]['post_date_format'] = date_format_convert_shortdatetime($timestamp);
				}
			}

			$output = array(
							"draw"            	=> intval($this->input->post('draw')),
							"recordsFiltered"   => intval($all_data),
							"recordsTotal" 		=> count($result_array),
							"data"            	=> $result_array
						);

			echo json_encode($output);
		}
	}
} 
?>