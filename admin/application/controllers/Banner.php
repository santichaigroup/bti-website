<?php 
class Banner extends CI_Controller {

	var $_data = array(),
		$_menu_name,
		$menu,
		$submenu;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('admin_library');
		$this->admin_library->forceLogin();
		$this->load->model('Banner_model');
		$this->load->library('form_validation');

		$this->path 	= 	$this->uri->ruri_string();
		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(1));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(1), $this->uri->segment(2));
	}

	public function index($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->menu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['success_message'] = $this->session->flashdata('success_message');

		// ---------- Tools Sequent ---------- //
			$seq 		= $this->input->post('seq');
			$content_id = $this->input->post('content_id');
			if($content_id) {

				foreach ($content_id as $key => $value) {

					$this->Banner_model->setSequence($key, $value);
				}
			}

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->menu['menu_icon']);
		$this->admin_library->setDetail($this->menu['menu_title']);
		$this->admin_library->view("banner/listview",$this->_data);
		$this->admin_library->output($this->path);
	}

	public function load_datatable($lang_id="TH")
	{
		$this->form_validation->set_rules("length","Length","trim|required|integer");
		$this->form_validation->set_rules("start","Start","trim|required|integer");

		if($this->form_validation->run()===false) {

			echo json_encode([]);
		} else {
		
			$limit 			= $this->input->post('length');
			$start 			= $this->input->post('start');
			$is_order 		= array();
			$is_search 		= array();
			$result_array 	= array();

			// ####### Column all ####### //
				if(!empty($this->input->post('columns'))) {

					foreach ($this->input->post('columns') as $key => $column) {

						$is_order[$key] = $column['data'];

						if($column['search']['value']) {

							$is_search[$column['data']] 	= $column['search']['value'];
						}
					}
				}

			// ####### Orderable ####### //
				if(!empty($this->input->post('order')[0]['column'])) {

					$is_order = array($is_order[$this->input->post('order')[0]['column']] => $this->input->post('order')[0]['dir']);
				} else {

					$is_order = array();
				}

			// ####### Searchable ####### //
				if(empty($is_search)) {

					$all_data 		= $this->Banner_model->dataTable($lang_id);
		            $query_data 	= $this->Banner_model->dataTable($lang_id, $limit, $start, $is_order)
		            															->result_array();
		        } else {

					$all_data 		= $this->Banner_model->dataTable($lang_id, null, null, null, $is_search);
					$query_data 	= $this->Banner_model->dataTable($lang_id, $limit, $start, $is_order, $is_search)
																				->result_array();
				}

			if(!empty($query_data)) {

				foreach ($query_data as $key => $value) {
					$result_array[$key] = $value;
					$result_array[$key]['no'] = ($key+1)+$start;
					$result_array[$key]['post_date_format'] = date_format_convert_shortdatetime($value['post_date']);
				}
			}

			$output = array(
							"draw"            	=> intval($this->input->post('draw')),
							"recordsFiltered"   => intval($all_data),
							"recordsTotal" 		=> count($result_array),
							"data"            	=> $result_array
						);

			echo json_encode($output);
		}
	}

	public function add($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id']=$lang_id;

		$this->form_validation->set_rules("content_subject","Title Name","trim|required|max_length[255]");
		$this->form_validation->set_rules("content_url","Site Url","trim");
		$this->form_validation->set_rules("content_detail","Description","trim");

		// เช็คอัพโหลดไฟล์
		$this->form_validation->set_rules("image_thumb[]","Pictures PC","callback_fileupload_images");
		$this->form_validation->set_rules("image_thumb_mb[]","Pictures Mobile","callback_fileupload_images_mb");
		// $this->form_validation->set_rules("file_thumb[]","Files Attachment","callback_fileupload_files");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']			= "Add ".$this->menu['menu_label'];
			$this->_data['_menu_icon']			= "glyphicon-plus-sign";
			$this->_data['_menu_title']			= " เพิ่ม".$this->menu['menu_title'];
			$this->_data['_menu_link']  		= $this->menu['menu_link'];

			$this->_data['validation_errors'] 	= validation_errors();
			$this->_data['error_message'] 		= $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("banner/add",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$main_id 		= $this->Banner_model->addData();
			$content_id 	= $this->Banner_model->addLanguage($main_id,$lang_id);

			$this->Banner_model->setDefaultContent($main_id,$lang_id,$content_id);
			$this->Banner_model->setDate($main_id);

			$data = array(
						'main_id'				=> $main_id,
						'lang_id'				=> $lang_id,
						'content_subject' 		=> $this->input->post("content_subject"),
						'subject_status' 		=> $this->input->post("subject_status"),
						'content_url' 			=> $this->input->post("content_url"),
						'content_detail'		=> $this->input->post("content_detail"),
						'content_startdate'		=> $this->input->post("content_startdate"),
						'content_enddate'		=> $this->input->post("content_enddate"),
						'content_title'			=> ($this->input->post("content_title") ? 
														$this->input->post("content_title") : 
															$this->input->post("content_subject")),
						'content_keyword'		=> $this->input->post("content_keyword"),
						'content_description'	=> $this->input->post("content_description"),
						'content_status'		=> $this->input->post("content_status"),
						'content_seo'			=> $this->input->post("content_subject"),
						'content_rewrite_id'	=> md5($this->menu['menu_id'].$main_id.time())
					);

			$checkUpdate = $this->Banner_model->updateContent($data);

			// ############# Images Upload ############# //
				if($this->_data['img_thumbnail']) {

					$data['img_thumbnail'] = $this->_data['img_thumbnail'];
					if($data['img_thumbnail'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail'] as $thumbnail)
						{
							$this->Banner_model->insertContent(
								$main_id,
								$thumbnail,
								'pc'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail'] as $thumbnail)
						// {
						// 	$this->Banner_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				if($this->_data['img_thumbnail_mb']) {

					$data['img_thumbnail_mb'] = $this->_data['img_thumbnail_mb'];
					if($data['img_thumbnail_mb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail_mb'] as $thumbnail)
						{
							$this->Banner_model->insertContent(
								$main_id,
								$thumbnail,
								'mb'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail'] as $thumbnail)
						// {
						// 	$this->Banner_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

			// ############# Files Upload ############# //
				// if($this->_data['file_thumbnail']) {

				// 	$data['file_thumb'] = $this->_data['file_thumbnail'];
				// 	if($data['file_thumb'])
				// 	{
				// 		// อัพโหลดครั้งเดียวทุกภาษา
				// 		foreach($data['file_thumb'] as $thumbnail )
				// 		{
				// 			$this->Banner_model->insertContent(
				// 				$main_id,
				// 				$thumbnail
				// 			);
				// 		}
				// 		// อัพโหลดภาษาละครั้ง
				// 		// foreach($data['file_thumb'] as $thumbnail )
				// 		// {
				// 		// 	$this->Banner_model->insertContent(
				// 		// 		$main_id,
				// 		// 		$thumbnail,
				// 		// 		$lang_id
				// 		// 	);
				// 		// }
				// 	}
				// }

			if(!$checkUpdate) {

				$this->session->set_flashdata("success_message","Content can't create.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content can't create",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->menu_backend_log($logs);

			} else {

				$this->session->set_flashdata("success_message","Content has been create.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'add',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content has been create",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->menu_backend_log($logs);

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					// $seo = array(
					// 			'lang_id'				=> $data['lang_id'],
					// 			'menu_link' 			=> $this->menu['menu_link'],
					// 			'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
					// 			'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
					// 			'content_subject'		=> $data['content_seo'],
					// 			'content_rewrite_id'	=> $data['content_rewrite_id']
					// 		);

					// $this->seo_library->rewrite_update($seo);

				admin_redirect($this->menu['menu_link']."/edit/".$main_id."/".$lang_id);
			}
		}
	}

	public function edit($main_id,$lang_id=NULL)
	{
		(empty($lang_id))?$lang_id 	= "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 	= $lang_id;
		$this->_data['main_id'] 	= $main_id;

		$this->_data['row'] = $this->Banner_model->getDetail($main_id,$lang_id);

		if(!$this->_data['row']) {
			$this->Banner_model->addLanguage($main_id,$lang_id);
			$this->_data['row'] = $this->Banner_model->getDetail($main_id,$lang_id);
		}

		if(!$this->_data['row']) {
			show_error("ไม่พบข้อมูลในระบบ");
		}

		if($this->_data['row']['content_thumbnail']) {
			$this->_data['row']['content_thumbnail'] = $this->Banner_model->getThumbnail_img($this->_data['row']['content_thumbnail'])
																			->row_array();
		}

		$this->admin_library->add_breadcrumb(
												$this->admin_library->getLanguagename($this->_data['row']['lang_id']),
												"Banner/edit/".$this->_data['row']['main_id']."/".$this->_data['row']['lang_id'],
												"icon-globe"
											);

		$this->load->library('form_validation');
		$this->form_validation->set_rules("content_subject","Title Name","trim|required|max_length[255]");
		$this->form_validation->set_rules("content_url","Site Url","trim");
		$this->form_validation->set_rules("content_detail","Description","trim");

		// เช็คอัพโหลดไฟล์
		$this->form_validation->set_rules("image_thumb[]","Pictures PC","callback_fileupload_images");
		$this->form_validation->set_rules("image_thumb_mb[]","Pictures Mobile","callback_fileupload_images_mb");
		// $this->form_validation->set_rules("file_thumb[]","อัพโหลดไฟล์","callback_fileupload_files");

		if($this->form_validation->run()===false) {

			$this->_data['_menu_name']	= "Edit ".$this->menu['menu_label'];
			$this->_data['_menu_icon']	= "glyphicon-plus-sign";
			$this->_data['_menu_title']	= " แก้ไข".$this->menu['menu_title'];
			$this->_data['_menu_link']  = $this->menu['menu_link'];

			$this->_data['rs_img'] 	= $this->Banner_model->getDetail_img($main_id);
			$this->_data['rs_file'] = $this->Banner_model->getDetail_file($main_id);

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->_data['_menu_icon']);
			$this->admin_library->setDetail($this->_data['_menu_title']);
			$this->admin_library->view("banner/edit",$this->_data);
			$this->admin_library->output($this->path);

		} else {

			$this->Banner_model->setDate($main_id);

			$data = array(
						'main_id'				=> $main_id,
						'lang_id'				=> $lang_id,
						'content_subject' 		=> $this->input->post("content_subject"),
						'subject_status' 		=> $this->input->post("subject_status"),
						'content_url' 			=> $this->input->post("content_url"),
						'content_detail'		=> $this->input->post("content_detail"),
						'content_startdate'		=> $this->input->post("content_startdate"),
						'content_enddate'		=> $this->input->post("content_enddate"),
						'content_title'			=> $this->input->post("content_title"),
						'content_keyword'		=> $this->input->post("content_keyword"),
						'content_description'	=> $this->input->post("content_description"),
						'content_status'		=> $this->input->post("content_status"),
						'content_seo'			=> ($this->input->post("content_seo") ? 
														$this->input->post("content_seo") : 
															$this->input->post("content_subject")),
						'content_rewrite_id'	=> ($this->_data['row']['content_rewrite_id'] ? 
														$this->_data['row']['content_rewrite_id'] :
															md5($this->menu['menu_id'].$main_id.time()))
					);

			$checkUpdate = $this->Banner_model->updateContent($data);

			// ############# Images Upload ############# //
				if($this->_data['img_thumbnail']) {

					$data['img_thumbnail'] = $this->_data['img_thumbnail'];
					if($data['img_thumbnail'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail'] as $thumbnail )
						{
							$this->Banner_model->insertContent(
								$main_id,
								$thumbnail,
								'pc'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail'] as $thumbnail )
						// {
						// 	$this->Banner_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				if($this->_data['img_thumbnail_mb']) {

					$data['img_thumbnail_mb'] = $this->_data['img_thumbnail_mb'];
					if($data['img_thumbnail_mb'])
					{
						// อัพโหลดครั้งเดียวทุกภาษา
						foreach($data['img_thumbnail_mb'] as $thumbnail )
						{
							$this->Banner_model->insertContent(
								$main_id,
								$thumbnail,
								'mb'
							);
						}
						// อัพโหลดภาษาละครั้ง
						// foreach($data['img_thumbnail_mb'] as $thumbnail )
						// {
						// 	$this->Banner_model->insertContent(
						// 		$main_id,
						// 		$thumbnail,
						// 		$lang_id
						// 	);
						// }
					}
				}

				// Set Sequence Images
					$img_id 				= $this->input->post('attachment_id');
					$attachment_name 		= $this->input->post('attachment_name');
					$attachment_name_old 	= $this->input->post('attachment_name_old');
					$attachment_name_main 	= $this->input->post('attachment_name_main');
					$attachment_detail 		= $this->input->post('attachment_detail');

					if(is_array($img_id) && count($img_id)>0) {

						$i=0;
						foreach($img_id as $id) {

							$data_update = array(
								'attachment_id'		=> $id,
								'attachment_name' 	=> (!empty($attachment_name[$id]) ?
															($attachment_name_old[$id] ?
																$attachment_name[$id] :
															 		$attachment_name[$id]."_".$attachment_name_main[$id]) :
																		$attachment_name_main[$id]
														),
								'attachment_detail'	=> $attachment_detail[$id],
								// 'lang_id'			=> $lang_id
							);

							// Change Image Name
								if(!empty($attachment_name[$id]) || !empty($attachment_name_old[$id])) {

									$data_change_path_file = array(

										'attachment_name' 	=> (!empty($attachment_name[$id]) ?
																	($attachment_name_old[$id] ?
																		$attachment_name[$id] :
																	 		$attachment_name[$id]."_".$attachment_name_main[$id]) :
																				$attachment_name_main[$id]
																),
										'attachment_name_old'	=> $attachment_name_old[$id],
										'attachment_type'		=> $attachment_type[$id],
										'attachment_name_main'	=> $attachment_name_main[$id]
									);

									if( $this->change_path_file($data_change_path_file, 'images') ) {

										$this->Banner_model->updateAttachmentContent($data_update);
									}
								} else {

									$this->Banner_model->updateAttachmentContent($data_update);
								}

							$i++;
							$this->Banner_model->setSequenceAttachment($id,$i);
						}
					}

			// ############# Files Upload ############# //
				// if($this->_data['file_thumbnail']) {

				// 	$data['file_thumb'] = $this->_data['file_thumbnail'];
				// 	if($data['file_thumb'])
				// 	{
				// 		// อัพโหลดครั้งเดียวทุกภาษา
				// 		foreach($data['file_thumb'] as $thumbnail )
				// 		{
				// 			$this->Banner_model->insertContent(
				// 				$main_id,
				// 				$thumbnail
				// 			);
				// 		}
				// 		// อัพโหลดภาษาละครั้ง
				// 		// foreach($data['file_thumb'] as $thumbnail )
				// 		// {
				// 		// 	$this->Banner_model->insertContent(
				// 		// 		$main_id,
				// 		// 		$thumbnail,
				// 		// 		$lang_id
				// 		// 	);
				// 		// }
				// 	}
				// }

			if(!$checkUpdate) {

				$this->session->set_flashdata("success_message","Content can't delete.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content can't delete",
								'data'			=> serialize($data),
								'status' 		=> 'failure'
							);

					$this->logs_library->menu_backend_log($logs);

			} else {

				$this->session->set_flashdata("success_message","Content has been delete.");

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
					$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
					$logs = array(
								'menu_link'		=> $this->menu['menu_link'],
								'menu_id' 		=> $this->menu['menu_id'],
								'submenu_id' 	=> __Function__,
								'action'		=> 'update',
								'username' 		=> $user['username'],
								'session' 		=> $this->session->session_id,
								'messages' 		=> "Content has been update",
								'data'			=> serialize($data),
								'status' 		=> 'success'
							);

					$this->logs_library->menu_backend_log($logs);

				// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

					// $seo = array(
					// 			'lang_id'				=> $data['lang_id'],
					// 			'menu_link' 			=> $this->menu['menu_link'],
					// 			'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
					// 			'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
					// 			'content_subject'		=> $data['content_seo'],
					// 			'content_rewrite_id'	=> $data['content_rewrite_id']
					// 		);

					// $this->seo_library->rewrite_update($seo);

				admin_redirect($this->menu['menu_link']."/edit/".$main_id.'/'.$lang_id);
			}
		}
	}

	public function delete($main_id,$lang_id=null)
	{
		$data 			= $this->Banner_model->getDetail($main_id);
		$checkUpdate 	= $this->Banner_model->deleteContent($data);

		// Re-set Sequence after Delete content
			$queryAll 		= $this->Banner_model->getAllContent();
			$queryResult 	= $this->Banner_model->getAllContent()->result_array();

			for($i=0; $i<$queryAll->num_rows(); $i++) {

				$this->Banner_model->setSequence($queryResult[$i]['main_id'], ($i+1));
			}

		if(!$checkUpdate) {

			$this->session->set_flashdata("success_message","Cant' delete centent.");

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Cant' delete centent",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->menu_backend_log($logs);

		} else {

			$this->session->set_flashdata("success_message","Delete centent success.");

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Delete centent success",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->menu_backend_log($logs);

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

				$seo = array(
							'lang_id'				=> $data['lang_id'],
							'menu_link' 			=> $this->menu['menu_link'],
							'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
							'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
							'content_subject'		=> $data['content_seo'],
							'content_rewrite_id'	=> $data['content_rewrite_id']
						);

				$this->seo_library->rewrite_delete($seo);

			admin_redirect($this->menu['menu_link']."/index/".$lang_id);
		}
	}

	public function handle_delete($lang_id=null)
	{
		$data = array(
					'main_id' 	=> $this->input->post("main_id")
				);

		foreach($data['main_id'] as $id) {

			$data 			= $this->Banner_model->getDetail($id);
			$this->Banner_model->deleteContent($data);

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'delete',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Delete content success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII Menu SEO IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //

			$seo = array(
						'lang_id'				=> $data['lang_id'],
						'menu_link' 			=> $this->menu['menu_link'],
						'target_path'			=> $this->menu['menu_link'].'/'.$this->menu['menu_link']."_detail",
						'target_detail_path'	=> $data['main_id'].'/'.$data['lang_id'],
						'content_subject'		=> $data['content_seo'],
						'content_rewrite_id'	=> $data['content_rewrite_id']
					);

			$this->seo_library->rewrite_delete($seo);
		}

		// Re-set Sequence after Delete content
			$queryAll = $this->Banner_model->getAllContent();
			$queryResult = $this->Banner_model->getAllContent()->result_array();

			for($i=0; $i<$queryAll->num_rows(); $i++) {

				$this->Banner_model->setSequence($queryResult[$i]['main_id'], ($i+1));
			}

			$this->session->set_flashdata("success_message","Delete centent success.");

		admin_redirect($this->menu['menu_link']."/index/".$lang_id);
	}

	public function handle_suspend($lang_id=null)
	{
		$data = array(
					'main_id' 	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);

		foreach($data['main_id'] as $id) {
			$this->Banner_model->setStatus($id,$data['lang_id'],"pending");
		}

		$this->session->set_flashdata("success_message","Update status success.");

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'update',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Update status success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);

		admin_redirect($this->menu['menu_link']."/index/".$lang_id);
	}

	public function handle_unsuspend($lang_id=null)
	{
		$data = array(
					'main_id'	=> $this->input->post("main_id"),
					'lang_id'	=> $this->input->post("lang_id")
				);

		foreach($data['main_id'] as $id) {
			$this->Banner_model->setStatus($id,$data['lang_id'],"active");
		}

		$this->session->set_flashdata("success_message","Update status success.");

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
			$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
			$logs = array(
						'menu_link'		=> $this->menu['menu_link'],
						'menu_id' 		=> $this->menu['menu_id'],
						'submenu_id' 	=> __Function__,
						'action'		=> 'update',
						'username' 		=> $user['username'],
						'session' 		=> $this->session->session_id,
						'messages' 		=> "Update status success",
						'data'			=> serialize($data),
						'status' 		=> 'success'
					);

			$this->logs_library->menu_backend_log($logs);

		admin_redirect($this->menu['menu_link']."/index/".$lang_id);
	}

	public function delete_img($attachment_id,$default_main_id,$attachment_name,$lang_id,$type_name)
	{
		$data = array(
					'attachment_id'		=> $attachment_id,
					'default_main_id'	=> $default_main_id,
					'attachment_name'	=> $attachment_name,
					'lang_id'			=> $lang_id,
					'type_name'			=> $type_name
				);

		if($type_name == "images") {
			$path = './public/uploads/banner/images/'.$attachment_name;
		} else {
			$path = './public/uploads/banner/files/'.$attachment_name;
		}

		$checkImage = $this->Banner_model->deleteImage($attachment_id);

		if(!$checkImage) {

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Can't delete image",
							'data'			=> serialize($data),
							'status' 		=> 'failure'
						);

				$this->logs_library->menu_backend_log($logs);

		} else {

			if(unlink($path)) {
				$this->session->set_flashdata("success_message","Delete file in Server success.");
			} else {
				$this->session->set_flashdata("error_message","Error! Can't delete file in Server.");
			}

			// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII User Logs IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII //
				$user = $this->admin_library->getuserinfo($this->admin_library->userdata('user_id'));
				$logs = array(
							'menu_link'		=> $this->menu['menu_link'],
							'menu_id' 		=> $this->menu['menu_id'],
							'submenu_id' 	=> __Function__,
							'action'		=> 'delete',
							'username' 		=> $user['username'],
							'session' 		=> $this->session->session_id,
							'messages' 		=> "Delete image success",
							'data'			=> serialize($data),
							'status' 		=> 'success'
						);

				$this->logs_library->menu_backend_log($logs);

			admin_redirect($this->menu['menu_link']."/edit/".$default_main_id."/".$lang_id);
		}
	}

	public function set_default_img($default_main_id,$content_id,$attachment_id,$lang_id)
	{
		$this->Banner_model->setDefaultImg($content_id,$attachment_id,$lang_id);
		admin_redirect($this->menu['menu_link']."/edit/".$default_main_id."/".$lang_id);
	}

	// ################### Check uploads Images. ######################

	public function fileupload_images()
	{
		$this->_data['img_thumbnail'] = array();
		$number_of_files 	= sizeof($_FILES['image_thumb']['tmp_name']);
		$remove_of_files 	= ( @$this->input->post("images") ? explode(',', $this->input->post("images")) : [] );
		$files 				= $_FILES['image_thumb'];
		$files_limit 		= 1;
		$main_id 			= @$this->_data['main_id'];
		$lang_id 			= $this->_data['lang_id'];

		// ถ้าต้องการเช็คอัพโหลดรูป
		// if($files['tmp_name']) {
		//     for($i=0;$i<$number_of_files;$i++)
		//     {
		//       if($_FILES['image_thumb']['error'][$i] != 0)
		//       {
		//         $this->form_validation->set_message('fileupload_images', 'The Upload Pictures field is required.');
		//         return FALSE;
		//       }
		//     }
		// }

		// ถ้าไม่ต้องการเช็คอัพโหลดรูป
		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		} else {

			return TRUE;
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/banner/images';
		$config['encrypt_name']		= true;
		$config['allowed_types'] 	= 'gif|jpg|jpeg|png';
		$config['max_size']			= '5024';
		$config['max_width']  		= '2000';
		$config['max_height']  		= '1600';

		if($main_id) {
			$count_images = $this->Banner_model->getDetail_img($main_id,'pc')->num_rows();
		} else {
			$count_images = 0;
		}

		for ($i = 0; $i < $number_of_files; $i++) {

			if(!in_array($i, $remove_of_files)) {

				$_FILES['image_thumb']['name'] 		= $files['name'][$i];
				$_FILES['image_thumb']['type'] 		= $files['type'][$i];
				$_FILES['image_thumb']['tmp_name'] 	= $files['tmp_name'][$i];
				$_FILES['image_thumb']['error'] 	= $files['error'][$i];
				$_FILES['image_thumb']['size'] 		= $files['size'][$i];

				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_thumb')) {

					$data  								= $this->upload->data();
					$this->_data['img_thumbnail'][] 	= $data['file_name'];

					// เช็คจำนวนอัพโหลดมากสุด
					if((count($this->_data['img_thumbnail'])+$count_images) > $files_limit) {

						$this->form_validation->set_message('fileupload_images', 'The Upload Pictures PC limit '.$files_limit);
						return FALSE;
					}

				} else {

					$this->form_validation->set_message('fileupload_images', $this->upload->display_errors());
					return FALSE;
				}
			}
		}

		return TRUE;
	}

	public function fileupload_images_mb()
	{
		$this->_data['img_thumbnail_mb'] = array();
		$number_of_files 	= sizeof($_FILES['image_thumb_mb']['tmp_name']);
		$remove_of_files 	= ( @$this->input->post("images_mb") ? explode(',', $this->input->post("images_mb")) : [] );
		$files 				= $_FILES['image_thumb_mb'];
		$files_limit 		= 1;
		$main_id 			= @$this->_data['main_id'];
		$lang_id 			= $this->_data['lang_id'];

		// ถ้าต้องการเช็คอัพโหลดรูป
		// if($files['tmp_name']) {
		//     for($i=0;$i<$number_of_files;$i++)
		//     {
		//       if($_FILES['image_thumb_mb']['error'][$i] != 0)
		//       {
		//         $this->form_validation->set_message('fileupload_images_mb', 'The Upload Pictures field is required.');
		//         return FALSE;
		//       }
		//     }
		// }

		// ถ้าไม่ต้องการเช็คอัพโหลดรูป
		if($files['tmp_name']) {

			if($files['tmp_name'][0]==NULL) {

				return TRUE;
			}
		} else {

			return TRUE;
		}

		$config['upload_path'] 		= FCPATH.'./public/uploads/banner/images';
		$config['encrypt_name']		= true;
		$config['allowed_types'] 	= 'gif|jpg|jpeg|png';
		$config['max_size']			= '5024';
		$config['max_width']  		= '640';
		$config['max_height']  		= '926';

		if($main_id) {
			$count_images = $this->Banner_model->getDetail_img($main_id,'mb')->num_rows();
		} else {
			$count_images = 0;
		}

		for ($i = 0; $i < $number_of_files; $i++) {

			if(!in_array($i, $remove_of_files)) {

				$_FILES['image_thumb_mb']['name'] 		= $files['name'][$i];
				$_FILES['image_thumb_mb']['type'] 		= $files['type'][$i];
				$_FILES['image_thumb_mb']['tmp_name'] 	= $files['tmp_name'][$i];
				$_FILES['image_thumb_mb']['error'] 	= $files['error'][$i];
				$_FILES['image_thumb_mb']['size'] 		= $files['size'][$i];

				$this->upload->initialize($config);
				if ($this->upload->do_upload('image_thumb_mb')) {

					$data  								= $this->upload->data();
					$this->_data['img_thumbnail_mb'][] 	= $data['file_name'];

					// เช็คจำนวนอัพโหลดมากสุด
					if((count($this->_data['img_thumbnail_mb'])+$count_images) > $files_limit) {

						$this->form_validation->set_message('fileupload_images_mb', 'The Upload Pictures Mobile limit '.$files_limit);
						return FALSE;
					}

				} else {

					$this->form_validation->set_message('fileupload_images_mb', $this->upload->display_errors());
					return FALSE;
				}
			}
		}

		return TRUE;
	}

	// ################### Check uploads Images. ######################

	// public function fileupload_files()
	// {
	// 	$this->_data['file_thumbnail'] 	= array();
	// 	$number_of_files 				= sizeof($_FILES['file_thumb']['tmp_name']);
	// 	$files 							= $_FILES['file_thumb'];

	//     // ถ้าต้องการเช็คอัพโหลดรูป
	// 	// if($files['tmp_name']) {
	// 	//     for($i=0;$i<$number_of_files;$i++)
	// 	//     {
	// 	//       if($_FILES['file_thumb']['error'][$i] != 0)
	// 	//       {
	// 	//         $this->form_validation->set_message('fileupload_files', 'The Upload Files Attachment field is required.');
	// 	//         return FALSE;
	// 	//       }
	// 	//     }
	// 	// }

	// 	// ถ้าไม่ต้องการเช็คอัพโหลดไฟล์
	// 	if($files['tmp_name']) {

	// 		if($files['tmp_name'][0]==NULL) {

	// 			return TRUE;
	// 		}
	// 	} else {

	// 		return TRUE;
	// 	}

	// 	$config['upload_path'] 		= FCPATH.'./public/uploads/banner/files';
	// 	$config['allowed_types'] 	= 'pdf|doc|docx|rar|zip|';
	// 	$config['max_size']			= 204800;
	// 	$config['encrypt_name'] 	= TRUE;

	// 	for ($i = 0; $i < $number_of_files; $i++) {

	// 		$_FILES['file_thumb']['name'] 		= $files['name'][$i];
	// 		$_FILES['file_thumb']['type'] 		= $files['type'][$i];
	// 		$_FILES['file_thumb']['tmp_name'] 	= $files['tmp_name'][$i];
	// 		$_FILES['file_thumb']['error'] 		= $files['error'][$i];
	// 		$_FILES['file_thumb']['size'] 		= $files['size'][$i];

	// 		$this->upload->initialize($config);
	// 		if ($this->upload->do_upload('file_thumb')) {

	// 			$data  								= $this->upload->data();
	// 			$this->_data['file_thumbnail'][] 	= $data['file_name'];

	// 	      	// เช็คจำนวนอัพโหลดมากสุด
	// 			if(count($this->_data['file_thumbnail']) > 1) {

	// 				$this->form_validation->set_message('fileupload_files', 'The Upload Files Attachment limit 1');
	// 				return FALSE;
	// 			}
	// 		} else {

	// 			$this->form_validation->set_message('fileupload_files', $this->upload->display_errors());
	// 			return FALSE;
	// 		}
	// 	}

	//     return TRUE;
	// }

	// ################### Change path & name File. ######################

	private function change_path_file(array $data = array(), $type="images")
	{
		$attachment_new 	= ($data['attachment_name'] ? 
										$data['attachment_name'] : 
											$data['attachment_name_old']);
		$attachment_old 	= ($data['attachment_name_old'] ? 
										$data['attachment_name_old'] : 
											$data['attachment_name_main']);

		$upload_path_new 	= './public/uploads/banner/'.$type.'/'.$attachment_new;
		$upload_path_main 	= './public/uploads/banner/'.$type.'/'.$attachment_old;

		if(file_exists($upload_path_main)) {

			return rename($upload_path_main, $upload_path_new);
		} else {

			return false;
		}
	}
} 
?>