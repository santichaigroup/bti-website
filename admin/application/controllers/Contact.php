<?php
class Contact extends CI_Controller{
	
	var $_data = array();
	var $_menu_name = "";
	var $menu;
	var $submenu;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('upload');
		$this->load->library('admin_library');	
		$this->admin_library->forceLogin();
		$this->load->model('address_model');
		$this->load->model('subscribe_model');
		$this->load->model('setting_email_model');

		$this->path 	= 	$this->uri->ruri_string();
		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(1));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(1), $this->uri->segment(2));
	}
	public function index()
	{
		admin_redirect();
	}

	public function address($lang_id="TH")
	{
		
		$rs = $this->address_model->checkdataTable();
		
		if($rs->num_rows() > 0){
			$row = $rs->row_array();
			admin_redirect("contact/edit/".$row['address_id']."/".$lang_id);
		}else{
			admin_redirect("contact/add/".$lang_id);
				
		}
	}
	
	/************************************************** Address Bangkok ************************************************/

	public function add($lang_id="TH")
	{
		(empty($lang_id))?$lang_id 		= "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->admin_library->add_breadcrumb($this->admin_library->getLanguagename($this->_data['lang_id']),"contact/add/".$this->_data['lang_id'],"icon-globe");
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("content_subject","ห้วข้อ","trim|required|max_length[255]");

		if($this->form_validation->run()===false){
			
			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');

			$this->admin_library->setTitle($this->_menu_name,'icon-book');
			$this->admin_library->setDetail("จัดการข้อมูล".$this->_menu_name."");
			$this->admin_library->view("address/add",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			
			$address_id = $this->address_model->addData();
			$content_id = $this->address_model->addLanguage($address_id,$lang_id);
			$this->address_model->setDefaultContent($address_id,$content_id); 

			$content_thumbnail 	= $this->uploadImage('add',$address_id,$lang_id);
			$content_qr_code 	= $this->uploadQRCode('add',$address_id,$lang_id);
			
			$this->address_model->setDate(
				$address_id,
				$this->input->post("address_date")
			);

			$this->address_model->updateContent(
				$address_id,
				$lang_id,
				$this->input->post("content_subject"),
				$this->input->post("content_group"),
				$this->input->post("content_address"),
				$this->input->post("content_phone"),
				$this->input->post("content_fax"),
				$this->input->post("content_email"),
				$this->input->post("content_googlemap"),
				$content_thumbnail,
				$this->input->post("content_detail"),
				$this->input->post("content_facebook"),
				$this->input->post("content_instagram"),
				$this->input->post("content_google"),
				$this->input->post("content_youtube"),
				$this->input->post("content_twitter"),
				$this->input->post("content_tumblr"),
				$this->input->post("content_pinterest"),
				$this->input->post("content_line"),
				$content_qr_code
			);
			$this->address_model->updateContentStatus(
				$address_id,
				$lang_id,
				$this->input->post("content_status")
			);
			$this->session->set_flashdata("success_message","Content has been create.");

			admin_redirect("contact/address/".$lang_id);
		}
	}

	public function edit($address_id,$lang_id="TH")
	{
		(empty($lang_id))?$lang_id 		= "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];
		
		$lang_id = strtoupper($lang_id);
		$this->address_model->checkExistst($address_id);
		$this->_data['row'] = $this->address_model->getDetail($address_id,$lang_id);

		if(!$this->_data['row']){
			$this->address_model->addLanguage($address_id,$lang_id);
			$this->_data['row'] = $this->address_model->getDetail($address_id,$lang_id);
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");	
		}
		$this->admin_library->add_breadcrumb($this->admin_library->getLanguagename($this->_data['row']['lang_id']),"contact/edit/".$this->_data['row']['address_id']."/".$this->_data['row']['lang_id'],"icon-globe");
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_subject","ห้วข้อ","trim|required|max_length[255]");
	
		if($this->form_validation->run()===false){

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata("error_message");

			$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
			$this->admin_library->setDetail($this->submenu['menu_title']);
			$this->admin_library->view("address/edit",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			
			$content_thumbnail 	= $this->uploadImage('edit',$address_id,$lang_id);
			$content_qr_code 	= $this->uploadQRCode('edit',$address_id,$lang_id);
			
			$this->address_model->setDate(
				$this->input->post("address_id"),
				$this->input->post("address_date")
			);
	
			$this->address_model->updateContent(
				$this->input->post("address_id"),
				$this->input->post("lang_id"),
				$this->input->post("content_subject"),
				$this->input->post("content_group"),
				$this->input->post("content_address"),
				$this->input->post("content_phone"),
				$this->input->post("content_fax"),
				$this->input->post("content_email"),
				$this->input->post("content_googlemap"),
				$content_thumbnail,
				$this->input->post("content_detail"),
				$this->input->post("content_facebook"),
				$this->input->post("content_instagram"),
				$this->input->post("content_google"),
				$this->input->post("content_youtube"),
				$this->input->post("content_twitter"),
				$this->input->post("content_tumblr"),
				$this->input->post("content_pinterest"),
				$this->input->post("content_line"),
				$content_qr_code
			);
			$this->address_model->updateContentStatus(
				$this->input->post("address_id"),
				$this->input->post("lang_id"),
				$this->input->post("content_status")
			);
			$this->session->set_flashdata("success_message","Content has been create.");
			
			admin_redirect("contact/edit/".$address_id."/".$lang_id);
		}
	}

	public function uploadImage($page=NULL,$address_id=NULL,$lang_id=NULL)
	{
		$image_thumb = $_FILES['image_thumb'];
		
		if($image_thumb['tmp_name'] == ""){
			return NULL;
		}
		$config['upload_path'] = './public/uploads/address/';
		$config['encrypt_name']	= true;
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '5024';

		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('image_thumb')){
			$show_error = $this->upload->display_errors();
			$this->session->set_flashdata("error_message",$show_error);
			
			($page=='add' and $page!=NULL)?admin_redirect("contact/add/".$lang_id): admin_redirect("contact/edit/".$address_id."/".$lang_id);
		
		}else{
			$data = $this->upload->data();
			return $data['file_name'];
		}
	}

	public function uploadQRCode($page=NULL,$address_id=NULL,$lang_id=NULL)
	{
		$image_thumb = $_FILES['content_qr_code'];
		
		if($image_thumb['tmp_name'] == ""){
			return NULL;
		}
		$config['upload_path'] = './public/uploads/address/';
		$config['encrypt_name']	= true;
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '5024';

		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('content_qr_code')){
			$show_error = $this->upload->display_errors();
			$this->session->set_flashdata("error_message",$show_error);
			
			($page=='add' and $page!=NULL)?admin_redirect("contact/add/".$lang_id): admin_redirect("contact/edit/".$address_id."/".$lang_id);
		
		}else{
			$data = $this->upload->data();
			return $data['file_name'];
		}
	}
	
	/**************************************************** Contact *****************************************/

	public function contactus($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['dataTable'] = $this->subscribe_model->dataTable();
		$this->_data['success_message'] = $this->session->flashdata('success_message');

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
		$this->admin_library->setDetail($this->submenu['menu_title']);
		$this->admin_library->view("address/contactus/listview",$this->_data); 
		$this->admin_library->output($this->path);
	}

	public function load_datatable()
	{
		$result_data 	= $this->subscribe_model->dataTable();

		$output = array(
			"iTotalRecords" => $result_data->num_rows(),
			"iTotalDisplayRecords" => "25",
 	           "aData" => $result_data->result()
 	       );

		echo json_encode($output);
	}

	public function view($faq_id)
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['row'] = $this->subscribe_model->getDetail($faq_id);

		// $category_title = $this->_data['row']['contactus_title'];

		// $category_title = explode(',', $category_title);

		// $this->_data['category_name'] = $this->category_name($category_title[0]);

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");	
		}
		if($this->_data['row']['status'] == "new"){
			$data = array('status' => 'read');
			$this->subscribe_model->updateStatus($data,$faq_id); 
		}

			$this->_data['success_message'] = $this->session->flashdata('success_message');

			$this->admin_library->setTitle("Email Contact",'glyphicon-envelope');
			$this->admin_library->setDetail("อีเมลติดต่อ");
			$this->admin_library->view("address/contactus/view",$this->_data); 
			$this->admin_library->output($this->path);
	}

	function delete_subscribe($faq_id)
	{
		$this->subscribe_model->delete($faq_id);
		admin_redirect("contact/contactus/");
	}

	public function handle_delete_subscribe()
	{
		$faq_id = $this->input->post("main_id");	
		foreach($faq_id as $id){
			$this->subscribe_model->delete($id);
		}
		admin_redirect("contact/contactus");
	}

	/**************************************************** Subscribe *****************************************/

	public function subscribe($lang_id="TH")
	{
		(empty($lang_id))?$lang_id = "EN" : $lang_id = $lang_id;
		$this->_data['lang_id'] 		= $lang_id;
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['dataTable'] = $this->subscribe_model->dataTableFaq();
		$this->_data['success_message'] = $this->session->flashdata('success_message');

		$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
		$this->admin_library->setDetail($this->submenu['menu_title']);
		$this->admin_library->view("address/subscribe/listview",$this->_data); 
		$this->admin_library->output($this->path);
	}

	public function load_datatable_subscribe()
	{
		$result_data 	= $this->subscribe_model->dataTableFaq();

		$output = array(
			"iTotalRecords" => $result_data->num_rows(),
			"iTotalDisplayRecords" => "25",
 	           "aData" => $result_data->result()
 	       );

		echo json_encode($output);
	}

	function delete_subscribe_faq($faq_id)
	{
		$this->subscribe_model->deleteFaq($faq_id);
		admin_redirect("contact/subscribe/");
	}

	public function handle_delete_subscribe_faq()
	{
		$faq_id = $this->input->post("main_id");
		foreach($faq_id as $id){
			$this->subscribe_model->deleteFaq($id);
		}
		admin_redirect("contact/subscribe");
	}
	
	/************************************************** Setting Email Admin ************************************************/

	public function setting_email($lang_id="TH")
	{
		$rs = $this->setting_email_model->dataTable($lang_id);
		
		if($rs->num_rows() > 0){
			$row = $rs->row_array();
			admin_redirect("contact/edit_mail/".$row['email_id']."/".$lang_id);
		}else{
			admin_redirect("contact/add_email/".$lang_id);
		}
	}
	public function add_email($lang_id="TH"){
		$this->_data['lang_id']=$lang_id;
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("cate_id","ประเภทสินค้า","trim|required");
		$this->form_validation->set_rules("country_id","ประเภทสินค้า","trim|required");
		$this->form_validation->set_rules("content_subject","ชื่อผู้ส่งอีเมล","trim|required|max_length[255]");
		$this->form_validation->set_rules("content_email","อีเมล","trim|required|max_length[255]|valid_emails");
		$this->form_validation->set_rules("content_email_cc","สำเนาอีเมล","trim|valid_emails");

		if($this->form_validation->run()===false){
			
			$this->_data['categories'] 	= $this->setting_email_model->dataTableCategory($lang_id, true)->result_array();
			
			foreach ($this->_data['categories'] as $key => $value) {

				$this->_data['categories'][$key]['countries'] = $this->setting_email_model
																		->dataTableCategoryCountry($value['email_id'], $lang_id)
																		->result_array();
			}

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');
			$this->admin_library->setTitle($this->_menu_name,'icon-book');
			$this->admin_library->setDetail("เพิ่มอีเมล");
			$this->admin_library->view("address/setting_email/add",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			
			$email_id = $this->setting_email_model->addData();
			$content_id = $this->setting_email_model->addLanguage($email_id,$lang_id);
			$this->setting_email_model->setDefaultContent($email_id,$content_id); 
			
			$this->setting_email_model->setDate(
				$email_id,
				$this->input->post("email_date")
			);
			$this->setting_email_model->updateContent(
				$email_id,
				$lang_id,
				$this->input->post("cate_id"),
				$this->input->post("country_id"),
				$this->input->post("content_subject"),
				$this->input->post("content_email"),
				$this->input->post("content_email_cc")
			);
			$this->setting_email_model->updateContentStatus(
				$email_id,
				$lang_id,
				$this->input->post("content_status")
			);
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("contact/setting_email/".$lang_id);
		}
	}
	public function edit_mail($email_id,$lang_id=NULL)
	{
		$this->_data['lang_id']		= $lang_id;
		
		$lang_id = strtoupper($lang_id);
		$this->setting_email_model->checkExistst($email_id);
		$this->_data['row'] 	= $this->setting_email_model->getDetail($email_id,$lang_id);
		$this->_data['row_all'] = $this->setting_email_model->dataTable($lang_id)->result_array();

		foreach ($this->_data['row_all'] as $key => $value) {

			$this->_data['row_all'][$key]['category'] = $this->setting_email_model
																->getDetailCategory($value['cate_id'], $lang_id);
			$this->_data['row_all'][$key]['country'] = $this->setting_email_model
																->dataTableCategoryCountry($value['cate_id'], $lang_id, $value['country_id'])
																->row_array();
		}

		if(!$this->_data['row']){
			$this->setting_email_model->addLanguage($email_id,$lang_id);
			$this->_data['row'] = $this->setting_email_model->getDetail($email_id,$lang_id);
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");	
		}
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("cate_id","ประเภทสินค้า","trim|required");
		$this->form_validation->set_rules("country_id","ประเภทสินค้า","trim|required");
		$this->form_validation->set_rules("content_subject","ชื่อผู้ส่งอีเมล","trim|required|max_length[255]");
		$this->form_validation->set_rules("content_email","อีเมล","trim|required|max_length[255]|valid_emails");
		$this->form_validation->set_rules("content_email_cc","สำเนาอีเมล","trim|valid_emails");
	
		if($this->form_validation->run()===false){

			$this->_data['categories'] 				= $this->setting_email_model->getDetailCategory($email_id, $lang_id);
			$this->_data['categories']['countries'] = $this->setting_email_model
															->dataTableCategoryCountry($email_id, $lang_id, $this->_data['row']['country_id'])
															->row_array();

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle($this->_menu_name,'icon-book');
			$this->admin_library->setDetail("แก้ไขอีเมล");
			$this->admin_library->view("address/setting_email/edit",$this->_data);
			$this->admin_library->output($this->path);
		}else{
			
			$this->setting_email_model->setDate(
				$this->input->post("email_id"),
				$this->input->post("email_date")
			);
			$this->setting_email_model->updateContent(
				$email_id,
				$lang_id,
				$this->input->post("cate_id"),
				$this->input->post("country_id"),
				$this->input->post("content_subject"),
				$this->input->post("content_email"),
				$this->input->post("content_email_cc")
			);

			// if($this->input->post("content_status")!="deleted") {

				$this->setting_email_model->updateContentStatus(
					$email_id,
					$lang_id,
					$this->input->post("content_status")
				);
			// } else {


			// }
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("contact/setting_email/".$lang_id);
		}
	}
	public function setting_email_category($lang_id="TH")
	{
		$rs = $this->setting_email_model->dataTableCategory($lang_id);
		
		if($rs->num_rows() > 0){
			$row = $rs->row_array();
			admin_redirect("contact/edit_mail_category/".$row['email_id']."/".$lang_id);
		}else{
			admin_redirect("contact/add_email_category/".$lang_id);
		}
	}
	public function add_email_category($lang_id="TH"){
		$this->_data['lang_id']=$lang_id;
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("content_subject","ประเภทอีเมล","trim|required|max_length[255]");

		if($this->form_validation->run()===false){
			
			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata('error_message');

			$this->_data['countries'] = $this->db->where('lang_id', $lang_id)
													->group_by('content_country')
													->order_by('content_country', 'asc')
													->get('setting_email_category_country')
													->result_array();

			$this->admin_library->setTitle($this->_menu_name,'icon-book');
			$this->admin_library->setDetail("เพิ่มประเภทสินค้า");
			$this->admin_library->view("address/setting_email/cate_add",$this->_data);
			$this->admin_library->output($this->path);
		}else{
			
			$email_id = $this->setting_email_model->addDataCategory();
			$content_id = $this->setting_email_model->addLanguageCategory($email_id,$lang_id);
			$this->setting_email_model->setDefaultContentCategory($email_id,$content_id);

			$this->setting_email_model->setDateCategory(
				$email_id,
				$this->input->post("email_date")
			);

			$this->setting_email_model->updateContentCategory(
				$email_id,
				$lang_id,
				$this->input->post("content_subject")
			);
			$this->setting_email_model->updateContentStatusCategory(
				$email_id,
				$lang_id,
				$this->input->post("content_status")
			);

			// Add Country
			if(count($this->input->post("content_detail"))>0) {

				foreach ($this->input->post("content_detail") as $content_detail) {
					
					$this->db->set('main_id', $email_id);
					$this->db->set('lang_id', $lang_id);
					$this->db->set('content_country', $content_detail);
					$this->db->set("update_date","NOW()",false);
					$this->db->set("update_by",$this->admin_library->userdata('user_id'));
					$this->db->set("update_ip",$this->input->ip_address());
					$this->db->insert("setting_email_category_country");
				}
			}

			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("contact/setting_email_category/".$lang_id);
		}
	}
	public function edit_mail_category($email_id,$lang_id=NULL)
	{
		$this->_data['lang_id']		= $lang_id;
		
		$lang_id = strtoupper($lang_id);
		$this->setting_email_model->checkExistst($email_id);
		$this->_data['row'] 	= $this->setting_email_model->getDetailCategory($email_id,$lang_id);
		$this->_data['row']['countries'] = $this->setting_email_model->dataTableCategoryCountry($email_id,$lang_id)
																		->result_array();

		$this->_data['row_all'] = $this->setting_email_model->dataTableCategory($lang_id);

		if(!$this->_data['row']){
			$this->setting_email_model->addLanguageCategory($email_id,$lang_id);
			$this->_data['row'] = $this->setting_email_model->getDetailCategory($email_id,$lang_id);
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");	
		}

		$this->_data['countries'] = $this->db->where('lang_id', $lang_id)
												->group_by('content_country')
												->order_by('content_country', 'asc')
												->get('setting_email_category_country')
												->result_array();
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_subject","ประเภทอีเมล","trim|required|max_length[255]");
	
		if($this->form_validation->run()===false){

			$this->_data['validation_errors'] = validation_errors();
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle($this->_menu_name,'icon-book');
			$this->admin_library->setDetail("แก้ไขประเภทสินค้า ".$this->_data['row']['content_subject']);
			$this->admin_library->view("address/setting_email/cate_edit",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			
			$this->setting_email_model->setDateCategory(
				$email_id,
				$this->input->post("email_date")
			);
			$this->setting_email_model->updateContentCategory(
				$email_id,
				$this->input->post("lang_id"),
				$this->input->post("content_subject")
			);
			$this->setting_email_model->updateContentStatusCategory(
				$email_id,
				$this->input->post("lang_id"),
				$this->input->post("content_status")
			);

			// Delete All
			$this->db->where('main_id', $email_id)
						->where('lang_id', $lang_id)
						->delete('setting_email_category_country');

			// Add Country
			if(count($this->input->post("content_detail"))>0) {

				foreach ($this->input->post("content_detail") as $content_detail) {
					
					$this->db->set('main_id', $email_id);
					$this->db->set('lang_id', $lang_id);
					$this->db->set('content_country', $content_detail);
					$this->db->set("update_date","NOW()",false);
					$this->db->set("update_by",$this->admin_library->userdata('user_id'));
					$this->db->set("update_ip",$this->input->ip_address());
					$this->db->insert("setting_email_category_country");
				}
			}

			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("contact/setting_email_category/".$lang_id);
		}
	}
	public function category_name($category_id)
	{
		$category_name = $this->subscribe_model->category_name($category_id);

		return $category_name['content_subject'];
	}
	public function sub_category_name($category_id,$sub_category_id)
	{
		$category_name = $this->subscribe_model->sub_category_name($category_id,$sub_category_id);

		return $category_name;
	}
	function export_member()
	{
		require_once 'application/libraries/PHPExcel/Classes/PHPExcel.php';

        $query                  = ""
                . " SELECT          faq.*"
                . " FROM            faq AS faq"
                . "";
        $rs             = $this->db->query($query)->result_array();
        $len            = count($rs);
        
        $TOTAL_PRICE    = 0;
        
        
        $objPHPExcel    = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle('SCG Packaging Subscribe');
        $worksheet      = $objPHPExcel->setActiveSheetIndex(0);
        $rowIndex       = 1;
        $page           = 1;
        $worksheet->setTitle('Page '.$page);
        $worksheet  ->setCellValueByColumnAndRow(0, 1, 'Email')
                    ->setCellValueByColumnAndRow(1, 1, 'Create date')
                    ->setCellValueByColumnAndRow(2, 1, 'Ip Address');
        $worksheet->getColumnDimension('A')->setWidth(30);
        $worksheet->getColumnDimension('B')->setWidth(30);
        $worksheet->getColumnDimension('C')->setWidth(30);
        for( $i=0;  $i<$len;  $i++ ){
            $rowIndex++;
            
            $worksheet->setCellValueByColumnAndRow(0, $rowIndex, $rs[$i]['faq_email']);
            $worksheet->setCellValueByColumnAndRow(1, $rowIndex, $rs[$i]['create_date']);
            $worksheet->setCellValueByColumnAndRow(2, $rowIndex, $rs[$i]['user_ip']);
        }
        $rowIndex++;
        #$worksheet->setCellValueByColumnAndRow(0, $rowIndex, 'Total');
        #$worksheet->setCellValueByColumnAndRow(1, $rowIndex, number_format($TOTAL_PRICE));
        
        
        /*
         header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
         header('Content-Disposition: attachment;filename="01simple.xlsx"');
         header('Cache-Control: max-age=0');
        */
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Email_Subscribe' . date('Y-m-d') . '.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
	}

}