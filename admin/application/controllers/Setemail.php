<?php
class Setemail extends CI_Controller{
	
	var $_data = array();
	var $_menu_name = "";
	var $menu;
	var $submenu;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('admin_library');
		$this->admin_library->forceLogin();
		$this->load->model('setemail_model');

		$this->path 	= 	$this->uri->ruri_string();
		$this->menu 	=	$this->admin_library->getMenu($this->uri->segment(1));
		$this->submenu 	=	$this->admin_library->getSubMenu($this->uri->segment(1), $this->uri->segment(2));
	}
	public function index()
	{
		
		$rs = $this->setemail_model->checkdataTable();
		
		if($rs->num_rows() > 0){
			$row = $rs->row_array();
			admin_redirect("setemail/edit/".$row['content_id']);
		}else{
			admin_redirect("setemail/add");
				
		}
	}
	public function add()
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];
		
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
		if($this->form_validation->run()===false){
			
			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata('error_message');
			$this->admin_library->setTitle("glyphicon-envelope");
			$this->admin_library->setDetail("เพิ่มอีเมลใหม่");
			$this->admin_library->view("setemail/add",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			
			$content_id = $this->setemail_model->addData();
			
			$this->setemail_model->updateContent(
				$content_id,
				$this->input->post("content_email"),
				$this->input->post("content_cc"),
				$this->input->post("content_mail_paysbuy"),
				$this->input->post("content_psbID"),
				$this->input->post("content_pin"),
				$this->input->post("content_secureCode")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("setemail/index");
		}
	}
	public function edit($content_id)
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];		

		$this->_data['row'] = $this->setemail_model->getDetail($content_id);

		if(!$this->_data['row']){
			admin_redirect("setemail/index");
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");	
		}
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
	
		if($this->form_validation->run()===false){

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle("glyphicon-envelope");
			$this->admin_library->setDetail("แก้ไขอีเมล");
			$this->admin_library->view("setemail/edit",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			

			$this->setemail_model->updateContent(
				$this->input->post("content_id"),
				$this->input->post("content_email"),
				$this->input->post("content_cc"),
				$this->input->post("content_mail_paysbuy"),
				$this->input->post("content_psbID"),
				$this->input->post("content_pin"),
				$this->input->post("content_secureCode")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("setemail/edit/".$content_id);
		}
	}

	public function main($content_id="2")
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['row'] = $this->setemail_model->getDetail($content_id);

		if(!$this->_data['row']){
			admin_redirect("setemail/index");
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");
		}
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
	
		if($this->form_validation->run()===false){

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
			$this->admin_library->setDetail($this->submenu['menu_title']);
			$this->admin_library->view("setemail/edit",$this->_data); 
			$this->admin_library->output($this->path);
		}else{
			

			$this->setemail_model->updateContent(
				$this->input->post("content_id"),
				$this->input->post("content_email"),
				$this->input->post("content_cc"),
				$this->input->post("content_mail_paysbuy"),
				$this->input->post("content_psbID"),
				$this->input->post("content_pin"),
				$this->input->post("content_secureCode")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("setemail/main");
		}
	}

	public function register($content_id="3")
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['row'] = $this->setemail_model->getDetail($content_id);

		$lang = "TH";
		$this->load->model('lang_model');
		($lang=="TH")?$this->_data['text_lang'] = $this->lang_model->langTH():$this->_data['text_lang'] = $this->lang_model->langEN();

		if(!$this->_data['row']){
			admin_redirect("setemail/index");
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");
		}
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
	
		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
			$this->admin_library->setDetail($this->submenu['menu_title']);
			$this->admin_library->view("setemail/edit_confirm_register",$this->_data); 
			$this->admin_library->output($this->path);
		} else {
			

			$this->setemail_model->updateContent(
				$this->input->post("content_id"),
				$this->input->post("content_detail"),
				$this->input->post("content_email"),
				$this->input->post("content_cc"),
				$this->input->post("content_mail_paysbuy"),
				$this->input->post("content_psbID"),
				$this->input->post("content_pin"),
				$this->input->post("content_secureCode")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("setemail/register");
		}
	}

	public function reset_password($content_id="4")
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['row'] = $this->setemail_model->getDetail($content_id);

		$lang = "TH";
		$this->load->model('lang_model');
		($lang=="TH")?$this->_data['text_lang'] = $this->lang_model->langTH():$this->_data['text_lang'] = $this->lang_model->langEN();

		if(!$this->_data['row']){
			admin_redirect("setemail/index");
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");
		}
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
	
		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
			$this->admin_library->setDetail($this->submenu['menu_title']);
			$this->admin_library->view("setemail/edit_reset_password",$this->_data); 
			$this->admin_library->output($this->path);
		} else {
			

			$this->setemail_model->updateContent(
				$this->input->post("content_id"),
				$this->input->post("content_detail"),
				$this->input->post("content_email"),
				$this->input->post("content_cc"),
				$this->input->post("content_mail_paysbuy"),
				$this->input->post("content_psbID"),
				$this->input->post("content_pin"),
				$this->input->post("content_secureCode")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("setemail/reset_password");
		}
	}

	public function contact($content_id="5")
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['row'] = $this->setemail_model->getDetail($content_id);

		$lang = "TH";
		$this->load->model('lang_model');
		($lang=="TH")?$this->_data['text_lang'] = $this->lang_model->langTH():$this->_data['text_lang'] = $this->lang_model->langEN();

		if(!$this->_data['row']){
			admin_redirect("setemail/index");
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");
		}
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
	
		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
			$this->admin_library->setDetail($this->submenu['menu_title']);
			$this->admin_library->view("setemail/edit_email_contact",$this->_data); 
			$this->admin_library->output($this->path);
		} else {
			

			$this->setemail_model->updateContent(
				$this->input->post("content_id"),
				$this->input->post("content_detail"),
				$this->input->post("content_email"),
				$this->input->post("content_cc"),
				$this->input->post("content_mail_paysbuy"),
				$this->input->post("content_psbID"),
				$this->input->post("content_pin"),
				$this->input->post("content_secureCode")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("setemail/contact");
		}
	}

	public function order_confirmation($content_id="10", $lang = "TH")
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['row'] = $this->setemail_model->getDetail($content_id);

		$this->load->model('lang_model');
		($lang=="TH") ? $this->_data['text_lang'] = $this->lang_model->langTH() : $this->_data['text_lang'] = $this->lang_model->langEN();

		if(!$this->_data['row']){
			admin_redirect("setemail/index");
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");
		}
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
	
		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
			$this->admin_library->setDetail($this->submenu['menu_title']);
			$this->admin_library->view("setemail/edit_order_confirmation",$this->_data); 
			$this->admin_library->output($this->path);
		} else {
			

			$this->setemail_model->updateContent(
				$this->input->post("content_id"),
				$this->input->post("content_detail"),
				$this->input->post("content_email"),
				$this->input->post("content_cc"),
				$this->input->post("content_mail_paysbuy"),
				$this->input->post("content_psbID"),
				$this->input->post("content_pin"),
				$this->input->post("content_secureCode")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("setemail/order_confirmation");
		}
	}

	// public function confirm_order_creditcard($content_id="7")
	// {
	// 	$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
	// 	$this->_data['_menu_link']  	= $this->menu['menu_link'];

	// 	$this->_data['row'] = $this->setemail_model->getDetail($content_id);

	// 	$lang = "TH";
	// 	$this->load->model('lang_model');
	// 	($lang=="TH")?$this->_data['text_lang'] = $this->lang_model->langTH():$this->_data['text_lang'] = $this->lang_model->langEN();

	// 	if(!$this->_data['row']){
	// 		admin_redirect("setemail/index");
	// 	}

	// 	if(!$this->_data['row']){
	// 		show_error("ไม่พบข้อมูลในระบบ");
	// 	}
		
	// 	$this->load->library('form_validation');	
	// 	$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
	
	// 	if($this->form_validation->run()===false) {

	// 		$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
	// 		$this->_data['error_message'] = $this->session->flashdata("error_message");
			
	// 		$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
	// 		$this->admin_library->setDetail($this->submenu['menu_title']);
	// 		$this->admin_library->view("setemail/edit_confirm_order_creditcard",$this->_data); 
	// 		$this->admin_library->output($this->path);
	// 	} else {
			

	// 		$this->setemail_model->updateContent(
	// 			$this->input->post("content_id"),
	// 			$this->input->post("content_detail"),
	// 			$this->input->post("content_email"),
	// 			$this->input->post("content_cc"),
	// 			$this->input->post("content_mail_paysbuy"),
	// 			$this->input->post("content_psbID"),
	// 			$this->input->post("content_pin"),
	// 			$this->input->post("content_secureCode")
	// 		);
			
	// 		$this->session->set_flashdata("success_message","Content has been create.");
	// 		admin_redirect("setemail/confirm_order_creditcard");
	// 	}
	// }

	public function payment_counterservice($content_id="8")
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['row'] = $this->setemail_model->getDetail($content_id);

		$lang = "TH";
		$this->load->model('lang_model');
		($lang=="TH")?$this->_data['text_lang'] = $this->lang_model->langTH():$this->_data['text_lang'] = $this->lang_model->langEN();

		if(!$this->_data['row']){
			admin_redirect("setemail/index");
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");
		}
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
	
		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
			$this->admin_library->setDetail($this->submenu['menu_title']);
			$this->admin_library->view("setemail/edit_payment_counterservice",$this->_data); 
			$this->admin_library->output($this->path);
		} else {
			

			$this->setemail_model->updateContent(
				$this->input->post("content_id"),
				$this->input->post("content_detail"),
				$this->input->post("content_email"),
				$this->input->post("content_cc"),
				$this->input->post("content_mail_paysbuy"),
				$this->input->post("content_psbID"),
				$this->input->post("content_pin"),
				$this->input->post("content_secureCode")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("setemail/payment_counterservice");
		}
	}

	public function payment_creditcard($content_id="9")
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['row'] = $this->setemail_model->getDetail($content_id);

		$lang = "TH";
		$this->load->model('lang_model');
		($lang=="TH")?$this->_data['text_lang'] = $this->lang_model->langTH():$this->_data['text_lang'] = $this->lang_model->langEN();

		if(!$this->_data['row']){
			admin_redirect("setemail/index");
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");
		}
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
	
		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
			$this->admin_library->setDetail($this->submenu['menu_title']);
			$this->admin_library->view("setemail/edit_payment_creditcard",$this->_data); 
			$this->admin_library->output($this->path);
		} else {
			

			$this->setemail_model->updateContent(
				$this->input->post("content_id"),
				$this->input->post("content_detail"),
				$this->input->post("content_email"),
				$this->input->post("content_cc"),
				$this->input->post("content_mail_paysbuy"),
				$this->input->post("content_psbID"),
				$this->input->post("content_pin"),
				$this->input->post("content_secureCode")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("setemail/payment_creditcard");
		}
	}
	
	public function email_voucher($content_id="10")
	{
		$this->_data['_menu_name'] 		= $this->submenu['menu_label'];
		$this->_data['_menu_link']  	= $this->menu['menu_link'];

		$this->_data['row'] = $this->setemail_model->getDetail($content_id);

		$lang = "TH";
		$this->load->model('lang_model');
		($lang=="TH")?$this->_data['text_lang'] = $this->lang_model->langTH():$this->_data['text_lang'] = $this->lang_model->langEN();

		if(!$this->_data['row']){
			admin_redirect("setemail/index");
		}

		if(!$this->_data['row']){
			show_error("ไม่พบข้อมูลในระบบ");
		}
		
		$this->load->library('form_validation');	
		$this->form_validation->set_rules("content_email","E-mail","trim|required|valid_emails|max_length[255]");
	
		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors('<div class="alert alert-error"><button class="close" data-dismiss="alert">×</button>','</div>');
			$this->_data['error_message'] = $this->session->flashdata("error_message");
			
			$this->admin_library->setTitle($this->_data['_menu_name'],$this->submenu['menu_icon']);
			$this->admin_library->setDetail($this->submenu['menu_title']);
			$this->admin_library->view("setemail/edit_email_voucher",$this->_data); 
			$this->admin_library->output($this->path);
		} else {
			

			$this->setemail_model->updateContent(
				$this->input->post("content_id"),
				$this->input->post("content_detail"),
				$this->input->post("content_email"),
				$this->input->post("content_cc"),
				$this->input->post("content_mail_paysbuy"),
				$this->input->post("content_psbID"),
				$this->input->post("content_pin"),
				$this->input->post("content_secureCode")
			);
			
			$this->session->set_flashdata("success_message","Content has been create.");
			admin_redirect("setemail/email_voucher");
		}
	}
}