<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Logs_library {

	private $ci;
	private $_data = array();

	public function __construct()
	{
		$this->ci = & get_instance();
		$this->ci->load->library('parser');
		$this->ci->load->library('admin_library');
	}

	public function access_log(array $data = array()) {

		$default = array(
			'post_by' 		=> $this->ci->admin_library->userdata('user_id'),
			'post_date' 	=> time(),
			'post_ip' 		=> $this->ci->input->ip_address()
		);

		$this->ci->db->insert("system_access_log", array_merge($data, $default));
	}

	public function users_log(array $data = array()) {

		$default = array(
			'post_by' 		=> $this->ci->admin_library->userdata('user_id'),
			'post_date' 	=> time(),
			'post_ip' 		=> $this->ci->input->ip_address()
		);

		$this->ci->db->insert("system_users_log", array_merge($data, $default));
	}

	public function menu_backend_log(array $data = array()) {

		$menu_link = $data['menu_link'];
		unset($data['menu_link']);

		$default = array(
			'post_by' 		=> $this->ci->admin_library->userdata('user_id'),
			'post_date' 	=> time(),
			'post_ip' 		=> $this->ci->input->ip_address()
		);

		$test = $this->ci->db->insert($menu_link."_log", array_merge($data, $default));
	}

}