<?php
class Verification_library {

	private $ci;
	private $_data = array();
	public function __construct()
	{
		$this->ci = & get_instance();
	}

	public function check_session($option=true)
	{
		$action_url = ( $this->ci->input->get('action_url') ? $this->ci->input->get('action_url') : 'age-verification' );

		$session_id = $this->ci->session->session_id;
		$check_session = $this->ci->db->like('id', $session_id)
											->where('countries_id !=', 0)
												->where('year !=', '')
													->count_all_results('ci_sessions_age');

		if(empty($check_session)) {

			if($option) {

				redirect(base_url('age-verification?action_url='.$this->ci->uri->uri_string()));
			}
		} else {

			if($option) {

				return true;
			} else {

				redirect(base_url('home'));
			}
		}
	}
}