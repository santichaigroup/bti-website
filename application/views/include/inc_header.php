<?php
  $content_facebook   = ( $address['content_facebook'] ? '<a href="'.$address['content_facebook'].'"><span><span class="ftlo-facebook"></span></span></a>' : '' );
  $content_twitter    = ( $address['content_twitter'] ? '<a href="'.$address['content_twitter'].'"><span><span class="ftlo-twitter-bird"></span></span></a>' : '' );
  $content_instagram  = ( $address['content_instagram'] ? '<a href="'.$address['content_instagram'].'"><span><span class="ftlo-instagram-filled"></span></span></a>' : '' );
?>

<!--[if lt IE 8]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="beforeHeader">
  <div class="container">
    <div class="beforeHeaderInner">
      <ul>
        <li><a href="http://boonrawd.co.th/" target="_blank"><span>BOONRAWD.CO.TH</span></a></li>
        <li><a href="http://www.singhaonlineshop.com" target="_blank"><span>SINGHA ONLINE SHOP</span></a></li>
        <li><a href="http://www.singha.com" target="_blank"><span>SINGHA.COM</span></a></li>
      </ul>
    </div>
  </div>
</div>
<div class="headerWrap">
  <div class="container">
    <div class="header">
      <div class="headerBrand">
        <a href="<?php echo base_url('home'); ?>" class="headerBrandInner"><span><figure class="image"><img src="img/logo-singha.png"></figure></span></a>
        <button class="btn btnHamburger" data-toggle="mainNavigation">
          <span class="btnHamburgerInner">
            <span>button </span>
            <span>toggle </span>
            <span>header </span>
          </span>
          <span class="btnHamburgerText">CLOSE</span>
        </button>           
      </div>
      <nav class="mainNavigation">
        <div class="container">
          <div class="mainNavigationInner">
            <ul class="navItems">
              <?php 
              
              $navigationItems = array(
                'home',
                'business',
                'products',
                'news',
                'contact');
              $activePage = array(
                'home',
                'business',
                'products',
                'news',
                'contact');
              $navigationLinks = array(
                'home',
                '#',
                'products',
                'news',
                'contact');
              $navigationSubItemsBusiness = array(
                'About Us',
                'Representatives');
              $navigationSubItemsBusinessLinks = array(
                'business/about',
                'business/representatives');

              for ($i=0; $i < count($navigationItems) ; $i++) : ?>
              <li role="presentation" class="navItem <?php echo($url_path === $activePage[$i]? 'active':''); ?>">
                <?php if ($navigationItems[$i] === 'business'): ?>
                <a href="#navSubCollapseBusiness" data-toggle="collapse"><span><?php echo($navigationItems[$i]); ?></span></a>
                <div class="collapse <?php echo($url_path === $activePage[$i]? 'in':''); ?>" id="navSubCollapseBusiness">
                  <ul class="navSubItems">
                    <?php for ($ii=0; $ii < count($navigationSubItemsBusiness); $ii++) : ?>
                    <li class="navSubItem <?php echo($url_path_sub === $navigationSubItemsBusinessLinks[$ii]? 'active':''); ?>">
                      <a href="<?php echo base_url($navigationSubItemsBusinessLinks[$ii]); ?>"><span><?php echo($navigationSubItemsBusiness[$ii]); ?></span></a>
                    </li>
                    <?php endfor; ?>
                  </ul>
                </div>
                <?php elseif ($navigationItems[$i] === 'products'): ?>
                <a href="#navSubCollapseProducts" data-toggle="collapse"><span><?php echo($navigationItems[$i]); ?></span></a>
                <div class="collapse <?php echo($url_path === $activePage[$i]? 'in':''); ?>" id="navSubCollapseProducts">
                  <ul class="navSubItems">
                    <?php for ($ii=0; $ii < count($product_category) ; $ii++) : ?>
                    <li class="navSubItem <?php echo($url_path_sub == $product_category[$ii]['content_seo'] ? 'active':''); ?>">
                      <a href="<?php echo site_url($product_category[$ii]['content_seo']); ?>"><span><?php echo $product_category[$ii]['content_subject']; ?></span></a>
                    </li>
                    <?php endfor; ?>
                  </ul>
                </div>
                <?php else : ?>
                <a href="<?php echo base_url($navigationLinks[$i]); ?>"><span><?php echo($navigationItems[$i]); ?></span></a>
                <?php endif; ?>
              </li>      
              <?php endfor; ?>
              <li role="presentation" class="navItem isGlobalNav">
                <a href="http://boonrawd.co.th/" target="_blank"><span>BOONRAWD.CO.TH</span></a>
                <a href="http://www.singhaonlineshop.com/ " target="_blank"><span>SINGHA ONLINE SHOP</span></a>
                <a href="http://www.singha.com" target="_blank"><span>SINGHA.COM</span></a>
              </li>
              <li role="presentation" class="navItem isSocial">
                <?php echo $content_facebook; ?>
                <?php echo $content_twitter; ?>
                <?php echo $content_instagram; ?>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </div>
</div>