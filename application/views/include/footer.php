<?php
  $content_group      = ( $address['content_group'] ? $address['content_group'] .'<br>' : '' );
  $content_address    = ( $address['content_address'] ? $address['content_address'] : '' );
  $content_phone      = ( $address['content_phone'] ? 'Tel: '.$address['content_phone'].' <br class="visible-xs"> ' : '' );
  $content_fax        = ( $address['content_fax'] ? 'Fax: '.$address['content_fax'] : '' );

  $content_facebook   = ( $address['content_facebook'] ? '<a href="'.$address['content_facebook'].'"><span><span class="ftlo-facebook"></span></span></a>' : '' );
  $content_twitter    = ( $address['content_twitter'] ? '<a href="'.$address['content_twitter'].'"><span><span class="ftlo-twitter-bird"></span></span></a>' : '' );
  $content_instagram  = ( $address['content_instagram'] ? '<a href="'.$address['content_instagram'].'"><span><span class="ftlo-instagram-filled"></span></span></a>' : '' );
?>

<div class="sgh_footer">
  <div class="container">
    <div class="sgh_footerInner">
      <div class="sgh_footerGoToTop">
        <a href="#">
          <span class="ftlo-up"></span>
        </a>
      </div>
      <div class="row">
        <div class="col-md-5 col-sm-6">
          <div class="sgh_footerBrand">
            <a href=""><figure class="image"><img src="img/logo-singha.png"></figure></a>
          </div>
          <div class="sgh_footerAddress">
            <address>
              <?php echo $content_group; ?>
              <?php echo $content_address; ?>
            </address>
            <p>Tel: <?php echo $content_phone; ?> <?php echo $content_fax; ?></p>
            <p><b><a href="<?php echo base_url('contact'); ?>"><span>SINGHA CORPORATION MAP</span><span class="ftlo-location"></span></a></b></p>
          </div>
        </div>
        <div class="col-md-7 col-sm-6">
          <div class="footerSocial">
            <div class="footerSocialInner">
              <?php echo $content_facebook; ?>
              <?php echo $content_twitter; ?>
              <?php echo $content_instagram; ?>
            </div>
          </div>
          <div class="sgh_footerCopyright">
            <!-- <span class="sgh_footerCopyrightLinks"><a href="">Terms &amp; Conditions</a>  |  <a href="">Privacy Policy</a>  |  <a href="">Responsibility Statement </a></span> -->
            <span class="sgh_footerCopyrightText">© COPYRIGHTS 2014 SINGHA CORPORATION CO., LTD. ALL RIGHTS RESERVED.</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>