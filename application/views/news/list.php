<!DOCTYPE html>
<html>
<head>
  <?php echo $header; ?>
</head>
<body>
  <?php echo $inc_header; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">

          <?php if(!empty($highlight)) { ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
              <?php
              if(is_array($highlight['images'])) {

                foreach ($highlight['images'] as $k => $v) {
                  
                  if($v['type']=="pc") {
              ?>
                  <style>
                    @media (min-width: 768px) {
                      .sgh_item:nth-child(<?php echo($k) + 1; ?>) .thumbnailHighlight .image {
                        background-image: url(<?php echo $base_url."admin/public/uploads/highlight/images/".$v['attachment_name']; ?>);
                      }
                    }
                  </style>
              <?php
                  }

                  if($v['type']=="mb") {
              ?>
                  <div class="image">
                    <img class="visible-xs" src="<?php echo $base_url."admin/public/uploads/highlight/images/".$v['attachment_name']; ?>" alt="<?php echo $v['attachment_detail']; ?>">
                  </div>
              <?php
                  }
                }
              }
              ?>
              </div>
            </div>
          </div>
          <?php } ?>

        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionNewsItems">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader text-center">
            <h1 class="titleText">NEWS</h1>
          </div>
          <div class="row load_more">
          <?php
          if(!empty($news)) {
            foreach($news as $n => $new) {

              $content_subject    = $new['content_subject'];
              // $content_detail     = substr_utf8(strip_tags($new['content_detail'],''),0,200);
              $content_url        = ( $new['content_seo'] ? site_url($new['content_seo']) : 'javascript:;' );
              $post_date          = date('d M Y', time($new['post_date']));
          ?>
              <div class="col-sm-4 col-md-3">
                <div class="thumbnailNews">
                  <div class="imageWrap">
                    <a href="<?php echo $content_url; ?>">
                      <?php
                      if($new['content_thumbnail']) {

                        $content_thumbnail = $base_url."admin/public/uploads/news/images/".$new['content_thumbnail']['attachment_name'];
                      ?>
                        <figure class="image" data-crop="news"><img src="<?php echo $content_thumbnail; ?>" alt="<?php echo $new['content_thumbnail']['attachment_detail']; ?>"></figure>
                      <?php
                      }
                      ?>
                    </a>
                  </div>
                  <div class="captionWrap">
                    <div class="caption">
                      <div class="eqh">
                        <div class="date"><?php echo $post_date; ?></div>
                        <h3 class="titleText"><?php echo $content_subject; ?></h3>
                      </div>
                      <div class="button"><a href="<?php echo $content_url; ?>"><span><span>More</span><span class="ftlo-right"></span></span></a></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } } ?>
          </div>
        </div>
      </div>
    </div>

    <?php if($news_all>8) { ?>
      <div class="sgh_section sgh_loadmore">
        <div class="sgh_sectionInner">
          <a href="javascript:;" class="btn btnTheme isHighlight isDown loadMore"><span>Load More <span class="ftlo-down"></span></span></a><div class="container">
        </div>
        </div>
      </div>
    <?php } ?>
  </div>
  <?php echo $footer; ?>




  <?php echo $inc_globalJS; ?>
  <script>
    var cols;
    // define page by classname
    document.querySelector('html').classList.add('newsPage');
    // $(document).ready(function () {
    //   tweenSectionHeader('.sgh_sectionNewsItems');
    //   $('.sgh_sectionNewsItems .sgh_sectionInner > .row > *').each(function (index, element) {
    //     tweenProducts('.' + element.classList[0] + ':nth-child(' + (index + 1) + ')', index)
    //   });
    // });

    // function tweenProducts (item, delay) {
    //   cols = 4;
    //   $(window).resize(function () { 
    //     getCols()
    //   });
    //   getCols()
    //   function getCols () {
    //     cols = 4
    //     if ($(window).width() < 992 && $(window).width() >= 768) {
    //       cols = 3
    //     } else if ($(window).width() < 768) {
    //       cols = 1
    //     }
    //   }      
    //   var tl = new TimelineLite();
    //   tl.from(item + ' .thumbnailNews ', 0.4, {y:-100, opacity:0, scale: 1.05, delay: (delay % cols) * 0.4})
    //   .from(item + ' .thumbnailNews .imageWrap .image', 0.8, {y:-100, opacity:0, scale: 1.05})
    //   .from(item + ' .thumbnailNews .caption .date', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6')
    //   .from(item + ' .thumbnailNews .caption .titleText', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6')
    //   .from(item + ' .thumbnailNews .caption .desc', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6')
    //   .from(item + ' .thumbnailNews .caption .button', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6');
    //   var scene = new ScrollMagic.Scene({
    //     triggerElement: item,
    //     triggerHook: 0.8
    //   })
    //   .setTween(tl)
    //   // .addIndicators()
    //   .addTo(ScrollMagicController); 
    //   return scene;     
    // }    

    $(function() {

      var page = 1;

      $('.loadMore').click(function() {

          $.ajax({
            url: "<?php echo base_url('news/load_more'); ?>",
            method: "POST",
            data: { 
              lang_id: '<?php echo $lang_id; ?>',
              start: page,
              '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            },
            success: function(data) {

              page++;
              var result = jQuery.parseJSON(data);
              if(!result['status']) {

                $('.sgh_loadmore').remove();
              }

              $('.load_more').append(result['html']);
              thumbnailImage();
            }
          });
      });
    });
  </script>
</body>

</html>
