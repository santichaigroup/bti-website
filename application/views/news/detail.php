<!DOCTYPE html>
<html>
<head>
  <?php echo $header; ?>
</head>
<body>
  <?php echo $inc_header; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight isSM">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php for ($i=0; $i < 1 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
                <style>
                  @media (min-width: 768px) {
                    .sgh_item:nth-child(<?php echo($i) + 1; ?>) .thumbnailHighlight .image {
                      background-image: url(img/highlight-news.png);
                    }
                  }
                </style>
                <div class="image">
                  <img class="visible-xs" src="img/highlightMobile-news.png" alt="">
                </div>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>

    <?php
      $content_subject    = $news['content_subject'];
      $content_detail     = $news['content_detail'];
      $content_url        = ( $news['content_seo'] ? site_url($news['content_seo']) : 'javascript:;' );
      $post_date          = date('d M Y', time($news['post_date']));
    ?>

    <div class="sgh_section sgh_sectionArticle isNewsDetail">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionArticleFeature">
            <div class="breadcrumbTheme">
              <nav>
                <span><a href="<?php echo base_url('news'); ?>">NEWS</a></span>
                <span><span><?php echo $content_subject; ?></span></span>
              </nav>
            </div>
            <div class="simpleButtons">
              <ul class="list-inline">
                <li><a href="javascript:;" onclick="window.history.go(-1); return false;" >BACK</a></li>
                <li> | </li>
                <li><a href="javascript:;" onclick="window.print()">PRINT</a></li>
              </ul>
            </div>
          </div>
          <div class="articleTheme sgh_editor">
            <article>
              <header class="sgh_sectionHeader text-center isSM">
                <h1 class="titleText"><?php echo $content_subject; ?></h1>
              </header>
              <span>
                <?php echo $content_detail; ?>
              </span>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php echo $footer; ?>




  <?php echo $inc_globalJS; ?>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('newsDetailPage');
  </script>
</body>

</html>
