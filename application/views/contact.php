<!DOCTYPE html>
<html>
<head>
  <?php echo $header; ?>
  <style type="text/css">
    .alert.alert-error {
        border-color: #d73925;
        background-color: #dd4b39 !important;
        color: #fff;
    }
  </style>
</head>
<body>
  <?php echo $inc_header; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight isGmap">
      <div class="sgh_sectionInner">
        <div class="thumbnailHighlight">
          <div class="imageWrap">
            <div class="image">
              <div id="map"></div>
              <script>
                function initMap() {
                  var latAndLng = {
                    lat: <?php echo $address['content_pinterest']; ?>,
                    lng: <?php echo $address['content_line']; ?>
                  }
                  var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 17,
                    center: {
                      lat: <?php echo $address['content_pinterest']; ?>,
                      lng: <?php echo $address['content_line']; ?>
                    }
                  });
                  var contentString = 
                      '<div class="map-content">'+
                        '<div class="map-icon">'+
                          '<img src="img/map-logo-singha.png" alt="">'+
                        '</div>'+
                        '<div class="map-content-body">'+
                          '<p><?php echo $address['content_detail']; ?></p>'+
                          '<p><a href="https://www.google.com/maps/place/Boonrawdtrading+International+Co.,LTD./@13.7878431,100.5144131,17z/data=!4m5!3m4!1s0x30e29be5a76dc9c5:0x34707b685a7dfdc8!8m2!3d13.7880222!4d100.5170166" target="_blank">GET DIRECTION</a></p>'+
                        '</div>'+
                      '</div>';

                  var infowindow = new google.maps.InfoWindow({
                    content: contentString,
                    maxWidth: 400
                  });
                  var image = 'img/pin-map.svg';
                  var marker = new google.maps.Marker({
                    position: latAndLng,
                    map: map,
                    icon: image
                  });
                  marker.addListener('click', function() {
                    infowindow.open(map, marker);
                  });  
                  setTimeout(() => {
                    infowindow.open(map, marker);
                  }, 500);
                  google.maps.event.addListener(infowindow, 'domready', function() {
                    var color = '#fac214';
                    $('#map .gm-style-iw').css({'color': '#000000','font-family' : 'korolev_condensedmedium', 'font-size': '16px', 'padding-top': '20px'});
                    $('#map .gm-style-iw a').css({'color': '#000000', 'text-decoration': 'underline'});
                    $('#map .gm-style-iw .map-icon').css('margin-bottom', '20px');
                    $('#map .gm-style-iw').prev().find(' > div:nth-child(4)').css('background-color', color);
                    $('#map .gm-style-iw').prev().find(' > div:nth-child(3) > div > div').css('background-color', color);
                  })
                }
              </script>
              <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDY7AE3e-2KArsOjo_GgBRI5nRAr1D_uew&callback=initMap">
              </script>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php
      $content_group      = ( $address['content_group'] ? $address['content_group'] .'<br>' : '' );
      $content_address    = ( $address['content_address'] ? $address['content_address'] : '' );
      $content_phone      = ( $address['content_phone'] ? 'Tel: '.$address['content_phone'] : '' );
      $content_fax        = ( $address['content_fax'] ? 'Fax: '.$address['content_fax'] : '' );
    ?>

    <div class="sgh_section sgh_sectionContactDetail">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader text-center">
            <h1 class="titleText">CONTACT</h1>
          </div>
          <div class="row">
            <div class="col-sm-5">
              <address class="contactAddress">
                <h2 class="sgh_sectionSubHeader"><?php echo $content_group; ?></h2>
                <ul class="list-unstyled">
                  <li> <span class="ftlo-location"></span> <span><?php echo $content_address; ?></span></li>
                  <li> <span class="ftlo-phone"></span> <span><?php echo $content_phone; ?></span></li>
                  <li> <span class="ftlo-fax"></span> <span><?php echo $content_fax; ?></span></li>
                </ul>
              </address>
            </div>
            <div class="col-sm-7">
              <div class="formTheme">
                  <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <?php if(@$validation_errors!=NULL){ ?>
                      <div class="form-group">
                          <div class="alert alert-error">
                              <button class="close" data-dismiss="alert">×</button>
                              <?php echo @$validation_errors; ?>
                          </div>
                      </div>
                      <?php }?>
                    </div>
                  </div>
                <?php echo form_open(base_url('contact'), array( 'name' => 'form-email', 'method' => 'post' ));?>
                  <h2 class="sgh_sectionSubHeader">LEAVE A MESSAGE</h2>
                  <div class="row">
                    <div class="col-sm-12 col-md-6">
                      <div class="form-group">
                        <select class="selectpicker form-control" name="email_type" id="email_type" title="PRODUCT *">
                          <?php
                          if(!empty($categories)) {
                            foreach ($categories as $ca => $category) {
                          ?>
                            <option value="<?php echo $category['content_id']; ?>"><?php echo $category['content_subject']; ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>              
                      </div>                
                    </div>
                    <div class="col-sm-12 col-md-6">
                      <div class="form-group">
                          <select class="selectpicker form-control" name="country" id="country" title="COUNTRY *"></select>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                      <div class="form-group">
                        <label class="form-control-floatText">
                          <div class="label-control">NAME <span class="text-require">*</span></div>
                          <input type="text" class="form-control" name="name" value="<?php echo set_value('name'); ?>">
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                      <div class="form-group">
                        <label class="form-control-floatText">
                          <div class="label-control">E-MAIL <span class="text-require">*</span></div>
                          <input type="text" class="form-control" name="email" value="<?php echo set_value('email'); ?>">
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                      <div class="form-group">
                        <label class="form-control-floatText">
                          <div class="label-control">TEL <span class="text-require">*</span></div>
                          <input type="text" class="form-control" name="tel" value="<?php echo set_value('tel'); ?>">
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label class="form-control-floatText isTextArea">
                          <div class="label-control">MESSAGE <span class="text-require">*</span></div>
                          <textarea class="form-control" cols="30" rows="10" name="message"><?php echo set_value('message'); ?></textarea>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group isReCaptcha">
                    
                    <div class="g-recaptcha" data-sitekey="6Ldb8WkUAAAAAFpXDU5wN3gCzbwMv-i6cHdvGJEY"></div>

                  </div>
                  <div class="form-group">
                    <button class="btn btnTheme"><span>SUBMIT <span class="ftlo-right"></span></span></button>
                  </div>
                <?php echo form_close();?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php echo $footer; ?>




  <?php echo $inc_globalJS; ?>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <!-- <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
      async defer>
  </script> -->
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('contactPage');

    $(function() {

      $('#email_type').change(function() {

        $.ajax({
          url: "<?php echo base_url('contact/load_more'); ?>",
          method: "POST",
          data: { 
            lang_id: '<?php echo $lang_id; ?>',
            email_type: $('#email_type').val(),
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          },
          success: function(data) {

            var result = jQuery.parseJSON(data);
            if(result['status']) {

              $('#country').html('');
              $.each(result['html'], function(key, value) {
                 $('#country')
                   .append($("<option></option>")
                    .attr("value",value['country_id'])
                      .text(value['content_country'])); 
              });

              $('#country').selectpicker('refresh');
            }
          }
        });
      });
    });
  </script>
</body>

</html>
