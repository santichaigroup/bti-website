<!DOCTYPE html>
<html>
<head>
  <?php echo $header; ?>
</head>
<body>
  <?php echo $inc_header; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php 
          foreach($banners as $key => $banner) : 

            // $content_subject  = explode(' ', $banner['content_subject']);
            $content_subject  = $banner['content_subject'];
            $subject_status   = $banner['subject_status'];
            $content_url      = ( preg_match("@^http://@i",$banner['content_url']) || preg_match("@^https://@i",$banner['content_url'])
                                    ? $banner['content_url'] : null );
            $content_detail   = substr_utf8(strip_tags($banner['content_detail'],''), 0, 180);
          ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
              <?php
              if(@is_array($banner['images'])) {

                foreach ($banner['images'] as $k => $v) {
                  
                  if($v['type']=="pc") {
              ?>
                  <style>
                    @media (min-width: 768px) {
                      .sgh_item:nth-child(<?php echo($key) + 1; ?>) .thumbnailHighlight .image {
                        background-image: url(<?php echo $base_url."admin/public/uploads/banner/images/".$v['attachment_name']; ?>);
                      }
                    }
                  </style>
              <?php
                  }

                  if($v['type']=="mb") {
              ?>
                  <div class="image">
                    <img class="visible-xs" src="<?php echo $base_url."admin/public/uploads/banner/images/".$v['attachment_name']; ?>" alt="<?php echo $v['attachment_detail']; ?>">
                  </div>
              <?php
                  }
                }
              }
              ?>
              </div>
              <div class="captionWrap">
                <div class="container">
                  <div class="caption">
                    <div class="captionInner">
                      <?php if($subject_status) { ?>
                        <h1 class="titleText">
                          <span class="isHL1"><?php echo $content_subject; ?></span>
                        </h1>
                      <?php } if($content_detail) { ?>
                        <div class="desc">
                          <p><?php echo $content_detail; ?></p>
                        </div>
                      <?php } ?>
                    </div>
                    <div class="buttonWrap">
                    <?php
                    if($content_url) {
                    ?>
                      <a href="<?php echo $content_url; ?>" class="btn btnTheme isHighlight" target="_blank">
                        <span>EXPLORE <span class="ftlo-right"></span></span>
                      </a>
                    <?php
                    }
                    ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionProductsHighlight">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader">
            <h2 class="titleText"><?php echo text_lang('products_group', $lang_id); ?></h2>
            <div class="desc">
              <p><?php echo text_lang('products_group_detail', $lang_id); ?></p>
            </div>
          </div>
          <div class="sgh_items">
            <?php 
            if(!empty($categories)) {
              foreach($categories as $c => $category) :

                $content_subject    = $category['content_subject'];
                $content_detail     = substr_utf8(strip_tags($category['content_detail'], ''),0,300);
                $content_url        = ( $category['content_seo'] ? site_url($category['content_seo']) : 'javascript:;' );
            ?>
            <div class="sgh_item" id="sgh_productItem-<?php echo $c; ?>">
              <div class="thumbnailProductsHighlight <?php echo ($c % 2 === 1 ? 'isDiff': ''); ?>">
                <div class="imageWrap">
                  <div class="imageDecor">
                    <?php
                    if(!empty($category['images'])) {
                      foreach ($category['images'] as $i => $image) {
                        if($image['type']=="hl") {

                          $attachment_name = $base_url."admin/public/uploads/product/images/".$image['attachment_name'];
                    ?>
                        <figure class="image" data-crop="products"><img src="<?php echo $attachment_name; ?>" alt="<?php echo $image['attachment_detail']; ?>"></figure>
                    <?php
                        }
                      }
                    }
                    ?>
                  </div>
                </div>
                <div class="captionWrap">
                  <div class="caption">
                    <div class="number">0<?php echo ($c + 1); ?></div>
                    <h3 class="titleText"><span class="titleTextInner"><?php echo $content_subject; ?></span></h3>
                    <div class="desc"><?php echo $content_detail; ?></div>
                    <div class="button"><a href="<?php echo $content_url; ?>" class="btn btnTheme"><span>More <span class="ftlo-right"></span></span></a></div>
                  </div>
                </div>
              </div>
            </div>
            <?php endforeach; } ?>
          </div>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionNewsHighlight">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader hasFloatButton">
            <h2 class="titleText">
              <span><?php echo text_lang('news_update', $lang_id); ?></span>
              <div class="button">
                <a href="<?php echo base_url('news'); ?>">VIEW ALL+</a>
              </div>
            </h2>
            <div class="desc">
              <p><?php echo text_lang('news_update_detail', $lang_id); ?></p>
            </div>
          </div>        
        </div>
        <div class="sgh_items isSlider">
          <?php
          if(!empty($news)) {
            foreach($news as $n => $new) {

              $content_subject    = $new['content_subject'];
              // $content_detail     = substr_utf8(strip_tags($new['content_detail'],''),0,200);
              $content_url        = ( $new['content_seo'] ? site_url($new['content_seo']) : 'javascript:;' );
              $post_date          = date('d M Y', time($new['post_date']));
          ?>
              <div class="sgh_item">
                <div class="thumbnailNews">
                  <div class="imageWrap">
                    <a href="<?php echo $content_url; ?>">
                      <?php
                      if($new['content_thumbnail']) {

                        $content_thumbnail = $base_url."admin/public/uploads/news/images/".$new['content_thumbnail']['attachment_name'];
                      ?>
                        <figure class="image" data-crop="news"><img src="<?php echo $content_thumbnail; ?>" alt="<?php echo $new['content_thumbnail']['attachment_detail']; ?>"></figure>
                      <?php
                      }
                      ?>
                    </a>
                  </div>
                  <div class="captionWrap">
                    <div class="caption">
                      <div class="eqh">
                        <div class="date"><?php echo $post_date; ?></div>
                        <h3 class="titleText"><?php echo $content_subject; ?></h3>
                      </div>
                      <div class="button"><a href="<?php echo $content_url; ?>"><span><span>More</span><span class="ftlo-right"></span></span></a></div>
                    </div>
                  </div>
                </div>
              </div>
          <?php } } ?>
        </div>
      </div>
    </div>
  </div>
  <?php echo $footer; ?>




  <?php echo $inc_globalJS; ?>

  <script>
    // define page by classname
    document.querySelector('html').classList.add('homePage');

    $(document).ready(function () {
      $('.sgh_sectionNewsHighlight .sgh_items.isSlider').flickity({
        cellAlign: 'left',
        prevNextButtons: false
      });      
      tweenSectionHeader('.sgh_sectionProducts');
      tweenProducts('#sgh_productItem-0');
      tweenProducts('#sgh_productItem-1');
      tweenProducts('#sgh_productItem-2');
      tweenProducts('#sgh_productItem-3');
      tweenProducts('#sgh_productItem-4');
      tweenSectionHeader('.sgh_sectionNewsHighlight');
      tweenNews('.sgh_sectionNewsHighlight')
    });

    function tweenProducts (item) {
      var tl = new TimelineLite();
      tl.from(item + ' .thumbnailProductsHighlight .caption .number', 0.5, {y:100, opacity:0})
      .from(item + ' .thumbnailProductsHighlight .caption .titleText', 0.5, {y:100, opacity:0}, '-=0.4')
      .from(item + ' .thumbnailProductsHighlight .caption .desc', 0.2, {y:100, opacity:0}, '-=0.1')
      .from(item + ' .thumbnailProductsHighlight .caption .button', 0.2, {y:100, opacity:0}, '-=0.1')
      .from(item + ' .thumbnailProductsHighlight .imageWrap', 0.6, {x:-200, opacity:0})
      .from(item + ' .thumbnailProductsHighlight .imageWrap .image', 0.6, {x:400}, '-=0.6');
      var scene = new ScrollMagic.Scene({
        triggerElement: item,
        triggerHook: 0.8
      })
      .setTween(tl)
      // .addIndicators()
      .addTo(ScrollMagicController); 
      return scene;     
    }
    function tweenNews (parent) {
      var tl = new TimelineLite();
      tl.from(parent + ' .sgh_items.isSlider .flickity-viewport', 0.3, {y:100, opacity:0})
      .from(parent + ' .sgh_items.isSlider .flickity-page-dots', 0.3, {y:100, opacity:0});
      var scene = new ScrollMagic.Scene({
        triggerElement: parent + ' .flickity-enabled',
        triggerHook: 1
      })
      .setTween(tl)
      // .addIndicators()
      .addTo(ScrollMagicController); 
      return scene;     
    }

  </script>
</body>

</html>