<!DOCTYPE html>
<html>
<head>
  <?php echo $header; ?>
</head>
<body>
  <?php echo $inc_header; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php for ($i=0; $i < 1 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
                <?php
                if(!empty($category['images'])) {
                  foreach ($category['images'] as $c => $image) {

                    if($image['type']=="pc") {
                ?>
                    <style>
                      @media (min-width: 768px) {
                        .sgh_item:nth-child(<?php echo($i) + 1; ?>) .thumbnailHighlight .image {
                          background-image: url(<?php echo $base_url."admin/public/uploads/product/images/".$image['attachment_name']; ?>);
                        }
                      }
                    </style>
                <?php
                    } else if($image['type']=="mb") {
                ?>
                  <div class="image">
                    <img class="visible-xs" src="<?php echo $base_url."admin/public/uploads/product/images/".$image['attachment_name']; ?>" alt="<?php echo $c['attachment_detail']; ?>">
                  </div>
                <?php
                    }
                  }
                }
                ?>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionProducts">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader text-center">
            <h1 class="titleText"><?php echo $category['content_subject']; ?></h1>
          </div>
          <div class="row load_more">
            <?php 
            if(!empty($products)) {
            foreach ($products as $p => $product) {

                $content_subject    = $product['content_subject'];
                $content_short_subject    = ( $product['content_short_subject'] ? '<h4 class="titleText">'.$product['content_short_subject'].'</h4>' : '' );
                $content_detail     = ( $product['content_short_detail'] ? '<p class="desc">'.substr_utf8(strip_tags($product['content_short_detail'], ''),0,160).'</p>' : '' );
                $content_url        = ( $product['content_seo'] ? site_url($product['content_seo']) : 'javascript:;' );
            ?>
            <div class="col-sm-4">
              <div class="thumbnailProduct">
                <div class="imageWrap">
                  <a href="<?php echo $content_url; ?>">
                    <?php
                    if($product['content_thumbnail']) {

                      $content_thumbnail = $base_url."admin/public/uploads/product/images/".$product['content_thumbnail']['attachment_name'];
                    ?>
                      <figure class="image" data-crop="1by1"><img src="<?php echo $content_thumbnail; ?>" alt="<?php echo $product['content_thumbnail']['attachment_detail']; ?>"></figure>
                    <?php
                    }
                    ?>
                  </a>
                </div>
                <div class="captionWrap">
                  <div class="caption">
                    <div class="eqh">
                      <h3 class="titleText"><?php echo $content_subject; ?></h3>
                      <?php echo $content_short_subject; ?>
                      <p class="desc">&nbsp;</p>
                    </div>
                    <div class="button"><a href="<?php echo $content_url; ?>" class="btn btnTheme"><span>More <span class="ftlo-right"></span></span></a></div>
                  </div>
                </div>
              </div>
            </div>

            <?php } } ?>
          </div>
        </div>
      </div>
    </div>

    <?php if($products_all>9) { ?>
      <div class="sgh_section sgh_loadmore">
        <div class="sgh_sectionInner">
          <a href="javascript:;" class="btn btnTheme isHighlight isDown loadMore"><span>Load More <span class="ftlo-down"></span></span></a><div class="container">
        </div>
        </div>
      </div>
    <?php } ?>

  </div>
  <?php echo $footer; ?>




  <?php echo $inc_globalJS; ?>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('productsPage');
    // $(document).ready(function () {
    //   tweenSectionHeader('.sgh_sectionProducts');
    //   $('.sgh_sectionProducts .sgh_sectionInner > .row > *').each(function (index, element) {
    //     tweenProducts('.' + element.className + ':nth-child(' + (index + 1) + ')', index)
    //   });
    // });

    // function tweenProducts (item, delay) {
    //   var cols = 3
    //   var tl = new TimelineLite();
    //   tl.from(item + ' .thumbnailProduct ', 0.4, {y:-100, opacity:0, scale: 1.05, delay: (delay % cols) * 0.4})
    //   .from(item + ' .thumbnailProduct .imageWrap .image', 0.8, {y:-100, opacity:0, scale: 1.05})
    //   .from(item + ' .thumbnailProduct .caption .titleText', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6')
    //   .from(item + ' .thumbnailProduct .caption .desc', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6')
    //   .from(item + ' .thumbnailProduct .caption .button', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6');
    //   var scene = new ScrollMagic.Scene({
    //     triggerElement: item,
    //     triggerHook: 0.8
    //   })
    //   .setTween(tl)
    //   // .addIndicators()
    //   .addTo(ScrollMagicController); 
    //   return scene;     
    // }

    $(function() {

      var page = 1;

      $('.loadMore').click(function() {

          $.ajax({
            url: "<?php echo base_url('products/load_more'); ?>",
            method: "POST",
            data: { 
              main_id : <?php echo $main_id; ?>,
              lang_id: '<?php echo $lang_id; ?>',
              start: page,
              '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            },
            success: function(data) {

              page++;
              var result = jQuery.parseJSON(data);
              if(!result['status']) {

                $('.sgh_loadmore').remove();
              }

              $('.load_more').append(result['html']);
              thumbnailImage();
            }
          });
      });
    });
  </script>
</body>

</html>
