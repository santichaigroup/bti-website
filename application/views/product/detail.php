<!DOCTYPE html>
<html>
<head>
  <?php echo $header; ?>
  <style type="text/css">
    .img-fullwidth {
      width: 100%;
    }
  </style>
</head>
<body>
  <?php echo $inc_header; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight isSM">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php for ($i=0; $i < 1 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
                <?php
                if(!empty($category['images'])) {
                  foreach ($category['images'] as $c => $image) {

                    if($image['type']=="pc") {
                ?>
                    <style>
                      @media (min-width: 768px) {
                        .sgh_item:nth-child(<?php echo($i) + 1; ?>) .thumbnailHighlight .image {
                          background-image: url(<?php echo $base_url."admin/public/uploads/product/images/".$image['attachment_name']; ?>);
                        }
                      }
                    </style>
                <?php
                    } else if($image['type']=="mb") {
                ?>
                  <div class="image">
                    <img class="visible-xs" src="<?php echo $base_url."admin/public/uploads/product/images/".$image['attachment_name']; ?>" alt="<?php echo $c['attachment_detail']; ?>">
                  </div>
                <?php
                    }
                  }
                }
                ?>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
    <div class="sgh_sectionProductDetailWrap">
      <div class="sgh_section sgh_sectionProductDetailFeature">
        <div class="container">
          <div class="sgh_sectionInner">
            <div class="button">
              <a href="<?php echo ( $category['content_seo'] ? site_url($category['content_seo']) : 'javascript:;' ); ?>" class="btnBack"><span class="ftlo-left-open"></span> <span>BACK TO PRODUCT</span></a>
            </div>
          </div>
        </div>
      </div>
      <div class="sgh_section sgh_sectionProductDetailIntro">
        <div class="container">
          <div class="sgh_sectionInner">
            <div class="sgh_sectionHeader">
              <h1 class="titleText"><?php echo $product['content_subject']; ?></h1>
              <div class="titleText_sub"><?php echo $product['content_short_subject']; ?></div>
              <div class="desc">
                <p><?php echo $product['content_short_detail']; ?></p>
              </div>
            </div>
            <div class="sgh_shortcutLinks">
              <ul class="list-inline">
                <?php
                if($product['content_youtube']) {
                ?>
                  <li>
                    <a href="<?php echo $product['content_youtube']; ?>" data-fancybox>
                      <span class="ftlo-play-circled2"></span><span><?php echo $product['content_subject']; ?> VIDEO</span>
                    </a>
                  </li>
                <?php
                }
                ?>
                <?php
                if(!empty($product['files'])) {

                  $attachment_name = $base_url."admin/public/uploads/product/files/".$product['files']['attachment_name'];
                ?>
                  <li><a href="<?php echo $attachment_name; ?>"><span class="ftlo-doc-text"></span><span>BROCHURE</span></a></li>
                <?php
                }
                ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <?php if(!empty($product['images'])) { ?>
      <div class="sgh_section sgh_sectionProductDetailHighlight">
        <div class="container">
          <div class="sgh_sectionInner">
            <div class="sgh_items">
              <?php
                foreach ($product['images'] as $i => $image) {

                  if($product['content_thumbnail']['attachment_id']!=$image['attachment_id']) {

                    $attachment_name = $base_url."admin/public/uploads/product/images/".$image['attachment_name'];
                    $attachment_detail = $image['attachment_detail'];
              ?>
                  <div class="sgh_item">
                    <div class="thumbnailProductHighlight hideCaption">
                      <div class="imageWrap">
                        <figure class="image"><img src="<?php echo $attachment_name; ?>" alt="<?php echo $attachment_detail; ?>"></figure>
                      </div>
                      <div class="captionWrap">
                        <div class="caption">
                          <ul class="list-unstyled">
                            <li class="isTitleText"><b>CAN</b></li>
                            <li>
                              <b>320ml.</b> <br>
                              <span>EXPORT SIZE <b>330 ml.</b></span>
                            </li>
                            <li>
                              <b>PRODUCT DIMENSIONS</b> <br>
                              <span>W 6.6 CM.</span> <br>
                              <span>H 11.52 CM.</span>
                            </li>
                            <li>
                              <b>CALORIE</b> <br>
                              <span>119 kcal.</span>
                            </li>
                            <li>
                              <b>5% ALCOHOL</b> BY VOLUME
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
              <?php
                  }
                }
              ?>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
      <div class="sgh_section sgh_sectionProductDetail">
        <div class="container">
          <div class="sgh_sectionInner sgh_editor">
            <span><?php echo $product['content_detail']; ?></span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php echo $footer; ?>




  <?php echo $inc_globalJS; ?>
  <!-- fancybox -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('productDetailPage');

    /* Remove "style" on img tags - Start */
    // $('.sgh_editor p>img, .sgh_editor div>img, .sgh_editor li>img, .sgh_editor span>img').removeAttr('style').addClass('img-fullwidth');
    // $('.sgh_editor').find('.img-fullwidth').parents('p').addClass('text-center');
    // $('.sgh_editor p, .sgh_editor div, .sgh_editor li, .sgh_editor span,').removeAttr('style');
    /* Remove "style" on img tags - End */
  </script>
</body>

</html>
