<!DOCTYPE html>
<html>
<head>
  <?php echo $header; ?>
  <style type="text/css">
    .alert.alert-error {
        border-color: #d73925;
        background-color: #dd4b39 !important;
        color: #fff;
    }
  </style>
</head>
<body>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionAgeVerification">
      <div class="sgh_sectionInner">
        <div class="ageVerificationForm">
          <div class="ageVerificationBrand"><figure><img src="./img/logo-singha.png" alt=""></figure></div>
          <div class="ageVerificationTitle">PLEASE FILL IN YOUR YEAR OF BIRTH</div>

          <?php if(@$validation_errors!=NULL){ ?>
          <div class="form-group">
              <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <?php echo @$validation_errors; ?>
              </div>
          </div>
          <?php }?>

          <div class="formTheme">
            <?php echo form_open(base_url('age-verification?action_url='.@$this->input->get('action_url')), array( 'name' => 'verification', 'method' => 'post' ));?>
              <div class="form-group isCountry">
                <select class="selectpicker form-control" name="country">
                  <?php
                  if(!empty($countries)) {
                    foreach ($countries as $c => $country) {
                  ?>
                    <option value="<?php echo $country['countries_id']; ?>" <?php echo set_select('country', $country['countries_id'], false); ?>><?php echo $country['countries_name_en']; ?></option>
                  <?php
                    }
                  }
                  ?>
                </select>              
              </div>  
              <div class="form-group isBirthYear">
                <select class="selectpicker form-control" name="year">
                  <?php
                  for($i=$years_now; $i>=$years_check; $i--) {
                  ?>
                    <option value="<?php echo $i; ?>" <?php echo set_select('year', $i, false); ?>><?php echo $i; ?></option>
                  <?php
                  }
                  ?>
                </select>              
              </div>
              <div class="form-group isButton">
                <button class="btn btn-block btnTheme isThemeDiff1"><span>ENTER <span class="ftlo-right"></span></span></button>  
              </div>  
            <?php echo form_close();?>
          </div>
          <div class="ageVerificationDesc">by choosing “enter” you agree to our <br class="visible-xs"> <a href="">terms &amp; conditions</a>  /  <a href="">privacy policy</a></div>
        </div>
      </div>
    </div>
  </div>


  <?php echo $inc_globalJS; ?>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('ageVerificationPage');
  </script>
</body>

</html>