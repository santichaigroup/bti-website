<!DOCTYPE html>
<html>
<head>
  <?php echo $header; ?>
</head>
<body>
  <?php echo $inc_header; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">

          <?php if(!empty($highlight)) { ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
              <?php
              if(is_array($highlight['images'])) {

                foreach ($highlight['images'] as $k => $v) {
                  
                  if($v['type']=="pc") {
              ?>
                  <style>
                    @media (min-width: 768px) {
                      .sgh_item:nth-child(<?php echo($k) + 1; ?>) .thumbnailHighlight .image {
                        background-image: url(<?php echo $base_url."admin/public/uploads/highlight/images/".$v['attachment_name']; ?>);
                      }
                    }
                  </style>
              <?php
                  }

                  if($v['type']=="mb") {
              ?>
                  <div class="image">
                    <img class="visible-xs" src="<?php echo $base_url."admin/public/uploads/highlight/images/".$v['attachment_name']; ?>" alt="<?php echo $v['attachment_detail']; ?>">
                  </div>
              <?php
                  }
                }
              }
              ?>
              </div>
            </div>
          </div>
          <?php } ?>
          
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionRepresentative">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader text-center">
            <h1 class="titleText">REPRESENTATIVES</h1>
          </div>
          <?php
          if(!empty($abouts)) {
          ?>
          <!-- Dropdown Nav tabs -->
          <div class="dropdown isNavTab visible-xs">

            <?php
            foreach ($abouts as $a => $about) {

              $content_subject = ( $about['content_subject'] ? $about['content_subject'] : '' );

              if(strtolower($content_subject)=="all") {
            ?>
                <button class="btn btn-default btn-block dropdown-toggle" type="button" id="dropdownNavTabs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  <?php
                  if(!empty($about['map'])) {
                  ?>
                    <figure class="image">
                    <?php
                      foreach ($about['map'] as $m => $map) {

                        if($m==0) {

                          $attachment_name  = $base_url."admin/public/uploads/business/images/".$map['attachment_name'];
                          $attachment_alt   = $map['attachment_detail'];
                    ?>
                          <img src="<?php echo $attachment_name; ?>" alt="<?php echo $attachment_alt; ?>">
                    <?php
                        }
                      }
                    ?>
                    </figure>
                  <?php
                  }
                  ?>
                  <span class="dropdown-toggle-text"><?php echo $content_subject; ?></span>
                  <span class="caret"></span>
                </button>
            <?php
              }
            }
            ?>

            <ul class="dropdown-menu" aria-labelledby="dropdownNavTabs">
              <?php
              foreach ($abouts as $a => $about) {

                $content_subject = ( $about['content_subject'] ? $about['content_subject'] : '' );

                if(strtolower($content_subject)=="all") {
                  $active_nav = 'style="display: none;"';
                } else {
                  $active_nav = '';
                }
              ?>
                <li role="presentation" <?php echo $active_nav; ?>>
                  <a href="#tab-<?php echo $a; ?>" aria-controls="tab-northAmerica" role="tab" data-toggle="tab" class="isNavTab">
                    <span class="titleText"><?php echo $content_subject; ?></span>
                    <?php
                    if(!empty($about['map'])) {
                    ?>
                      <figure class="image">
                      <?php
                        foreach ($about['map'] as $m => $map) {

                          if($m==0) {

                            $attachment_name  = $base_url."admin/public/uploads/business/images/".$map['attachment_name'];
                            $attachment_alt   = $map['attachment_detail'];
                      ?>
                            <img src="<?php echo $attachment_name; ?>" alt="<?php echo $attachment_alt; ?>">
                      <?php
                          }
                        }
                      ?>
                      </figure>
                    <?php
                    }
                    ?>
                  </a>
                </li>
              <?php
              }
              ?>
            </ul>
          </div>


          <!-- Nav tabs -->
          <ul class="nav nav-tabs hidden-xs" role="tablist">
            <?php
            foreach ($abouts as $a => $about) {

              $content_subject = ( $about['content_subject'] ? $about['content_subject'] : '' );

              if(strtolower($content_subject)=="all") {
                $active_tab = 'class="active"';
              } else {
                $active_tab = '';
              }
            ?>
              <li role="presentation" <?php echo $active_tab; ?>>
                <a href="#tab-<?php echo $a; ?>" aria-controls="tab-northAmerica" role="tab" data-toggle="tab">
                  <span class="titleText"><?php echo $content_subject; ?></span>
                  <?php
                  if(!empty($about['map'])) {
                  ?>
                    <figure class="image">
                    <?php
                      foreach ($about['map'] as $m => $map) {

                        $attachment_name  = $base_url."admin/public/uploads/business/images/".$map['attachment_name'];
                        $attachment_alt   = $map['attachment_detail'];
                    ?>
                      <img src="<?php echo $attachment_name; ?>" alt="<?php echo $attachment_alt; ?>">
                    <?php
                      }
                    ?>
                    </figure>
                  <?php
                  }
                  ?>
                </a>
              </li>
            <?php
            }
            ?>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <?php
            foreach ($abouts as $a => $about) {

              $content_subject = ( $about['content_subject'] ? $about['content_subject'] : '' );

              if(strtolower($content_subject)=="all") {
                $active_panes = ' active in';
              } else {
                $active_panes = '';
              }
            ?>

              <div role="tabpanel" class="tab-pane fade <?php echo $active_panes; ?>" id="tab-<?php echo $a; ?>">
                <?php
                if(empty($about['countries'])) {
                ?>
                  <h2><?php echo $content_subject; ?></h2>
                <?php
                } else {
                ?>
                  <div class="row">
                    <?php 
                    foreach ($about['countries'] as $c => $country) :

                      $country_name   = $country['content_subject'];
                      $country_detail = $country['content_detail'];
                      $country_url    = ( $country['content_url'] ? $country['content_url'] : 'javascript:;' );
                    ?>
                      <div class="col-md-3 col-sm-4 col-xs-6">
                        <h2 class="media-title"><?php echo $country_name; ?></h2>

                        <div class="media">
                          <div class="media-left">
                            <div class="media-left-inner">
                              <?php
                              if(!empty($country['images'])) {
                                foreach ($country['images'] as $i => $image) {

                                  $country_image  = $base_url."admin/public/uploads/business/images/".$image['country_name'];
                                  $country_alt    = $image['country_detail'];
                                  if($i==0) {  
                              ?>
                                    <a href="<?php echo $country_url; ?>">
                                      <img class="media-object" src="<?php echo $country_image; ?>" alt="<?php echo $country_alt; ?>">
                                    </a>
                              <?php
                                  } else {
                              ?>
                                    <div class="media-left-brand">
                                      <img src="<?php echo $country_image; ?>" alt="<?php echo $country_alt; ?>">
                                    </div>
                              <?php
                                  }
                                }
                              }
                              ?>
                            </div>
                          </div>

                          <div class="media-body">
                            <?php echo $country_detail; ?>
                          </div>
                        </div>

                      </div>
                    <?php endforeach; ?>
                  </div>
                <?php
                }
                ?>
              </div>
            <?php } ?>
          </div>
          <?php
          }
          ?>

        </div>
      </div>
    </div>
  </div>
  <?php echo $footer; ?>




  <?php echo $inc_globalJS; ?>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('representativePage');
    $('.sgh_sectionRepresentative .media').matchHeight();
  </script>
</body>

</html>
