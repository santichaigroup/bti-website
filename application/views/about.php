<!DOCTYPE html>
<html>
<head>
  <?php echo $header; ?>
</head>
<body>
  <?php echo $inc_header; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">

          <?php if(!empty($highlight)) { ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
              <?php
              if(is_array($highlight['images'])) {

                foreach ($highlight['images'] as $k => $v) {
                  
                  if($v['type']=="pc") {
              ?>
                  <style>
                    @media (min-width: 768px) {
                      .sgh_item:nth-child(<?php echo($k) + 1; ?>) .thumbnailHighlight .image {
                        background-image: url(<?php echo $base_url."admin/public/uploads/highlight/images/".$v['attachment_name']; ?>);
                      }
                    }
                  </style>
              <?php
                  }

                  if($v['type']=="mb") {
              ?>
                  <div class="image">
                    <img class="visible-xs" src="<?php echo $base_url."admin/public/uploads/highlight/images/".$v['attachment_name']; ?>" alt="<?php echo $v['attachment_detail']; ?>">
                  </div>
              <?php
                  }
                }
              }
              ?>
              </div>
            </div>
          </div>
          <?php } ?>

        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionArticle">
      <div class="container">
        <div class="sgh_sectionInner">
          <article>
            <div class="sgh_sectionHeader text-center">
              <h1 class="titleText"><?php echo $about['content_subject']; ?></h1>
            </div>
            <?php
            if(!empty($about['images'])) {
              foreach ($about['images'] as $a => $value) {

                $attachment_description = $value['attachment_description'];
                $attachment_name = $base_url."admin/public/uploads/business/images/".$value['attachment_name'];

                if($a % 2 === 1) {
            ?>
                  <div class="row">
                    <div class="col-sm-6">
                      <span>
                        <?php echo $attachment_description; ?>
                      </span>
                    </div>
                    <div class="col-sm-6">
                      <figure class="image"><img src="<?php echo $attachment_name; ?>" alt=""></figure>
                    </div>
                  </div>
            <?php
                } else {
            ?>
                  <div class="row">
                    <div class="col-sm-6">
                      <figure class="image"><img src="<?php echo $attachment_name; ?>" alt=""></figure>
                    </div>
                    <div class="col-sm-5">
                      <?php echo $attachment_description; ?>
                    </div>
                  </div>
            <?php
                }
              }
            }
            ?>
          </article>
        </div>
      </div>
    </div>
  </div>
  <?php echo $footer; ?>




  <?php echo $inc_globalJS; ?>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('aboutPage');
  </script>
</body>

</html>
