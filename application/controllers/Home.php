<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	var $_data = array();
	public function __construct()
	{
		parent::__construct();

		/****************************** Check Session *****************************/
			// $this->verification_library->check_session();

		/****************************** URL *****************************/
			$this->path 					= $this->uri->ruri_string();
			$this->_data['url_path'] 		= $this->uri->segment(1, 'home');
			$this->_data['url_path_sub']	= false;

			$this->language 		= $this->settings_model->getLanguageList();
			$this->lang_id 			= (!empty($rsegment = $this->uri->rsegment_array()) ? end($rsegment) : '');
			$this->_data['lang_id'] = ( is_int(array_search($this->lang_id, array_column($this->language, 'lang_id'))) ? $this->lang_id : 'TH' );

		/****************************** SEO Setting *****************************/
			$global 	= $this->settings_model->getAnalytic('Global');
			$analytic 	= $this->settings_model->getAnalytic($this->_data['url_path']);
			if(!empty($analytic)) {

				$this->_data['title']			= text_lang('home_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('home_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('home_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= ( $analytic['seo_tracking_code'] ? $analytic['seo_tracking_code'] : $global['seo_tracking_code']);
				$this->_data['header_code'] 	= ( $analytic['seo_code'] ? $analytic['seo_code'] : $global['seo_code'] );
			} else {

				$this->_data['title']			= text_lang('global_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('global_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('global_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= $global['seo_tracking_code'];
				$this->_data['header_code'] 	= $global['seo_code'];
			}

		/****************************** Footer & Contact Setting *****************************/
			$contact = $this->settings_model->address($this->_data['lang_id'])->row_array();
			if(!empty($contact)) {

				$this->_data['address']	= $contact;
			} else {
				$this->_data['address'] = "";
			}

		/****************************** Sub Menu Product *****************************/
			$search = [ 'content_status' => 'active' ];
			$this->_data['product_category'] = $this->product_model->dataTableCategory($this->_data['lang_id'], 100, 0, '', $search)
																	->result_array();
	}

	public function index()
	{
		$data_fillter 		= [
								'content_startdate <='=>date('Y-m-d h:m:i'), 
								'content_enddate >='=>date('Y-m-d h:m:i')
							];
		$banner 			= $this->settings_model->banner($this->_data['lang_id'], $data_fillter)->result_array();
		$banner_attachment 	= $this->settings_model->banner_attachment()->result_array();
			foreach ($banner as $k => $v) {

				$this->_data['banners'][$k] = $v;

				foreach ($banner_attachment as $key => $value) {
				
					if($v['main_id']==$value['default_main_id']) {

						$this->_data['banners'][$k]['images'][] = $value;
					}
				}
			}
		$search 			= [ 'content_status' => 'active' ];
		$category 			= $this->product_model->dataTableCategory($this->_data['lang_id'], 99, 0, '', $search)->result_array();
			foreach ($category as $c => $value) {
				
				$this->_data['categories'][$c] = $value;
				$this->_data['categories'][$c]['images'] = $this->product_model->getDetailCategory_img($value['main_id'])
																						->result_array();
			}

		$news 				= $this->news_model->dataTable($this->_data['lang_id'], 8, 0, '', $search)->result_array();
			foreach ($news as $n => $new) {
				
				$this->_data['news'][$n] = $new;
				if($new['content_thumbnail']) {
					$this->_data['news'][$n]['content_thumbnail'] = $this->news_model->getThumbnail_img($new['content_thumbnail'])->row_array();
				}
			}

		$this->core_library->view("index", $this->_data);
		$this->core_library->output();
	}
}
