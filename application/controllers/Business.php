<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {

	var $_data = array();
	public function __construct()
	{
		parent::__construct();

		/****************************** Check Session *****************************/
			// $this->verification_library->check_session();

		/****************************** URL *****************************/
			$this->path 					= $this->uri->ruri_string();
			$this->_data['url_path'] 		= $this->uri->segment(1);
			$this->_data['url_path_sub']	= false;

			$this->language 		= $this->settings_model->getLanguageList();
			$this->lang_id 			= (!empty($rsegment = $this->uri->rsegment_array()) ? end($rsegment) : '');
			$this->_data['lang_id'] = ( is_int(array_search($this->lang_id, array_column($this->language, 'lang_id'))) ? $this->lang_id : 'TH' );

		/****************************** SEO Setting *****************************/
			$global 	= $this->settings_model->getAnalytic('Global');
			$analytic 	= $this->settings_model->getAnalytic($this->_data['url_path']);
			if(!empty($analytic)) {

				$this->_data['title']			= text_lang('business_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('business_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('business_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= ( $analytic['seo_tracking_code'] ? $analytic['seo_tracking_code'] : $global['seo_tracking_code']);
				$this->_data['header_code'] 	= ( $analytic['seo_code'] ? $analytic['seo_code'] : $global['seo_code'] );
			} else {

				$this->_data['title']			= text_lang('global_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('global_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('global_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= $global['seo_tracking_code'];
				$this->_data['header_code'] 	= $global['seo_code'];
			}

		/****************************** Footer & Contact Setting *****************************/
			$contact = $this->settings_model->address($this->_data['lang_id'])->row_array();
			if(!empty($contact)) {

				$this->_data['address']	= $contact;
			} else {
				$this->_data['address'] = "";
			}

		/****************************** Sub Menu Product *****************************/
			$search = [ 'content_status' => 'active' ];
			$this->_data['product_category'] = $this->product_model->dataTableCategory($this->_data['lang_id'], 100, 0, '', $search)
																	->result_array();
	}

	public function about()
	{
		$this->_data['url_path_sub']	= 'business/about';

		$about 				= $this->business_model->dataTable(1, $this->_data['lang_id'], 1)->row_array();
		$about['images']	= $this->business_model->getDetail_img($about['main_id'], 0)->result_array();

		$this->_data['about'] = $about;

		// Highlight Banner
		$highlight 				= $this->highlight_model->dataTable($this->_data['lang_id'], 1, 0, '', ['content_subject'=>$this->_data['url_path_sub']])
															->row_array();
		$highlight['images'] 	= $this->highlight_model->getDetail_img($highlight['main_id'])
															->result_array();

		$this->_data['highlight'] = $highlight;

		$this->core_library->view("about", $this->_data);
		$this->core_library->output();
	}

	public function representative()
	{
		$this->_data['url_path_sub']	= 'business/representative';

		$is_order = [
						'sequence' => 'asc'
					];
		$about = $this->business_model->dataTable(2, $this->_data['lang_id'], 100, '', $is_order)->result_array();
			foreach ($about as $a => $value) {

				$this->_data['abouts'][$a] = $value;
				$this->_data['abouts'][$a]['map'] = $this->business_model->getDetail_img($value['main_id'], 1)->result_array();

				if(strtolower($value['content_subject'])=="all") {
					$countries = $this->business_model->dataTableCountry(null, $this->_data['lang_id'], 999, 0, ['content_subject' => 'asc', 'sequence' => 'asc'])
													->result_array();
				} else {
					$countries = $this->business_model->dataTableCountry($value['main_id'], $this->_data['lang_id'], 100, 0, ['content_subject' => 'asc', 'sequence' => 'asc'])
													->result_array();
				}

				if(!empty($countries)) {

					foreach ($countries as $c => $country) {
						
						$this->_data['abouts'][$a]['countries'][$c] = $country;
						$this->_data['abouts'][$a]['countries'][$c]['images'] = $this->business_model->getDetailCountry_img($country['content_id'])->result_array();
					}
				} else {

					$this->_data['abouts'][$a]['countries'] = null;
				}
			}

		// Highlight Banner
		$highlight 				= $this->highlight_model->dataTable($this->_data['lang_id'], 1, 0, '', ['content_subject'=>$this->_data['url_path_sub']])
															->row_array();
		$highlight['images'] 	= $this->highlight_model->getDetail_img($highlight['main_id'])
															->result_array();

		$this->_data['highlight'] = $highlight;

		$this->core_library->view("representative", $this->_data);
		$this->core_library->output();
	}
}
