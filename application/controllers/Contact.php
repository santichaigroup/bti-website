<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	var $_data = array();
	public function __construct()
	{
		parent::__construct();

		/****************************** Check Session *****************************/
			// $this->verification_library->check_session();

		/****************************** URL *****************************/
			$this->path 					= $this->uri->ruri_string();
			$this->_data['url_path'] 		= $this->uri->segment(1, 'home');
			$this->_data['url_path_sub']	= false;

			$this->language 		= $this->settings_model->getLanguageList();
			$this->lang_id 			= (!empty($rsegment = $this->uri->rsegment_array()) ? end($rsegment) : '');
			$this->_data['lang_id'] = ( is_int(array_search($this->lang_id, array_column($this->language, 'lang_id'))) ? $this->lang_id : 'TH' );

		/****************************** SEO Setting *****************************/
			$global 	= $this->settings_model->getAnalytic('Global');
			$analytic 	= $this->settings_model->getAnalytic($this->_data['url_path']);
			if(!empty($analytic)) {

				$this->_data['title']			= text_lang('contact_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('contact_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('contact_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= ( $analytic['seo_tracking_code'] ? $analytic['seo_tracking_code'] : $global['seo_tracking_code']);
				$this->_data['header_code'] 	= ( $analytic['seo_code'] ? $analytic['seo_code'] : $global['seo_code'] );
			} else {

				$this->_data['title']			= text_lang('global_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('global_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('global_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= $global['seo_tracking_code'];
				$this->_data['header_code'] 	= $global['seo_code'];
			}

		/****************************** Footer & Contact Setting *****************************/
			$contact = $this->settings_model->address($this->_data['lang_id'])->row_array();
			if(!empty($contact)) {

				$this->_data['address']	= $contact;
			} else {
				$this->_data['address'] = "";
			}

		/****************************** Sub Menu Product *****************************/
			$search = [ 'content_status' => 'active' ];
			$this->_data['product_category'] = $this->product_model->dataTableCategory($this->_data['lang_id'], 100, 0, '', $search)
																	->result_array();
	}

	public function index()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules(
										"email_type","PRODUCT","trim|required|integer", 
										array(
											'required' => 'กรุณากรอกข้อมูล PRODUCT',
											'integer' => 'กรุณากรอกข้อมูล PRODUCT เฉพาะตัวเลข'
										)
									);
		$this->form_validation->set_rules(
										"country","COUNTRY","trim|required",
										array(
											'required' => 'กรุณากรอกข้อมูล COUNTRY'
										)
									);
		$this->form_validation->set_rules(
										"name","NAME","trim|required",
										array(
											'required' => 'กรุณากรอกข้อมูล NAME'
										)
									);
		$this->form_validation->set_rules(
										"email","EMAIL","trim|required|valid_emails",
										array(
											'required' => 'กรุณากรอกข้อมูล EMAIL',
											'valid_emails' => 'กรุณากรอกข้อมูล EMAIL ให้ถูกต้อง'
										)
									);
		$this->form_validation->set_rules(
										"tel","TEL","trim|required",
										array(
											'required' => 'กรุณากรอกข้อมูล TEL'
										)
									);
		$this->form_validation->set_rules(
										"message","MESSAGE","trim|required",
										array(
											'required' => 'กรุณากรอกข้อมูล MESSAGE'
										)
									);
		$this->form_validation->set_rules(
										'g-recaptcha-response', 'recaptcha validation', 'required|callback_validate_captcha',
										array(
											'required' => 'กรุณายืนยันตัวตน',
											'validate_captcha' => 'Please check the the captcha form'
										)
									);

		if($this->form_validation->run()===false) {

			$this->_data['categories'] 	= $this->setting_email_model->dataTableCategory($this->_data['lang_id'], true, ['content_subject'=>'asc'])->result_array();
			$this->_data['validation_errors'] = validation_errors();

			$this->core_library->view("contact", $this->_data);
			$this->core_library->output();
		} else {

			$cate_id 	= $this->input->post('email_type');
			$country_id	= $this->input->post('country');
			$name 		= $this->input->post('name');
			$emails 	= $this->input->post('email');
			$tel 		= $this->input->post('tel');
			$message 	= $this->input->post('message');

			$email = $this->setting_email_model->getDetail('',$this->_data['lang_id'],$cate_id,$country_id);
			$email_content_subject 	= $email['content_subject'];
			$email_content_email 	= $email['content_email'];
			$email_content_email_cc = $email['content_email_cc'];

			$this->db->set('cate_id', $cate_id);
			$this->db->set('country_id', $country_id);
			$this->db->set('contactus_fullname', $name);
			$this->db->set('contactus_email', $emails);
			$this->db->set('contactus_phone', $tel);
			$this->db->set('contactus_detail', $message);
			$this->db->set('create_date',"NOW()",false);
			$this->db->insert('address_contactus');

			// $this->email->initialize($this->config_mail);
			// $this->load->library('email');
			// $this->email->from($email, $name);
			// $this->email->to($email_content_email);
			// $this->email->cc($email_content_email_cc);
			// // $this->email->bcc('them@their-example.com');

			// $this->email->subject($subject);
			// $this->email->message($message);

			// $this->email->send();

			redirect('contact');
		}
	}

	public function load_ajax()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules("lang_id","Lang ID","trim|required");
		$this->form_validation->set_rules("email_type","Email Type","trim|required");

		if($this->form_validation->run()===false) {

			echo json_encode([]);
		} else {

			$countries = $this->setting_email_model->dataTableCategoryCountry($this->input->post('email_type'), 
																	$this->input->post('lang_id'))->result_array();

			$status = false;
			$html 	= "";
			if(count($countries)>0) {

				$status = true;
				$html 	= $countries;
			}

			$output = array(
							"status"			=> $status,
							"html"            	=> $html
						);

			echo json_encode($output);
		}
	}

	function validate_captcha() {
        $captcha = $this->input->post('g-recaptcha-response');
         $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Ldb8WkUAAAAACrkNiZpujUWcBD0ayUMBrIFmMef&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        if ($response . 'success' == false) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
