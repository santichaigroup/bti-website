<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	var $_data = array();
	public function __construct()
	{
		parent::__construct();

		/****************************** Check Session *****************************/
			// $this->verification_library->check_session();

		/****************************** URL *****************************/
			$this->path 					= $this->uri->ruri_string();
			$this->_data['url_path'] 		= $this->uri->segment(1);
			$this->_data['url_path_sub']	= false;

			$this->language 		= $this->settings_model->getLanguageList();
			$this->lang_id 			= (!empty($rsegment = $this->uri->rsegment_array()) ? end($rsegment) : '');
			$this->_data['lang_id'] = ( is_int(array_search($this->lang_id, array_column($this->language, 'lang_id'))) ? $this->lang_id : 'TH' );

		/****************************** SEO Setting *****************************/
			$global 	= $this->settings_model->getAnalytic('Global');
			$analytic 	= $this->settings_model->getAnalytic($this->_data['url_path']);
			if(!empty($analytic)) {

				$this->_data['title']			= text_lang('products_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('products_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('products_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= ( $analytic['seo_tracking_code'] ? $analytic['seo_tracking_code'] : $global['seo_tracking_code']);
				$this->_data['header_code'] 	= ( $analytic['seo_code'] ? $analytic['seo_code'] : $global['seo_code'] );
			} else {

				$this->_data['title']			= text_lang('global_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('global_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('global_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= $global['seo_tracking_code'];
				$this->_data['header_code'] 	= $global['seo_code'];
			}

		/****************************** Footer & Contact Setting *****************************/
			$contact = $this->settings_model->address($this->_data['lang_id'])->row_array();
			if(!empty($contact)) {

				$this->_data['address']	= $contact;
			} else {
				$this->_data['address'] = "";
			}

		/****************************** Sub Menu Product *****************************/
			$search = [ 'content_status' => 'active' ];
			$this->_data['product_category'] = $this->product_model->dataTableCategory($this->_data['lang_id'], 100, 0, '', $search)
																	->result_array();
	}

	// public function index()
	// {
	// 	$this->core_library->view("product/list", $this->_data);
	// 	$this->core_library->output();
	// }

	public function product_category($main_id)
	{
		if($main_id==1) {

			$this->verification_library->check_session();
		}

		$category = $this->product_model->getDetailCategory($main_id, $this->_data['lang_id']);
		$category['images'] = $this->product_model->getDetailCategory_img($main_id)->result_array();

		$this->_data['category'] = $category;

			$this->_data['title']			= $category['content_title'];
			$this->_data['description']		= $category['content_description'];
			$this->_data['keywords']		= $category['content_keyword'];

			$this->_data['analytic'] 		= "";
			$this->_data['header_code'] 	= "";

			$this->_data['url_path_sub'] 	= $this->uri->uri_string();

		$search 	= [ 'content_status' => 'active' ];
		$products 	= $this->product_model->dataTable($main_id, $this->_data['lang_id'], 9, 0, '', $search)->result_array();
		foreach ($products as $key => $value) {
			
			$this->_data['products'][$key] = $value;
			if($value['content_thumbnail']) {
				$this->_data['products'][$key]['content_thumbnail'] = $this->product_model->getThumbnail_img($value['content_thumbnail'])->row_array();
			}
		}
		$this->_data['main_id'] 		= $main_id;
		$this->_data['products_all'] 	= $this->product_model->dataTable($main_id, $this->_data['lang_id'], '', '', '', $search);

		$this->core_library->view("product/list", $this->_data);
		$this->core_library->output();
	}

	public function product_detail($main_id)
	{
		$product 						= $this->product_model->getDetail($main_id, $this->_data['lang_id']);
		if($product['cate_id']==1) {

			$this->verification_library->check_session();
		}

		$product['content_thumbnail'] 	= $this->product_model->getThumbnail_img($product['content_thumbnail'])->row_array();
		$product['images']				= $this->product_model->getDetail_img($main_id)->result_array();
		$product['files'] 				= $this->product_model->getDetail_file($main_id)->row_array();

		$category = $this->product_model->getDetailCategory($product['cate_id'], $this->_data['lang_id']);
		$category['images'] = $this->product_model->getDetailCategory_img($product['cate_id'])->result_array();

		$this->_data['title']			= $product['content_title'];
		$this->_data['description']		= $product['content_description'];
		$this->_data['keywords']		= $product['content_keyword'];

		$this->_data['category'] 	= $category;
		$this->_data['product'] 	= $product;

		$this->core_library->view("product/detail", $this->_data);
		$this->core_library->output();
	}

	public function load_ajax()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules("main_id","Main ID","trim|required|integer");
		$this->form_validation->set_rules("lang_id","Lang ID","trim|required");
		$this->form_validation->set_rules("start","Start","trim|required|integer");

		if($this->form_validation->run()===false) {

			echo json_encode([]);
		} else {
		
			$limit 			= 9;
			$start 			= 9*$this->input->post('start');
			$cate_id 		= $this->input->post('main_id');
			$lang_id 		= $this->input->post('lang_id');
			$result_array 	= array();
			$html 			= '';
			$status 		= false;

			$search 		= [ 'content_status' => 'active' ];
			$query_data 	= $this->product_model->dataTable($cate_id, $lang_id, $limit, $start, '', $search)
													->result_array();

			if(!empty($query_data)) {

				foreach ($query_data as $key => $product) {

	                $content_subject    = $product['content_subject'];
	                $content_short_subject    = ( $product['content_short_subject'] ? '<h4 class="titleText">'.$product['content_short_subject'].'</h4>' : '' );
	                $content_detail     = ( $product['content_short_detail'] ? '<p class="desc">'.substr_utf8(strip_tags($product['content_short_detail'], ''),0,160).'</p>' : '' );
	                $content_url        = ( $product['content_seo'] ? site_url($product['content_seo']) : 'javascript:;' );

					if($product['content_thumbnail']) {
						$product['content_thumbnail'] = $this->product_model->getThumbnail_img($product['content_thumbnail'])
																									->row_array();
					}

		            $html .= '<div class="col-sm-4">
		              <div class="thumbnailProduct">
		                <div class="imageWrap">
		                  <a href="'.$content_url.'">';
		                    
		                    if($product['content_thumbnail']) {

		                      $content_thumbnail = base_url("admin/public/uploads/product/images/".$product['content_thumbnail']['attachment_name']);
		                    
		                      $html .= '<figure class="image" data-crop="1by1"><img src="'.$content_thumbnail.'" alt="'.$product['content_thumbnail']['attachment_detail'].'"></figure>';
		                    }
		                    
		                  $html .= '</a>
		                </div>
		                <div class="captionWrap">
		                  <div class="caption">
		                  	<div class="eqh">
			                    <h3 class="titleText">'.$content_subject.'</h3>
			                    '.$content_short_subject.'
			                    <p class="desc">&nbsp;</p>
			                </div>
		                    <div class="button"><a href="'.$content_url.'" class="btn btnTheme"><span>More Detail <span class="ftlo-right"></span></span></a></div>
		                  </div>
		                </div>
		              </div>
		            </div>';
				}

				if(count($query_data)==9) {

					$status = true;
				}
			}

			$output = array(
							"status"			=> $status,
							"html"            	=> $html
						);

			echo json_encode($output);
		}
	}
}
