<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AgeVerification extends CI_Controller {

	var $_data = array();
	public function __construct()
	{
		parent::__construct();

		/****************************** Check Session *****************************/
			$this->verification_library->check_session(false);

		/****************************** URL *****************************/
			$this->path 					= $this->uri->ruri_string();
			$this->_data['url_path'] 		= $this->uri->segment(1, 'home');
			$this->_data['url_path_sub']	= false;

			$this->language 		= $this->settings_model->getLanguageList();
			$this->lang_id 			= (!empty($rsegment = $this->uri->rsegment_array()) ? end($rsegment) : '');
			$this->_data['lang_id'] = ( is_int(array_search($this->lang_id, array_column($this->language, 'lang_id'))) ? $this->lang_id : 'TH' );

		/****************************** SEO Setting *****************************/
			$global 	= $this->settings_model->getAnalytic('Global');
			$analytic 	= $this->settings_model->getAnalytic($this->_data['url_path']);
			if(!empty($analytic)) {

				$this->_data['title']			= text_lang('ageverification_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('ageverification_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('ageverification_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= ( $analytic['seo_tracking_code'] ? $analytic['seo_tracking_code'] : $global['seo_tracking_code']);
				$this->_data['header_code'] 	= ( $analytic['seo_code'] ? $analytic['seo_code'] : $global['seo_code'] );
			} else {

				$this->_data['title']			= text_lang('global_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('global_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('global_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= $global['seo_tracking_code'];
				$this->_data['header_code'] 	= $global['seo_code'];
			}

		/****************************** Footer & Contact Setting *****************************/
			$contact = $this->settings_model->address($this->_data['lang_id'])->row_array();
			if(!empty($contact)) {

				$this->_data['address']	= $contact;
			} else {
				$this->_data['address'] = "";
			}

		/****************************** Sub Menu Product *****************************/
			$search = [ 'content_status' => 'active' ];
			$this->_data['product_category'] = $this->product_model->dataTableCategory($this->_data['lang_id'], 100, 0, '', $search)
																	->result_array();
	}

	public function index()
	{
		$action_url = ( $this->input->get('action_url') ? $this->input->get('action_url') : 'home' );
		$session_id = $this->session->session_id;

		$this->load->library('form_validation');

		$this->form_validation->set_rules(
											"country","Country","trim|required|integer",
											array(
												'required' => 'กรุณากรอกข้อมูล Country',
												'integer' => 'กรุณากรอกข้อมูล Country เฉพาะตัวเลข'
											)
										);
		$this->form_validation->set_rules(
											"year","Year","trim|required|integer|callback_valid_date",
											array(
												'required' => 'กรุณากรอกข้อมูล Year',
												'integer' => 'กรุณากรอกข้อมูล Year เฉพาะตัวเลข'
											)
										);

		if($this->form_validation->run()===false) {

			$this->_data['validation_errors'] = validation_errors();

			$this->_data['countries'] = $this->db->where('countries_status', 'active')
													->order_by('sequence', 'asc')
														->get('system_countries')
															->result_array();

			$now = new DateTime();
			$this->_data['years_now'] = $now->format("Y");
			$this->_data['years_check'] = 1950;

			$this->core_library->view("ageVerification", $this->_data);
			$this->core_library->output();
		} else {

			$this->load->library('user_agent');

			$user_agent = $this->agent->agent_string();
			$country 	= $this->input->post('country');
			$year 		= $this->input->post('year');

			$this->db->set('action_url', $action_url);
			$this->db->set('countries_id', $country);
			$this->db->set('year', $year);
			$this->db->set('user_agent', $user_agent);
			$this->db->where('id', $session_id);
			$this->db->update('ci_sessions_age');

			redirect(site_url($action_url));
		}
	}

	public function valid_date()
    {
		$this->load->library('form_validation');

    	$day 		= "01";
    	$month 		= "01";
        $year 		= (int)$this->input->post('year');
        $birthday 	= mktime(0, 0, 0, $month, $day, $year);

        if (!checkdate($month, $day, $year)) {

            $this->form_validation->set_message('valid_date', 'The %s field is invalid.');
            return FALSE;
         }
         
        if ($birthday > strtotime('-18 years')) {

            $this->form_validation->set_message('valid_date', 'You must be 18 years old to visit website.');
        	return false;
        }
            
        return TRUE; 
    }
}
