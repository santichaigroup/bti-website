<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	var $_data = array();
	public function __construct()
	{
		parent::__construct();

		/****************************** Check Session *****************************/
			// $this->verification_library->check_session();

		/****************************** URL *****************************/
			$this->path 					= $this->uri->ruri_string();
			$this->_data['url_path'] 		= $this->uri->segment(1);
			$this->_data['url_path_sub']	= false;

			$this->language 		= $this->settings_model->getLanguageList();
			$this->lang_id 			= (!empty($rsegment = $this->uri->rsegment_array()) ? end($rsegment) : '');
			$this->_data['lang_id'] = ( is_int(array_search($this->lang_id, array_column($this->language, 'lang_id'))) ? $this->lang_id : 'TH' );

		/****************************** SEO Setting *****************************/
			$global 	= $this->settings_model->getAnalytic('Global');
			$analytic 	= $this->settings_model->getAnalytic($this->_data['url_path']);
			if(!empty($analytic)) {

				$this->_data['title']			= text_lang('news_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('news_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('news_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= ( $analytic['seo_tracking_code'] ? $analytic['seo_tracking_code'] : $global['seo_tracking_code']);
				$this->_data['header_code'] 	= ( $analytic['seo_code'] ? $analytic['seo_code'] : $global['seo_code'] );
			} else {

				$this->_data['title']			= text_lang('global_title', $this->_data['lang_id']);
				$this->_data['description']		= text_lang('global_description', $this->_data['lang_id']);
				$this->_data['keywords']		= text_lang('global_keywords', $this->_data['lang_id']);

				$this->_data['analytic'] 		= $global['seo_tracking_code'];
				$this->_data['header_code'] 	= $global['seo_code'];
			}

		/****************************** Footer & Contact Setting *****************************/
			$contact = $this->settings_model->address($this->_data['lang_id'])->row_array();
			if(!empty($contact)) {

				$this->_data['address']	= $contact;
			} else {
				$this->_data['address'] = "";
			}

		/****************************** Sub Menu Product *****************************/
			$search = [ 'content_status' => 'active' ];
			$this->_data['product_category'] = $this->product_model->dataTableCategory($this->_data['lang_id'], 100, 0, '', $search)
																	->result_array();
	}

	public function index()
	{
		$search 			= [ 'content_status' => 'active' ];
		$news 				= $this->news_model->dataTable($this->_data['lang_id'], 8, 0, '', $search)->result_array();
			foreach ($news as $n => $new) {
				
				$this->_data['news'][$n] = $new;
				if($new['content_thumbnail']) {
					$this->_data['news'][$n]['content_thumbnail'] = $this->news_model->getThumbnail_img($new['content_thumbnail'])->row_array();
				}
			}

		$this->_data['news_all'] 	= $this->news_model->dataTable($this->_data['lang_id'], '', '', '', $search);

		// Highlight Banner
		$highlight 				= $this->highlight_model->dataTable($this->_data['lang_id'], 1, 0, '', ['content_subject'=>$this->_data['url_path']])
															->row_array();
		$highlight['images'] 	= $this->highlight_model->getDetail_img($highlight['main_id'])
															->result_array();

		$this->_data['highlight'] = $highlight;

		$this->core_library->view("news/list", $this->_data);
		$this->core_library->output();
	}

	public function news_detail($main_id)
	{
		$news 						= $this->news_model->getDetail($main_id, $this->_data['lang_id']);
		$news['content_thumbnail'] 	= $this->news_model->getThumbnail_img($news['content_thumbnail'])->row_array();

		$this->_data['title']			= $news['content_title'];
		$this->_data['description']		= $news['content_description'];
		$this->_data['keywords']		= $news['content_keyword'];

		$this->_data['news'] = $news;

		$this->core_library->view("news/detail", $this->_data);
		$this->core_library->output();
	}

	public function load_ajax()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules("lang_id","Lang ID","trim|required");
		$this->form_validation->set_rules("start","Start","trim|required|integer");

		if($this->form_validation->run()===false) {

			echo json_encode([]);
		} else {
		
			$limit 			= 8;
			$start 			= 8*$this->input->post('start');
			$cate_id 		= $this->input->post('main_id');
			$lang_id 		= $this->input->post('lang_id');
			$result_array 	= array();
			$html 			= '';
			$status 		= false;

			$search 		= [ 'content_status' => 'active' ];
			$query_data 	= $this->news_model->dataTable($lang_id, $limit, $start, '', $search)
													->result_array();

			if(!empty($query_data)) {

				foreach ($query_data as $key => $new) {

		              $content_subject    = $new['content_subject'];
		              $content_url        = ( $new['content_seo'] ? site_url($new['content_seo']) : 'javascript:;' );
		              $post_date          = date('d M Y', time($new['post_date']));

					if($new['content_thumbnail']) {
						$new['content_thumbnail'] = $this->news_model->getThumbnail_img($new['content_thumbnail'])
																		->row_array();
					}

		            $html .=  '<div class="col-sm-4 col-md-3">
		                <div class="thumbnailNews">
		                  <div class="imageWrap">
		                    <a href="'.$content_url.'">';

		                      if($new['content_thumbnail']) {

		                        $content_thumbnail = base_url("admin/public/uploads/news/images/".$new['content_thumbnail']['attachment_name']);

		                        $html .= '<figure class="image" data-crop="news"><img src="'.$content_thumbnail.'" alt="'.$new['content_thumbnail']['attachment_detail'].'"></figure>';
		                      }
		                $html .=    '</a>
		                  </div>
		                  <div class="captionWrap">
		                    <div class="caption">
		                    	<div class="eqh">
			                      <div class="date">'.$post_date.'</div>
			                      <h3 class="titleText">'.$content_subject.'</h3>
			                    </div>
		                      <div class="button"><a href="'.$content_url.'"><span><span>MORE DETAIL</span><span class="ftlo-right"></span></span></a></div>
		                    </div>
		                  </div>
		                </div>
		              </div>';
				}

				if(count($query_data)==8) {

					$status = true;
				}
			}

			$output = array(
							"status"			=> $status,
							"html"            	=> $html
						);

			echo json_encode($output);
		}
	}
}
