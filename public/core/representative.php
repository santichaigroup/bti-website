<?php 
  include'_inc-config.php'; 
  $page = 'business';
  $pageSub = './representative.php'
?>
<!DOCTYPE html>
<html>
<head>
  <?php include'_inc-docHead.php'; ?>
</head>
<body>
  <?php include'_inc-header.php'; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php for ($i=0; $i < 1 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
                <style>
                  @media (min-width: 768px) {
                    .sgh_item:nth-child(<?php echo($i) + 1; ?>) .thumbnailHighlight .image {
                      background-image: url(img/highlight-representative.png);
                    }
                  }
                </style>
                <div class="image">
                  <img class="visible-xs" src="img/highlightMobile-representative.png" alt="">
                </div>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionRepresentative">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader text-center">
            <h1 class="titleText">REPRESENTATIVE</h1>
          </div>
          <!-- Dropdown Nav tabs -->
          <div class="dropdown isNavTab visible-xs">
            <button class="btn btn-default btn-block dropdown-toggle" type="button" id="dropdownNavTabs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
              <figure class="image">
                <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/cd1f4f6641bfa8eb529eeda77425be62.png" alt="">
              </figure>
              <span class="dropdown-toggle-text">ASIA</span>
              <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownNavTabs">
              <li role="presentation">
                <a href="#tab-00" aria-controls="tab-northAmerica" role="tab" data-toggle="tab" class="isNavTab">
                  <span class="titleText">ALL</span>
                  <figure class="image">
                    <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/4fec6d2971e80585711d4c2eb06810d1.jpg"
                      alt="">
                  </figure>
                </a>
              </li>
              <li role="presentation">
                <a href="#tab-0" aria-controls="tab-northAmerica" role="tab" data-toggle="tab" class="isNavTab">
                  <span class="titleText">AUSTRALIA &amp; SOUTHERN PACIFIC</span>
                  <figure class="image">
                    <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/4fec6d2971e80585711d4c2eb06810d1.jpg"
                      alt="">
                  </figure>
                </a>
              </li>
              <li role="presentation">
                <a href="#tab-1" aria-controls="tab-northAmerica" role="tab" data-toggle="tab" class="isNavTab">
                  <span class="titleText">NORTH AMERICA</span>
                  <figure class="image">
                    <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/bb5f9a42f3483e9a20a2f63f38f722f5.png"
                      alt="">
                  </figure>
                </a>
              </li>
              <li role="presentation">
                <a href="#tab-2" aria-controls="tab-northAmerica" role="tab" data-toggle="tab" class="isNavTab">
                  <span class="titleText">SOUTH AMERICA</span>
                  <figure class="image">
                    <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/689fc218123094e61fe587494d3fab55.png"
                      alt="">
                  </figure>
                </a>
              </li>
              <li role="presentation" style="display: none;">
                <a href="#tab-3" aria-controls="tab-northAmerica" role="tab" data-toggle="tab" class="isNavTab">
                  <span class="titleText">ASIA</span>
                  <figure class="image">
                    <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/cd1f4f6641bfa8eb529eeda77425be62.png"
                      alt="">
                  </figure>
                </a>
              </li>
              <li role="presentation">
                <a href="#tab-4" aria-controls="tab-northAmerica" role="tab" data-toggle="tab" class="isNavTab">
                  <span class="titleText">MIDDLE EAST &amp; AFRICA</span>
                  <figure class="image">
                    <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/dfd9a5061ac9f4206b4bfdb9c940a39f.jpg"
                      alt="">
                  </figure>
                </a>
              </li>
              <li role="presentation">
                <a href="#tab-5" aria-controls="tab-northAmerica" role="tab" data-toggle="tab" class="isNavTab">
                  <span class="titleText">EUROPE</span>
                  <figure class="image">
                    <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/ae8c1cf455c8a61f9f3ab9568c29e930.png"
                      alt="">
                  </figure>
                </a>
              </li>
            </ul>
          </div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs hidden-xs" role="tablist">
            <li role="presentation">
              <a href="#tab-00" aria-controls="tab-northAmerica" role="tab" data-toggle="tab">
                <span class="titleText">ALL</span>
                <figure class="image">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/4fec6d2971e80585711d4c2eb06810d1.jpg"
                    alt="">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/3b6b0e30e108c4eac7af98f7daca4b70.jpg"
                    alt="">
                </figure>
              </a>
            </li>
            <li role="presentation">
              <a href="#tab-0" aria-controls="tab-northAmerica" role="tab" data-toggle="tab">
                <span class="titleText">AUSTRALIA &amp; SOUTHERN PACIFIC</span>
                <figure class="image">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/4fec6d2971e80585711d4c2eb06810d1.jpg"
                    alt="">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/3b6b0e30e108c4eac7af98f7daca4b70.jpg"
                    alt="">
                </figure>
              </a>
            </li>
            <li role="presentation">
              <a href="#tab-1" aria-controls="tab-northAmerica" role="tab" data-toggle="tab">
                <span class="titleText">NORTH AMERICA</span>
                <figure class="image">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/bb5f9a42f3483e9a20a2f63f38f722f5.png"
                    alt="">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/e340baa452b4704f004101c131496f1f.png"
                    alt="">
                </figure>
              </a>
            </li>
            <li role="presentation">
              <a href="#tab-2" aria-controls="tab-northAmerica" role="tab" data-toggle="tab">
                <span class="titleText">SOUTH AMERICA</span>
                <figure class="image">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/689fc218123094e61fe587494d3fab55.png"
                    alt="">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/82036ca833e6c29ecf0db6bb9d51dabb.png"
                    alt="">
                </figure>
              </a>
            </li>
            <li role="presentation" class="active">
              <a href="#tab-3" aria-controls="tab-northAmerica" role="tab" data-toggle="tab">
                <span class="titleText">ASIA</span>
                <figure class="image">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/cd1f4f6641bfa8eb529eeda77425be62.png"
                    alt="">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/de341f68dbec9bef8ff1bdf23307dda7.png"
                    alt="">
                </figure>
              </a>
            </li>
            <li role="presentation">
              <a href="#tab-4" aria-controls="tab-northAmerica" role="tab" data-toggle="tab">
                <span class="titleText">MIDDLE EAST &amp; AFRICA</span>
                <figure class="image">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/dfd9a5061ac9f4206b4bfdb9c940a39f.jpg"
                    alt="">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/d70075dec0f48ffb1d33f7e87741532a.jpg"
                    alt="">
                </figure>
              </a>
            </li>
            <li role="presentation">
              <a href="#tab-5" aria-controls="tab-northAmerica" role="tab" data-toggle="tab">
                <span class="titleText">EUROPE</span>
                <figure class="image">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/ae8c1cf455c8a61f9f3ab9568c29e930.png"
                    alt="">
                  <img src="http://contango-digital.com/demo/BTI/static/admin/public/uploads/business/images/d1457fc1f813cf9a20c299934b1df3f7.png"
                    alt="">
                </figure>
              </a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade" id="tab-00">
              <h2>ALL</h2>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab-0">
              <h2>AUSTRALIA &amp; SOUTHERN PACIFIC</h2>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab-1">
              <h2>NORTH AMERICA</h2>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab-2">
              <h2>SOUTH AMERICA</h2>
            </div>
            <div role="tabpanel" class="tab-pane fade active in" id="tab-3">
              <div class="row">
                <?php 
                $flagNames = array(
                  'CAMBODIA',
                  'CHINA',
                  'CHINA',
                  'HONG KONG',
                  'JAPAN',
                  'NORTH KOREA',
                  'LAOS',
                  'MALAYSIA',
                  'MONGOLIA',
                  'MYANMAR',
                  'MYANMAR',
                  'MYANMAR',
                  'MYANMAR',
                  'MYANMAR',
                  'NEPAL',
                  'PHILIPPINES',
                  'SINGAPORE',
                  'SRI LANKA',
                  'TAIWAN',
                  'TAIWAN',
                  'VIETNAM'
                );
                $flagImages = array(
                  './img/flags/kh.svg',
                  './img/flags/cn.svg',
                  './img/flags/cn.svg',
                  './img/flags/hk.svg',
                  './img/flags/jp.svg',
                  './img/flags/kp.svg',
                  './img/flags/la.svg',
                  './img/flags/my.svg',
                  './img/flags/mn.svg',
                  './img/flags/mm.svg',
                  './img/flags/mm.svg',
                  './img/flags/mm.svg',
                  './img/flags/mm.svg',
                  './img/flags/mm.svg',
                  './img/flags/np.svg',
                  './img/flags/ph.svg',
                  './img/flags/sg.svg',
                  './img/flags/lk.svg',
                  './img/flags/tw.svg',
                  './img/flags/tw.svg',
                  './img/flags/vn.svg'
                );
                $flagDetails = array(
                  'CP&A Trading International Co., Ltd. <br>
                  Mr. Cheam phan<br>
                  Telephone : +85515553838<br>
                  Email : sun.cheam@cp-a.com.kh',
                  'SHANGHAI JD TRADIND CO., LTD.<br>
                  Mr. Koh Tan Firm<br>
                  Telephone : +8602164822119,<br>
                  +8602164822050<br>
                  Email : tanfirm@gmail.com',
                  'unnan Feiside Economic Trading Co, Ltd<br>
                  Ms. Irina Yi<br>
                  Telephone : +8615969446807<br>
                  Email : sfert_irina@msn.com',
                  'Telford International Company Limited<br>
                  Ms. Michelle Wan<br>
                  Telephone : +86223152562<br>
                  Email : michelle_wan@telford.com.hk',
                  'KAMEI Corporation<br>
                  Mr. Hiromitsu Go<br>
                  Telephone : +81364590480<br>
                  Email : h-gou@ikemitsu.co.jp',
                  'Hitejinro Co., Ltd.<br>
                  Ms. Jinju Kwon<br>
                  Telephone : +82232190339<br>
                  Email : jinju.k@hitejinro.com',
                  'PS IMPORT & EXPORT TRADING CO.,LTD.<br>
                  คุณธเธนศ<br>
                  Telephone : +844036245<br>
                  Email : fortune.999@hotmail.com',
                  'ARUMAX<br>
                  william Tan<br>
                  Telephone : +60122003535<br>
                  Email : williamtan_1199@yahoo.com',
                  'Naran Trade<br>
                  Ms. Sarnai<br>
                  Telephone : +97670113544<br>
                  Email : sarnai.intc@narangroup.mn',
                  'MHTS Duty Free Shop Meakong<br>
                  คุณดุสิต มังคละคีรี<br>
                  Telephone : 081-7520125<br>
                  Email : mekonggroup.ms@gmail.com',
                  'A&K IMPORT - EXPORT ENTERPRISE<br>
                  คุณเก๋ง, คุณสมพงษ์<br>
                  Telephone : 081-5303580, 086-6695655<br>
                  Email : sompong@akthailand.com',
                  'Shwe Kaw Thaung (MaeSod)<br>
                  คุณวิชัย, คุณจอย<br>
                  Telephone : 089-7033637, 085-0358558<br>
                  Email : ps_phatcha1984@hotmail.com',
                  'Shwe Kaw Thaung (Ranong)<br>
                  คุณย้ง, คุณเจี๊ยบ<br>
                  Telephone : 081-4320610, 081-3042126<br>
                  Email : p_pirin@hotmail.co.th',
                  'MWAY MYINT AUNG CO., LTD.<br>
                  คุณจุติ วงศ์ฉิน (เซิน)<br>
                  Telephone : +661981-445, 090-498-2355<br>
                  Email : mway_myint_aung@hotmail.com',
                  'Evolution Beverages Pvt. Ltd.<br>
                  Mr.Tarun Bajracharya<br>
                  Telephone : +977 1 424408<br>
                  Email : evotrade@wlink.com.np',
                  'B&L Worldwide Marketing Corporation<br>
                  Ms. Lucinda Tee<br>
                  Telephone : +639175796718 , +639088653658<br>
                  Email : lucinda_tee168@yahoo.com',
                  'Yen Investments Pte. Ltd.<br>
                  Mr. Patrick Koh T.C<br>
                  Telephone : +6593665001<br>
                  Email : kohtc@yen.com.sg',
                  'FAVOURITE INTERNATIONAL (PVT) LTD<br>
                  Mr. C.R. Suraj De Silva<br>
                  Telephone : +94777747400<br>
                  Email : suraj@favouritegroup.com',
                  'Podium International Inc.<br>
                  Ms. Grace Su<br>
                  Telephone : +886225979913<br>
                  Email : peak9232@seed.net.tw',
                  'Tait Marketing & Distribution Co.,Ltd.<br>
                  Ms. Erica Tsai<br>
                  Telephone : +886227216600 Ext. 514<br>
                  Email : erica_tsai@taitnet.com.tw',
                  'C & T iTrading Company Limited<br>
                  Mr. chinnawat  Teeraphatpornchai<br>
                  Telephone : +84839911573<br>
                  Email : Chinnawat.teeraphatpornchai@ct-itrading.com'
                );
                for ($i=0; $i < count($flagNames) ; $i++) : ?>
                <div class="col-md-3 col-sm-4 col-xs-6">
                  <h2 class="media-title"><?php echo($flagNames[$i]); ?></h2>
                  <div class="media">
                    <div class="media-left">
                      <div class="media-left-inner">
                        <a href="#">
                          <img class="media-object" src="<?php echo($flagImages[$i]); ?>">
                        </a>
                        <?php if ($i === 0): ?>
                        <div class="media-left-brand">
                          <img src="./img/represent_logo-singha.png" alt="">
                          <img src="./img/represent_logo-leo.png" alt="">
                        </div>
                        <?php elseif ($i === 1): ?>
                        <div class="media-left-brand">
                          <img src="./img/represent_logo-singha.png" alt="">
                        </div>
                        <?php elseif ($i === 2): ?>
                        <div class="media-left-brand">
                          <img src="./img/represent_logo-leo.png" alt="">
                        </div>
                        <?php endif; ?>
                      </div>
                    </div>
                    <div class="media-body">
                      <?php echo($flagDetails[$i]); ?>
                    </div>
                  </div>
                </div>
                <?php endfor; ?>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab-4">
              <h2>MIDDLE EAST</h2>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab-5">
              <h2>EUROPE</h2>
            </div>
          </div>          
        </div>
      </div>
    </div>
  </div>
  <?php include'_inc-footer.php'; ?>




  <?php include'_inc-globalJS.php'; ?>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('representativePage');
    $('.sgh_sectionRepresentative .media').matchHeight();
  </script>
</body>

</html>
