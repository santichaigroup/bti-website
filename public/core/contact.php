<?php include'_inc-config.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <?php include'_inc-docHead.php'; ?>
</head>
<body>
  <?php include'_inc-header.php'; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight isGmap">
      <div class="sgh_sectionInner">
        <div class="thumbnailHighlight">
          <div class="imageWrap">
            <div class="image">
              <div id="map"></div>
              <script>
                function initMap() {
                  var latAndLng = {
                    lat: 13.791315,
                    lng: 100.515651
                  }
                  var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 14,
                    center: {
                      lat: 13.809,
                      lng: 100.515651
                    }
                  });
                  var contentString = 
                      '<div class="map-content">'+
                        '<div class="map-icon">'+
                          '<img src="img/map-logo-singha.png" alt="">'+
                        '</div>'+
                        '<div class="map-content-body">'+
                          '<p> <b>BOONRAWD TRADING INTERNATIONAL COMPANY LIMITED</b> <br> 83 Amnuay-Songkran Road, Dusit, Bangkok 10300 Thailand</p>'+
                          '<p> <a href="https://www.google.com/maps/place/Boonrawd+Trading+Co.,LTD./@13.791239,100.5134194,17z/data=!3m1!4b1!4m5!3m4!1s0x30e29be57411e519:0xe3b3dcc99520de96!8m2!3d13.791239!4d100.5156081" target="_blank">GET DIRECTION</a></p>'+
                        '</div>'+
                      '</div>';

                  var infowindow = new google.maps.InfoWindow({
                    content: contentString,
                    maxWidth: 400
                  });
                  var image = 'img/pin-map.svg';
                  var marker = new google.maps.Marker({
                    position: latAndLng,
                    map: map,
                    icon: image
                  });
                  marker.addListener('click', function() {
                    infowindow.open(map, marker);
                  });  
                  setTimeout(() => {
                    infowindow.open(map, marker);
                  }, 500);
                  google.maps.event.addListener(infowindow, 'domready', function() {
                    var color = '#fac214';
                    $('#map .gm-style-iw').css({'color': '#000000','font-family' : 'korolev_condensedmedium', 'font-size': '16px', 'padding-top': '20px'});
                    $('#map .gm-style-iw a').css({'color': '#000000', 'text-decoration': 'underline'});
                    $('#map .gm-style-iw .map-icon').css('margin-bottom', '20px');
                    $('#map .gm-style-iw').prev().find(' > div:nth-child(4)').css('background-color', color);
                    $('#map .gm-style-iw').prev().find(' > div:nth-child(3) > div > div').css('background-color', color);
                  })
                }
              </script>
              <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDY7AE3e-2KArsOjo_GgBRI5nRAr1D_uew&callback=initMap">
              </script>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionContactDetail">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader text-center">
            <h1 class="titleText">CONTACT</h1>
          </div>
          <div class="row">
            <div class="col-sm-5">
              <address class="contactAddress">
                <h2 class="sgh_sectionSubHeader">BOONRAWD TRADING INTERNATIONAL CO., LTD.</h2>
                <ul class="list-unstyled">
                  <li> <span class="ftlo-location"></span> <span>83 Amnuay-Songkran Road, Dusit, Bangkok 10300 Thailand</span></li>
                  <li> <span class="ftlo-phone"></span> <span>Tel.: (662) 636-9600</span></li>
                  <li> <span class="ftlo-fax"></span> <span>Fax.: (662) 669-5456, 669-4261</span></li>
                </ul>
              </address>
            </div>
            <div class="col-sm-7">
              <div class="formTheme">
                <h2 class="sgh_sectionSubHeader">LEAVE A MESSAGE</h2>
                <div class="row">
                  <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                      <select class="selectpicker form-control" title="SELECT CONTACT TYPE *">
                        <option>Mustard</option>
                        <option>Ketchup</option>
                        <option>Relish</option>
                      </select>              
                    </div>                
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                      <label class="form-control-floatText">
                        <div class="label-control">SUBJECT</div>
                        <input type="text" class="form-control">
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-4">
                    <div class="form-group">
                      <label class="form-control-floatText">
                        <div class="label-control">NAME <span class="text-require">*</span></div>
                        <input type="text" class="form-control">
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4">
                    <div class="form-group">
                      <label class="form-control-floatText">
                        <div class="label-control">E-MAIL <span class="text-require">*</span></div>
                        <input type="text" class="form-control">
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4">
                    <div class="form-group">
                      <label class="form-control-floatText">
                        <div class="label-control">TEL <span class="text-require">*</span></div>
                        <input type="text" class="form-control">
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label class="form-control-floatText isTextArea">
                        <div class="label-control">MESSAGE</div>
                        <textarea class="form-control" cols="30" rows="10"></textarea>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group isReCaptcha">
                  <img src="img/dummy-reCaptcha.png" alt="">
                </div>
                <div class="form-group">
                  <button class="btn btnTheme"><span>SUBMIT <span class="ftlo-right"></span></span></button>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include'_inc-footer.php'; ?>




  <?php include'_inc-globalJS.php'; ?>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('contactPage');
  </script>
</body>

</html>
