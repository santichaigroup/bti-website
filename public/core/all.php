<?php include'_inc-config.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <?php include('_inc-docHead.php'); ?>
  <style>
  h1 {
    text-align:center;
    padding:20px;
  }

  .mainHyperLink .btn { 
    margin-bottom: 15px;
    font-size: 12px;
    font-family: 'Tahoma';
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.22);

  }
  [data-filename*="_inc-"],
  [data-filename="all.php"],
  [data-filename=".htaccess"],
  [data-filename=".DS_Store"],
  [data-filename*="_element_"],
  [data-filename=".gitignore"] {
    display:none;
  }

</style>
</head>

<body>
<div class="mainHyperLink">
  <div class="container" style="padding: 60px 15px;">
  <div class="row">
    <?php

        $dir = scandir('.');
        sort($dir,0);
        $html = '';
        $i=3;
        foreach ($dir as $d){
          if ($d == '.' || $d == '..' || is_dir($d)) continue;
          $a = explode('_',$d);
          if ($a[0] > 0) $i++;
          $html .= '<div class="col-md-4 col-sm-6" data-filename="'.$d.'"><a href="'.$d.'" target="_blank" class="btn btn-default btn-block">'.$d.'</a></div>';
        }
        echo $html;
    ?>
  </div>
  </div>
</div>

  <?php include'_inc-globalJS.php'; ?>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('afterHomePage','totalPage');

  </script>
</body>

</html>
