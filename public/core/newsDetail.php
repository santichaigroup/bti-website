<?php include'_inc-config.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <?php include'_inc-docHead.php'; ?>
</head>
<body>
  <?php include'_inc-header.php'; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight isSM">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php for ($i=0; $i < 1 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
                <style>
                  @media (min-width: 768px) {
                    .sgh_item:nth-child(<?php echo($i) + 1; ?>) .thumbnailHighlight .image {
                      background-image: url(img/highlight-news.png);
                    }
                  }
                </style>
                <div class="image">
                  <img class="visible-xs" src="img/highlightMobile-news.png" alt="">
                </div>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionArticle isNewsDetail">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionArticleFeature">
            <div class="breadcrumbTheme">
              <nav>
                <span><a href="">NEWS</a></span>
                <span><span>SED VEL SCELERISQUE NISL</span></span>
              </nav>
            </div>
            <div class="simpleButtons">
              <ul class="list-inline">
                <li><a href="">BACK</a></li>
                <li> | </li>
                <li><a href="">PRINT</a></li>
              </ul>
            </div>
          </div>
          <div class="articleTheme sgh_editor">
            <article>
              <p style="text-align: center;"><iframe allowfullscreen="" frameborder="0" height="360" src="//www.youtube.com/embed/i0vGcvzXZ3Y" width="640"></iframe></p>
              <header class="sgh_sectionHeader text-center isSM">
                <h1 class="titleText">SED VEL SCELERISQUE NISL</h1>
              </header>
              <p>	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac bibendum elit. Phasellus ex nulla, aliquet at turpis quis, ultrices
                hendrerit erat. Duis magna urna, dapibus vel blandit quis, ullamcorper in nunc. Sed lacus libero, interdum sed interdum laoreet, imperdiet vel nisl.
                Curabitur hendrerit tellus libero, ac eleifend magna dictum quis. Aliquam ullamcorper vel justo nec fermentum. Aliquam faucibus, ligula quis auctor
                rhoncus, nulla nunc aliquam erat, a hendrerit nisi eros ac mi. Vivamus tempus pharetra est sit amet congue. Mauris accumsan lacinia feugiat.
                Aliquam nec sagittis tortor, ut porta nulla. Maecenas facilisis nisl pharetra purus dictum, sed venenatis nisl semper. Interdum et malesuada fames
                ac ante ipsum primis in faucibus. Quisque vel feugiat ante, fringilla tincidunt dolor. Ut non ex id nisl pretium aliquam vitae nec mi. Lorem ipsum dolor
                sit amet, consectetur adipiscing elit.</p>
              <figure>
                <img src="img/dummy-newsDetail.png" alt="">
              </figure>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel libero, amet soluta consectetur sequi modi minus officia. In exercitationem sit commodi modi dolor, accusantium molestias fugiat! Ullam porro dolorem vitae?</p>
              <p>Voluptates sint non magnam quibusdam nesciunt ipsa nobis, odit incidunt voluptate dolor necessitatibus minus eaque, sequi eius est nemo expedita saepe repellendus. Accusamus rerum eligendi quisquam qui molestiae possimus accusantium.</p>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include'_inc-footer.php'; ?>




  <?php include'_inc-globalJS.php'; ?>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('newsDetailPage');
  </script>
</body>

</html>
