<?php 
  include'_inc-config.php'; 
  $page = 'business';
  $pageSub = './about.php'
?>
<!DOCTYPE html>
<html>
<head>
  <?php include'_inc-docHead.php'; ?>
</head>
<body>
  <?php include'_inc-header.php'; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php for ($i=0; $i < 1 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
                <style>
                  @media (min-width: 768px) {
                    .sgh_item:nth-child(<?php echo($i) + 1; ?>) .thumbnailHighlight .image {
                      background-image: url(img/highlight-about.png);
                    }
                  }
                </style>
                <div class="image">
                  <img class="visible-xs" src="img/highlightMobile-about.png" alt="">
                </div>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionArticle">
      <div class="container">
        <div class="sgh_sectionInner">
          <article>
            <div class="sgh_sectionHeader text-center">
              <h1 class="titleText">ABOUT US</h1>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <figure class="image"><img src="img/dummy-thumbnailAbout-1.png" alt=""></figure>
              </div>
                <p class="col-sm-5">
                <b>Boonrawd Trading International Company Limited (BTI)</b> was
                established in 2001. BTI operates as a trading company tasking with distributing
                food and beverage including alcohol and non-alcohol products to more than
                50 countries around the world.                  
              </p>
              <p class="col-sm-12 col-md-5">
                Apart from products under Boonrawd and Singha’s umbrella, BTI also selects
                premium food and beverage from high quality sources to export to global market
                along with supplying food services to hotel and restaurant businesses with highest
                quality ingredients. The ready-to-eats and finished recipes can be customized
                base on customers’ requirements as well.                   
              </p>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <p>
                  We also have an end-to-end service to provide for the next generation of new entrepreneurs
                  with the ability to source OEM service for your liking. Starting from the beginning to the end,
                  we will take care of your chosen products as early as the state of production line to as far as foreign
                  distribution. BTI is the leading player in the international trading world. Throughout our channels,
                  your products are guaranteed to be seen and purchased by potential customers across the globe.
                </p>
              </div>
              <div class="col-sm-6">
                <figure class="image"><img src="img/dummy-thumbnailAbout-2.png" alt=""></figure>
              </div>
            </div>
          </article>
        </div>
      </div>
    </div>
  </div>
  <?php include'_inc-footer.php'; ?>




  <?php include'_inc-globalJS.php'; ?>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('aboutPage');
  </script>
</body>

</html>
