'use strict';

$(document).ready(function () {
  Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) {
      s = "0" + s;
    }
    return s;
  };
  $('.sgh_sectionHighlight .sgh_items.isSlider').flickity({
    selectedAttraction: 0.03,
    on: {
      ready: function ready() {
        var $thisSlider = this.$element;
        var thisSlider = this;
        $thisSlider.after('<div class="sgh_sectionHighlightLength"><span>00</span><span>/</span><span>00</span></div>');
        function setCustomUI() {
          var rSide = $(window).width() - $('.header .btnHamburger').offset().left - $('.header .btnHamburger').width();
          var $dots = $thisSlider.find('.flickity-page-dots');
          var $btnPrev = $thisSlider.find('.flickity-prev-next-button.previous');
          var $btnNext = $thisSlider.find('.flickity-prev-next-button.next');
          $dots.css({
            'opacity': 1,
            'right': rSide
          });
          $btnPrev.css({
            'opacity': 1,
            'right': rSide,
            'left': 'auto',
            'top': $dots.offset().top - $btnPrev.outerHeight() - 20
          });
          $btnNext.css({
            'opacity': 1,
            'right': rSide,
            'left': 'auto',
            'top': $dots.offset().top + $dots.outerHeight() + 20
          });
          $thisSlider.parent().find('.sgh_sectionHighlightLength').css({
            'opacity': 1,
            'right': rSide,
            'left': 'auto',
            'top': $dots.offset().top + $dots.outerHeight() + $btnPrev.outerHeight() + 50
          });
          $thisSlider.parent().find('.sgh_sectionHighlightLength > *').eq(0).text((thisSlider.selectedIndex + 1).pad(2));
          $thisSlider.parent().find('.sgh_sectionHighlightLength > *').eq(2).text(thisSlider.cells.length.pad(2));
          if (thisSlider.cells.length < 2) {
            $('.sgh_sectionHighlightLength').hide();
            $('.flickity-page-dots').hide();
            $('.flickity-prev-next-button').hide();
          }
        }
        var resizeTimer = void 0;
        $(window).on('resize', function (e) {
          clearTimeout(resizeTimer);
          resizeTimer = setTimeout(function () {
            setCustomUI();
          }, 600);
        });
        setTimeout(function () {
          setCustomUI();
        }, 600);
      },
      change: function change(index) {
        this.$element.parent().find('.sgh_sectionHighlightLength > *').eq(0).text((index + 1).pad(2));
      }
    }
  });
});