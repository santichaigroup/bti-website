<?php 
  include'_inc-config.php'; 
  $page = 'business';
  $pageSub = './representative.php'
?>
<!DOCTYPE html>
<html>
<head>
  <?php include'_inc-docHead.php'; ?>
</head>
<body>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionAgeVerification">
      <div class="sgh_sectionInner">
        <div class="ageVerificationForm">
          <div class="ageVerificationBrand"><figure><img src="./img/logo-singha.png" alt=""></figure></div>
          <div class="ageVerificationTitle">PLEASE FILL IN YOUR YEAR OF BIRTH</div>
          <div class="formTheme">
            <div class="form-group isCountry">
              <select class="selectpicker form-control">
                <option>THAILAND</option>
                <option>Ketchup</option>
                <option>Relish</option>
              </select>              
            </div>  
            <div class="form-group isBirthYear">
              <select class="selectpicker form-control">
                <option>2018</option>
                <option>Ketchup</option>
                <option>Relish</option>
              </select>              
            </div>
            <div class="form-group isButton">
              <button class="btn btn-block btnTheme isThemeDiff1"><span>ENTER <span class="ftlo-right"></span></span></button>  
            </div>  
          </div>
          <div class="ageVerificationDesc">by choosing “enter” you agree to our <br class="visible-xs"> <a href="">terms &amp; conditions</a>  /  <a href="">privacy policy</a></div>
        </div>
      </div>
    </div>
  </div>


  <?php include'_inc-globalJS.php'; ?>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('ageVerificationPage');
  </script>
</body>

</html>
