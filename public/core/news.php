<?php include'_inc-config.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <?php include'_inc-docHead.php'; ?>
</head>
<body>
  <?php include'_inc-header.php'; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php for ($i=0; $i < 1 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
                <style>
                  @media (min-width: 768px) {
                    .sgh_item:nth-child(<?php echo($i) + 1; ?>) .thumbnailHighlight .image {
                      background-image: url(img/highlight-news.png);
                    }
                  }
                </style>
                <div class="image">
                  <img class="visible-xs" src="img/highlightMobile-news.png" alt="">
                </div>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionNewsItems">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader text-center">
            <h1 class="titleText">NEWS</h1>
          </div>
          <div class="row">
            <?php 
              $newsImage = array(
                'img/dummy-thumbnailNews-1.png', 
                'img/dummy-thumbnailNews-2.png', 
                'img/dummy-thumbnailNews-3.png', 
                'img/dummy-thumbnailNews-4.png',
                'img/dummy-thumbnailNews-5.png',
                'img/dummy-thumbnailNews-6.png',
                'img/dummy-thumbnailNews-7.png',
                'img/dummy-thumbnailNews-8.png'
              );            
            for ($i=0; $i < count($newsImage) ; $i++) : ?>
            <div class="col-sm-4 col-md-3">
              <div class="thumbnailNews">
                <div class="imageWrap">
                  <a href="./newsDetail.php"><figure class="image" data-crop="news"><img src="<?php echo($newsImage[$i % 8]); ?>" alt=""></figure></a>
                </div>
                <div class="captionWrap">
                  <div class="caption">
                    <div class="eqh">
                      <div class="date">17 APR 2018</div>
                      <h3 class="titleText">SINGHA NEWS</h3>
                      <p class="desc">Consectetur adipiscing elit. Fusce sodales scelerisque bibendum. Nullam sodales tincidunt libero vel euismod Curabitur semper augue id dolor iaculis pulvinar.</p>
                    </div>
                    <div class="button"><a href="./newsDetail.php"><span><span>MORE DETAIL</span><span class="ftlo-right"></span</span></a></div>
                  </div>
                </div>
              </div>
            </div>
            <?php endfor; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include'_inc-footer.php'; ?>




  <?php include'_inc-globalJS.php'; ?>
  <script>
    var cols;
    // define page by classname
    document.querySelector('html').classList.add('newsPage');
    // $(document).ready(function () {
    //   tweenSectionHeader('.sgh_sectionNewsItems');
    //   $('.sgh_sectionNewsItems .sgh_sectionInner > .row > *').each(function (index, element) {
    //     tweenProducts('.' + element.classList[0] + ':nth-child(' + (index + 1) + ')', index)
    //   });
    // });

    // function tweenProducts (item, delay) {
    //   cols = 4;
    //   $(window).resize(function () { 
    //     getCols()
    //   });
    //   getCols()
    //   function getCols () {
    //     cols = 4
    //     if ($(window).width() < 992 && $(window).width() >= 768) {
    //       cols = 3
    //     } else if ($(window).width() < 768) {
    //       cols = 1
    //     }
    //   }      
    //   var tl = new TimelineLite();
    //   tl.from(item + ' .thumbnailNews ', 0.4, {y:-100, opacity:0, scale: 1.05, delay: (delay % cols) * 0.4})
    //   .from(item + ' .thumbnailNews .imageWrap .image', 0.8, {y:-100, opacity:0, scale: 1.05})
    //   .from(item + ' .thumbnailNews .caption .date', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6')
    //   .from(item + ' .thumbnailNews .caption .titleText', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6')
    //   .from(item + ' .thumbnailNews .caption .desc', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6')
    //   .from(item + ' .thumbnailNews .caption .button', 0.8, {y:100, opacity:0, scale: 1.05}, '-=0.6');
    //   var scene = new ScrollMagic.Scene({
    //     triggerElement: item,
    //     triggerHook: 0.8
    //   })
    //   .setTween(tl)
    //   // .addIndicators()
    //   .addTo(ScrollMagicController); 
    //   return scene;     
    // }    
  </script>
</body>

</html>
