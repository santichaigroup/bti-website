<?php 
  include'_inc-config.php'; 
  $page = 'product';
  $pageSub = './productDetail.php'
?>
<!DOCTYPE html>
<html>
<head>
  <?php include'_inc-docHead.php'; ?>
</head>
<body>
  <?php include'_inc-header.php'; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight isSM">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php for ($i=0; $i < 1 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
                <style>
                  @media (min-width: 768px) {
                    .sgh_item:nth-child(<?php echo($i) + 1; ?>) .thumbnailHighlight .image {
                      background-image: url(img/highlight-news.png);
                    }
                  }
                </style>
                <div class="image">
                  <img class="visible-xs" src="img/highlightMobile-news.png" alt="">
                </div>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
    <div class="sgh_sectionProductDetailWrap">
      <div class="sgh_section sgh_sectionProductDetailFeature">
        <div class="container">
          <div class="sgh_sectionInner">
            <div class="button">
              <a href="" class="btnBack"><span class="ftlo-left-open"></span> <span>BACK TO PRODUCT</span></a>
            </div>
          </div>
        </div>
      </div>
      <div class="sgh_section sgh_sectionProductDetailIntro">
        <div class="container">
          <div class="sgh_sectionInner">
            <div class="sgh_sectionHeader">
              <h1 class="titleText"><b>SINGHA</b> BEER</h1>
              <div class="titleText_sub">THE ORIGINAL THAI BEER</div>
              <div class="desc">
                <p>Consectetur adipiscing elit. Fusce sodales scelerisque bibendum. Nullam sodales tincidunt libero vel euismod Curabitur semper augue id dolor iaculis pulvinar.</p>
              </div>
            </div>
            <div class="sgh_shortcutLinks">
              <ul class="list-inline">
                <li><a href="https://www.youtube.com/watch?time_continue=6&v=Yy0HwvyyP3o" data-fancybox><span class="ftlo-play-circled2"></span><span>SINGHA BEER VIDEO</span></a></li>
                <li><a href=""><span class="ftlo-doc-text"></span><span>BROCHURE</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="sgh_section sgh_sectionProductDetailHighlight">
        <div class="container">
          <div class="sgh_sectionInner">
            <div class="sgh_items">
              <div class="sgh_item">
                <div class="thumbnailProductHighlight hideCaption">
                  <div class="imageWrap">
                    <figure class="image"><img src="img/product-beer-can.png" alt=""></figure>
                  </div>
                  <div class="captionWrap">
                    <div class="caption">
                      <ul class="list-unstyled">
                        <li class="isTitleText"><b>CAN</b></li>
                        <li>
                          <b>320ml.</b> <br>
                          <span>EXPORT SIZE <b>330 ml.</b></span>
                        </li>
                        <li>
                          <b>PRODUCT DIMENSIONS</b> <br>
                          <span>W 6.6 CM.</span> <br>
                          <span>H 11.52 CM.</span>
                        </li>
                        <li>
                          <b>CALORIE</b> <br>
                          <span>119 kcal.</span>
                        </li>
                        <li>
                          <b>5% ALCOHOL</b> BY VOLUME
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="sgh_item">
                <div class="thumbnailProductHighlight hideCaption">
                  <div class="imageWrap">
                    <figure class="image"><img src="img/product-beer-bottle-sm.png" alt=""></figure>
                  </div>
                  <div class="captionWrap">
                    <div class="caption">
                      <ul class="list-unstyled">
                        <li class="isTitleText"><b>BOTTLE</b></li>
                        <li>
                          <b>320ml.</b> <br>
                          <span>EXPORT SIZE <b>330 ml.</b></span>
                        </li>
                        <li>
                          <b>PRODUCT DIMENSIONS</b> <br>
                          <span>W 5.8 CM.</span> <br>
                          <span>H 23.1 CM.</span>              
                        </li>
                        <li>
                          <b>CALORIE</b> <br>
                          <span>119 kcal.</span>
                        </li>
                        <li>
                          <b>5% ALCOHOL</b> BY VOLUME
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="sgh_item">
                <div class="thumbnailProductHighlight hideCaption">
                  <div class="imageWrap">
                    <figure class="image"><img src="img/product-beer-bottle-lg.png" alt=""></figure>
                  </div>
                  <div class="captionWrap">
                    <div class="caption">
                      <ul class="list-unstyled">
                        <li class="isTitleText"><b>BOTTLE</b></li>
                        <li>
                          <b>620ml.</b> <br>
                          <span>EXPORT SIZE <b>630 ml.</b></span>
                        </li>
                        <li>
                          <b>PRODUCT DIMENSIONS</b> <br>
                          <span>W 5.8 CM.</span> <br>
                          <span>H 23.1 CM.</span>
                        </li>
                        <li>
                          <b>CALORIE</b> <br>
                          229 kcal.
                        </li>
                        <li>
                          <b>5% ALCOHOL</b> BY VOLUME
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="sgh_section sgh_sectionProductDetail">
        <div class="container">
          <div class="sgh_sectionInner sgh_editor">
            <div class="row">
              <div class="col-sm-6">
                <div class="thumbnailProductDetail">
                  <figure class="image"><img src="img/product-detail-thumbnail-hop.png" alt=""></figure>
                  <div class="caption">
                    <h2 class="titleText"><span class="isHL1">DOUBLE SAAZ</span> HOPS</h2>
                    <p>A premium hop made in Europe will refresh you <br> with one-of-a-kind taste from flower and herbs. </p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="thumbnailProductDetail">
                  <figure class="image"><img src="img/product-detail-thumbnail-malt.png" alt=""></figure>
                  <div class="caption">
                    <h2 class="titleText"><span class="isHL1">BARLEY</span> MALT</h2>
                    <p>Singha sets its international standard as <br> the only Thai brewer who purely uses 100% premium <br> barley for distinctively tasty sip. </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="thumbnailProductDetail">
              <figure class="image"><img src="img/product-detail-thumbnail-beer-bottle.png" alt=""></figure>
              <div class="caption">
                <h2 class="titleText">ABOUT <span class="isHL1">GARUDA</span></h2>
                <p>Just like other Thai banks and trusted local companies, the story behind garuda symbol on our beer bottle <br> can be traced back to 1939 when King Rama VIII gave Boonrawd Brewery an allowance to use the symbol. At that time, <br> Garuda symbol represented company’s highest honor meaning that Singha is trusted as financially <br> stable company which produces quality goods.</p>
                <p>After receiving the symbol of pride, Bhirombhakdi family has proudly presented this <br> iconic symbol on all office buildings, factories or even on every Singha product tags.</p>
              </div>
            </div>
            <div class="thumbnailProductDetail">
              <figure class="image"><img src="img/product-detail-thumbnail-beer-singha-logo.png" alt=""></figure>
              <div class="caption">
                <h2 class="titleText">ABOUT<br class="visible-xs"> <span class="isHL1">THE GOLDEN MYSTICAL LION</span></h2>
                <p>An iconic Golden Mystical Lion has become the ultimate logo of Singha that needs no further explanation. But <br> you might have no idea that the familiar logo replicates one of the four great lions in Thai literature which Thai folk believes <br> to be the king of Himmapan Forest. Singha’s current icon is the gold of Bandhu Rachasri lion—a refined <br> version from former typically yellow lion.</p>
              </div>
            </div>
            <p><iframe allowfullscreen="" data-video-ratio="16by9" frameborder="0" height="360" src="//www.youtube.com/embed/i0vGcvzXZ3Y" width="640"></iframe></p>
            <p><iframe allowfullscreen="true" data-video-ratio="1by1" allowtransparency="true" frameborder="0" height="560" scrolling="no" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FSingha%2Fvideos%2F842664665857975%2F&amp;show_text=0&amp;width=560" style="border:none;overflow:hidden" width="560"></iframe></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include'_inc-footer.php'; ?>




  <?php include'_inc-globalJS.php'; ?>
  <!-- fancybox -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('productDetailPage');
  </script>
</body>

</html>
