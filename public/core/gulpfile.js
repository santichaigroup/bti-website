var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var autoprefixer = require('gulp-autoprefixer');
var babel = require('gulp-babel');
var BrowserSync = require('browser-sync');
var reload = BrowserSync.reload;

var pathProject = 'HB/ibtt';
var pathLocalhost = 'http://localhost/gulp/www/'+ pathProject;

var autoprefix_browser = [
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

var assetPaths = {
    styles: {
        src: './scss',
        dist: './css'
    },
    scripts: {
        src: './babel',
        dist: './js'
    }
}

// function cleanCSS () {
//     return del(assetPaths.styles.dists);
// }

function styles () {
    return sass(`${assetPaths.styles.src}/main/*.scss`,{style:'nested'}) //nested (default), compact, compressed, or expanded.
    .on('error', function (err) {
        console.log(err.message);
    })
    .pipe(autoprefixer({
        browsers: autoprefix_browser
    }))
    .pipe(gulp.dest(assetPaths.styles.dist));
}


function scripts () {
	return gulp.src(`${assetPaths.scripts.src}/**/*`)
    .pipe(babel({
      "presets": ["env"]
    }))
    .on('error', function(e) {
      console.log('>>> ERROR', e);
      // emit here
      this.emit('end');
    })
    .pipe(gulp.dest(assetPaths.scripts.dist))
}

// BrowserSync
function  browserSync (done) {
    BrowserSync({
        proxy: pathLocalhost
    });
    done();
};

// BrowserSync Reload
function browserSyncReload(done) {
    reload();
    done();
}

function watchFiles () {
    gulp.watch(`${assetPaths.styles.src}/main/*.scss` , gulp.series(styles, browserSyncReload));
    gulp.watch(`${assetPaths.scripts.src}/*.js` , gulp.series(scripts, browserSyncReload));
    gulp.watch('./*', browserSyncReload);
    gulp.watch('./img/**/*', browserSyncReload);
}

gulp.task('default', gulp.parallel(styles, babel, watchFiles, browserSync))

gulp.task('styles', styles)