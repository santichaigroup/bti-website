<?php include'_inc-config.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <?php include'_inc-docHead.php'; ?>
</head>
<body>
  <?php include'_inc-header.php'; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php for ($i=0; $i < 4 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
                <style>
                  @media (min-width: 768px) {
                    .sgh_item:nth-child(<?php echo($i) + 1; ?>) .thumbnailHighlight .image {
                      background-image: url(img/highlight.png);
                    }
                  }
                </style>
                <div class="image">
                  <img class="visible-xs" src="img/highlightMobile.png" alt="">
                </div>
              </div>
              <div class="captionWrap">
                <div class="container">
                  <div class="caption">
                    <div class="captionInner">
                      <h1 class="titleText">
                        <span class="isHL1">BOONRAWD TRADING INTERNATIONAL</span>
                      </h1>
                      <div class="desc">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt minus odio quam quo vel, animi magni! Ipsa enim nemo reiciendis non possimus nobis eius, unde iure temporibus fugiat labore repellendus.</p>
                      </div>
                    </div>
                    <div class="buttonWrap">
                      <a href="" class="btn btnTheme isHighlight"><span>EXPLORE <span class="ftlo-right"></span></span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionProductsHighlight">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader">
            <h2 class="titleText">PRODUCT GROUP</h2>
            <div class="desc">
              <p>Consectetur adipiscing elit. Fusce sodales scelerisque bibendum. Nullam sodales tincidunt libero vel euismod Curabitur semper augue id dolor iaculis pulvinar.</p>
            </div>
          </div>
          <div class="sgh_items">
            <?php
            $productsImage = array(
              'img/dummy-thumbnailProducts-1.png',
              'img/dummy-thumbnailProducts-2.png',
              'img/dummy-thumbnailProducts-3.png',
              'img/dummy-thumbnailProducts-4.png'
            );
            $productsTitle = array(
              'BEER & ALCOHOL',
              'BEVERAGE',
              'FOOD',
              'SNACK'
            );
            $productsDesc = array(
              'Consectetur adipiscing elit. Fusce sodales scelerisque bibendum. Nullam sodales tincidunt libero vel euismod Curabitur semper augue id dolor iaculis pulvinar.',
              'Consectetur adipiscing elit. Fusce sodales scelerisque bibendum. Nullam sodales tincidunt libero vel euismod Curabitur semper augue id dolor iaculis pulvinar.',
              'Consectetur adipiscing elit. Fusce sodales scelerisque bibendum. Nullam sodales tincidunt libero vel euismod Curabitur semper augue id dolor iaculis pulvinar.',
              'Consectetur adipiscing elit. Fusce sodales scelerisque bibendum. Nullam sodales tincidunt libero vel euismod Curabitur semper augue id dolor iaculis pulvinar.'
            );
            for ($i=0; $i < 4 ; $i++) : ?>
            <div class="sgh_item" id="sgh_productItem-<?php echo($i); ?>">
              <div class="thumbnailProductsHighlight <?php echo($i % 2 === 1 ? 'isDiff': ''); ?>">
                <div class="imageWrap">
                  <div class="imageDecor">
                    <figure class="image" data-crop="products"><img src="<?php echo($productsImage[$i]); ?>"></figure>
                  </div>
                </div>
                <div class="captionWrap">
                  <div class="caption">
                    <div class="number">0<?php echo($i + 1); ?></div>
                    <h3 class="titleText"><span class="titleTextInner"><?php echo($productsTitle[$i]); ?></span></h3>
                    <div class="desc"><?php echo($productsDesc[$i]); ?></div>
                    <div class="button"><a href="" class="btn btnTheme"><span>More Detail <span class="ftlo-right"></span></span></a></div>
                  </div>
                </div>
              </div>
            </div>
            <?php endfor; ?>
          </div>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionNewsHighlight">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader hasFloatButton">
            <h2 class="titleText">
              <span>NEWS UPDATE</span>
              <div class="button">
                <a href="">VIEW ALL+</a>
              </div>
            </h2>
            <div class="desc">
              <p>Consectetur adipiscing elit. Fusce sodales scelerisque bibendum. Nullam sodales tincidunt libero vel euismod Curabitur semper augue id dolor iaculis pulvinar.</p>
            </div>
          </div>
        </div>
        <div class="sgh_items isSlider">
          <?php
          $newsImage = array(
            'img/dummy-thumbnailNews-1.png',
            'img/dummy-thumbnailNews-2.png',
            'img/dummy-thumbnailNews-3.png',
            'img/dummy-thumbnailNews-4.png'
          );
          for ($i=0; $i < 8 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailNews">
              <div class="imageWrap">
                <a href=""><figure class="image" data-crop="news"><img src="<?php echo($newsImage[$i % 4]); ?>" alt=""></figure></a>
              </div>
              <div class="captionWrap">
                <div class="caption">
                  <div class="eqh">
                    <div class="date">17 APR 2018</div>
                    <h3 class="titleText">SINGHA NEWS</h3>
                    <p class="desc">Consectetur adipiscing elit. Fusce sodales scelerisque bibendum. Nullam sodales tincidunt libero vel euismod Curabitur semper augue id dolor iaculis pulvinar.</p>
                  </div>
                  <div class="button"><a href=""><span><span>MORE DETAIL</span><span class="ftlo-right"></span</span></a></div>
                </div>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
  </div>
  <?php include'_inc-footer.php'; ?>




  <?php include'_inc-globalJS.php'; ?>

  <script>
    // define page by classname
    document.querySelector('html').classList.add('homePage');

    $(document).ready(function () {
      $('.sgh_sectionNewsHighlight .sgh_items.isSlider').flickity({
        cellAlign: 'left',
        prevNextButtons: false
      });
      tweenSectionHeader('.sgh_sectionProducts');
      tweenProducts('#sgh_productItem-0');
      tweenProducts('#sgh_productItem-1');
      tweenProducts('#sgh_productItem-2');
      tweenProducts('#sgh_productItem-3');
      tweenProducts('#sgh_productItem-4');
      tweenSectionHeader('.sgh_sectionNewsHighlight');
      tweenNews('.sgh_sectionNewsHighlight')
    });

    function tweenProducts (item) {
      var tl = new TimelineLite();
      tl.from(item + ' .thumbnailProductsHighlight .caption .number', 0.5, {y:100, opacity:0})
      .from(item + ' .thumbnailProductsHighlight .caption .titleText', 0.5, {y:100, opacity:0}, '-=0.4')
      .from(item + ' .thumbnailProductsHighlight .caption .desc', 0.2, {y:100, opacity:0}, '-=0.1')
      .from(item + ' .thumbnailProductsHighlight .caption .button', 0.2, {y:100, opacity:0}, '-=0.1')
      .from(item + ' .thumbnailProductsHighlight .imageWrap', 0.6, {x:-200, opacity:0})
      .from(item + ' .thumbnailProductsHighlight .imageWrap .image', 0.6, {x:400}, '-=0.6');
      var scene = new ScrollMagic.Scene({
        triggerElement: item,
        triggerHook: 0.8
      })
      .setTween(tl)
      // .addIndicators()
      .addTo(ScrollMagicController);
      return scene;
    }
    function tweenNews (parent) {
      var tl = new TimelineLite();
      tl.from(parent + ' .sgh_items.isSlider .flickity-viewport', 0.3, {y:100, opacity:0})
      .from(parent + ' .sgh_items.isSlider .flickity-page-dots', 0.3, {y:100, opacity:0});
      var scene = new ScrollMagic.Scene({
        triggerElement: parent + ' .flickity-enabled',
        triggerHook: 1
      })
      .setTween(tl)
      // .addIndicators()
      .addTo(ScrollMagicController);
      return scene;
    }

  </script>
</body>

</html>
