const _html = $('html'),
    _window = $(window),
    ScrollMagicController = new ScrollMagic.Controller();

$(document).ready(function() {
  $('.sgh_footerGoToTop a').click(function (e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 1000)
  });
  $('.thumbnailNews .caption .eqh').matchHeight();
  $('.thumbnailNews .caption > *:nth-child(1)').matchHeight();
  $('.thumbnailNews .caption > *:nth-child(2)').matchHeight();
  $('.thumbnailNews .caption > *:nth-child(3)').matchHeight();
  $('.thumbnailProduct .caption .eqh').matchHeight();
  $('.thumbnailProduct .caption > *:nth-child(1)').matchHeight();
  $('.thumbnailProduct .caption > *:nth-child(2)').matchHeight();
  $('.thumbnailProduct .caption > *:nth-child(3)').matchHeight();
  toggleMainNav();
  formFocus();
  thumbnailImage();
  fixedEditor();
  dropdownDisplayCurrentTextAndIcon();
  customScroll();
});

$(window).on('load', function () {
  thumbnailImage();
})

function dropdownDisplayCurrentTextAndIcon () {
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    if ($(this).hasClass('isNavTab')) {
      $(this).parent().hide()
      $(this).parent().siblings().show()
      $(this).parent().removeClass('active')
      let $currentSyncNavTab = $('.nav-tabs a[data-toggle="tab"][href="' + e.target.hash + '"]')
      $currentSyncNavTab.parent().addClass('active')
      $currentSyncNavTab.parent().siblings().removeClass('active')
    }
    let $button = $('.dropdown.isNavTab .dropdown-toggle');
    let $buttonImage = $button.find('.image');
    let $buttonText = $button.find('.dropdown-toggle-text');
    let text = $(this).find('.titleText').text();
    let image = $(this).find('.image img:first-child').clone();
    $buttonImage.html(image)
    $buttonText.text(text)
  })
}

function fixedEditor () {
  $('.sgh_editor').find('iframe').each(function (index, element) {
    let iframeWidth = $(this).attr('width')
    let iframeRatio = $(this).attr('data-video-ratio')
    $(this).addClass('embed-responsive-item').wrap('<div style="max-width: ' + iframeWidth + 'px; width: 100%; margin: auto;"><div class="embed-responsive embed-responsive-' + iframeRatio + '"></div></div>')
  });
}
function customScroll () {

  let container, instantCustomScroll
  if ($('.mainNavigation').length) {
    container = document.querySelector('.mainNavigation .navItems')
    instantCustomScroll = new PerfectScrollbar(container, {
      suppressScrollX: true
    })
  }
  return instantCustomScroll
}

function toggleMainNav () {
  // var highlightHeight = '';
  // function setHeight () {
  //   highlightHeight = $('.sgh_sectionHighlight').outerHeight();
  //   $('.mainNavigation').css('height', highlightHeight);
  // }
  // $(window).resize(function () {
  //   setHeight();
  // });
  // Main navigation
  $('[data-toggle="mainNavigation"]').click(function(event) {
    event.preventDefault();
    // setHeight()
    // customScroll();
    var $this = $(this);
    if (!_html.hasClass('mainNavigationOpen')) {
      $this.addClass('active');
      _html.addClass('mainNavigationOpen');
    }else {
      $this.removeClass('active');
      _html.removeClass('mainNavigationOpen');
    }
  });
}
function formFocus () {
  let inputContainer = '.form-control-floatText';
  let $input =  $(inputContainer).find('.form-control');
  $input.focusout(function() {
    $(inputContainer).removeClass('focus');
  });
  $input.focus(function() {
    $(this).parents(inputContainer).addClass('focus');
  });
  $input.on('change keydown keyup',function(e) {
    if($(this).val().length > 0){
      $(this).parents(inputContainer).addClass('filled');
    }
    else{
      $(this).parents(inputContainer).removeClass('filled');
    }
  });
  $('.selectpicker').on('show.bs.select', function (e) {
    $(this).parents(inputContainer).addClass('focus');
    $(this).parents(inputContainer).removeClass('filled');
  });
  $('.selectpicker').on('hide.bs.select', function (e) {
    $(this).parents(inputContainer).removeClass('focus');
    if($(this).val().length > 0){
      $(this).parents(inputContainer).addClass('filled');
    }
  });
}
function thumbnailImage() {
  // 1.call script by add attribute to figure element that wrap an image.
  // ========
  // data-crop="16by9"
  // data-crop="16by5"
  // data-crop="4by3"
  // data-crop="1by1"
  // ========
  //
  // (optional) add this attribute below after do first step for fit an image to figure element.
  // data-crop-reverse="true"
  // ========
  let $wrap = ''
  let $img = ''
  $('figure[data-crop]').each(function () {
    $(this).addClass('cropable')
    $img = $(this).find('img')
    let imageRatio = $img.outerWidth()/$img.outerHeight()
    let wrapRatio = $(this).outerWidth()/$(this).outerHeight()
    setTimeout(()=> {
      $(this).addClass('croped')
      if (wrapRatio > imageRatio) {
        $(this).addClass('focusX');
      }
    }, 100);
  });
}
function tweenSectionHeader (parent) {
  var tl = new TimelineLite();
  tl.from(parent + ' .sgh_sectionHeader .titleText', 0.5, {x:100, opacity:0})
  .from(parent + ' .sgh_sectionHeader .desc', 0.5, {x:-100, opacity:0}, '-=0.5')
  .from(parent + ' .sgh_sectionHeader .button', 0.2, {y:-50, opacity:0});
  var scene = new ScrollMagic.Scene({
    triggerElement: parent + ' .sgh_sectionHeader',
    triggerHook: 0.9
  })
  .setTween(tl)
  // .addIndicators()
  .addTo(ScrollMagicController);
  return scene;
}