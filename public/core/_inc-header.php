<!--[if lt IE 8]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class="beforeHeader">
  <div class="container">
    <div class="beforeHeaderInner">
      <ul>
        <li><a href="http://boonrawd.co.th/" target="_blank"><span>BOONRAWD.CO.TH</span></a></li>
        <li><a href="http://www.singhaonlineshop.com/ " target="_blank"><span>SINGHA online shop</span></a></li>
        <li><a href="http://www.singha.com" target="_blank"><span>SINGHA.COM</span></a></li>
      </ul>
    </div>
  </div>
</div>
<div class="headerWrap">
  <div class="container">
    <div class="header">
      <div class="headerBrand">
        <a href="./" class="headerBrandInner"><span><figure class="image"><img src="img/logo-singha.png"></figure></span></a>
        <button class="btn btnHamburger" data-toggle="mainNavigation">
          <span class="btnHamburgerInner">
            <span>button </span>
            <span>toggle </span>
            <span>header </span>
          </span>
          <span class="btnHamburgerText">CLOSE</span>
        </button>           
      </div>
      <nav class="mainNavigation">
        <div class="container">
          <div class="mainNavigationInner">
            <ul class="navItems">
              <?php 
              $navigationItems = array(
                'home',
                'business',
                'products',
                'news',
                'contact');
              $activePage = array(
                'index.php',
                'business',
                'product',
                'news.php',
                'contact.php');
              $navigationLinks = array(
                './',
                '#',
                './products.php',
                './news.php',
                './contact.php');
              $navigationSubItemsBusiness = array(
                'About Us',
                'Representative');
              $navigationSubItemsBusinessLinks = array(
                './about.php',
                './representative.php');                
              $navigationSubItemsProduct = array(
                'BEER & ALCOHOL​',
                'BEVERAGE​',
                'FOOD​',
                'SNACK​',
                'PARTNERSHIP​'
              );
              $navigationSubItemsProductLinks = array(
                './products.php',
                '',
                '',
                '',
                ''
              );
              for ($i=0; $i < count($navigationItems) ; $i++) : ?>
              <li role="presentation" class="navItem <?php echo($page === $activePage[$i]? 'active':''); ?>">
                <?php if ($navigationItems[$i] === 'business'): ?>
                <a href="#navSubCollapseBusiness" data-toggle="collapse"><span><?php echo($navigationItems[$i]) ?></span></a>
                <div class="collapse <?php echo($page === $activePage[$i]? 'in':''); ?>" id="navSubCollapseBusiness">
                  <ul class="navSubItems">
                    <?php for ($ii=0; $ii < count($navigationSubItemsBusiness) ; $ii++) : ?>
                    <li class="navSubItem <?php echo($pageSub === $navigationSubItemsBusinessLinks[$ii]? 'active':''); ?>">
                      <a href="<?php echo($navigationSubItemsBusinessLinks[$ii]); ?>"><span><?php echo($navigationSubItemsBusiness[$ii]); ?></span></a>
                    </li>
                    <?php endfor; ?>
                  </ul>
                </div>
                <?php elseif ($navigationItems[$i] === 'products'): ?>
                <a href="#navSubCollapseProducts" data-toggle="collapse"><span><?php echo($navigationItems[$i]) ?></span></a>
                <div class="collapse <?php echo($page === $activePage[$i]? 'in':''); ?>" id="navSubCollapseProducts">
                  <ul class="navSubItems">
                    <?php for ($ii=0; $ii < count($navigationSubItemsProduct) ; $ii++) : ?>
                    <li class="navSubItem <?php echo($pageSub === $navigationSubItemsProductLinks[$ii]? 'active':''); ?>">
                      <a href="<?php echo($navigationSubItemsProductLinks[$ii]); ?>"><span><?php echo($navigationSubItemsProduct[$ii]); ?></span></a>
                    </li>
                    <?php endfor; ?>
                  </ul>
                </div>
                <?php else : ?>
                <a href="<?php echo($navigationLinks[$i]) ?>"><span><?php echo($navigationItems[$i]) ?></span></a>
                <?php endif; ?>
              </li>      
              <?php endfor; ?>
              <li role="presentation" class="navItem isGlobalNav">
                <a href="http://boonrawd.co.th/" target="_blank"><span>BOONRAWD.CO.TH</span></a>
                <a href="http://www.singhaonlineshop.com/ " target="_blank"><span>SINGHA online shop</span></a>
                <a href="http://www.singha.com" target="_blank"><span>SINGHA.COM</span></a>
              </li>
              <li role="presentation" class="navItem isSocial">
                <a href=""><span><span class="ftlo-facebook"></span></span></a>
                <a href=""><span><span class="ftlo-twitter-bird"></span></span></a>
                <a href=""><span><span class="ftlo-instagram-filled"></span></span></a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </div>
</div>