<!-- globalScriptJS   -->
<script src="js/all-vendor.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- slider cdn -->
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
<script src="js/highlightSlider.js"></script>

<script src="js/scrollmagic/minified/ScrollMagic.min.js"></script>
<script src="js/scrollmagic/minified/plugins/animation.gsap.min.js"></script>
<!-- <script src="js/scrollmagic/minified/plugins/debug.addIndicators.min.js"></script> -->
<script src="js/greensock/TweenMax.min.js"></script>

<!-- perfect scrollbar -->
<script src="js/ps/perfect-scrollbar.min.js"></script>
<link rel="stylesheet" href="js/ps/perfect-scrollbar.css">

<script src="js/main.js"></script>
