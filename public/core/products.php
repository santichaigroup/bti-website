<?php 
  include'_inc-config.php'; 
  $page = 'product';
  $pageSub = './products.php'
?>
<!DOCTYPE html>
<html>
<head>
  <?php include'_inc-docHead.php'; ?>
</head>
<body>
  <?php include'_inc-header.php'; ?>
  <div class="sgh_wrap">
    <div class="sgh_section sgh_sectionHighlight">
      <div class="sgh_sectionInner">
        <div class="sgh_items isSlider">
          <?php for ($i=0; $i < 1 ; $i++) : ?>
          <div class="sgh_item">
            <div class="thumbnailHighlight">
              <div class="imageWrap">
                <style>
                  @media (min-width: 768px) {
                    .sgh_item:nth-child(<?php echo($i) + 1; ?>) .thumbnailHighlight .image {
                      background-image: url(img/highlight-products.png);
                    }
                  }
                </style>
                <div class="image">
                  <img class="visible-xs" src="img/highlightMobile-products.png" alt="">
                </div>
              </div>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_sectionProducts">
      <div class="container">
        <div class="sgh_sectionInner">
          <div class="sgh_sectionHeader text-center">
            <h1 class="titleText">BEERS &amp; ALCOHOLS</h1>
          </div>
          <div class="row">
            <?php 
              $productImages = array(
                'img/dummy-thumbnailProduct-1.png', 
                'img/dummy-thumbnailProduct-2.png', 
                'img/dummy-thumbnailProduct-3.png', 
                'img/dummy-thumbnailProduct-4.png',
                'img/dummy-thumbnailProduct-5.png',
                'img/dummy-thumbnailProduct-6.png',
                'img/dummy-thumbnailProduct-7.png'
              );     
              $productTitles = array(
                'SINGHA BEER',
                'SINGHA LIGHT',
                'LEO BEER',
                'ASAHI',
                'CARLSBERG',
                'CORONA',
                'ASAHI'
              );     
              $productLinks = array(
                './productDetail.php',
                '#',
                '#',
                '#',
                '#',
                '#',
                '#'
              );        
            for ($i=0; $i < count($productImages) ; $i++) : ?>
            <div class="col-sm-4">
              <div class="thumbnailProduct">
                <div class="imageWrap">
                  <a href="<?php echo($productLinks[$i]); ?>"><figure class="image" data-crop="1by1"><img src="<?php echo($productImages[$i]); ?>" alt=""></figure></a>
                </div>
                <div class="captionWrap">
                  <div class="caption">
                    <div class="eqh">
                      <h3 class="titleText"><?php echo($productTitles[$i]); ?></h3>
                      <p class="desc">Consectetur adipiscing elit. Fusce sodales scelerisque bibendum. Nullam sodales tincidunt libero vel euismod Curabitur semper augue id dolor iaculis pulvinar.</p>
                    </div>
                    <div class="button"><a href="<?php echo($productLinks[$i]); ?>" class="btn btnTheme"><span>More Detail <span class="ftlo-right"></span></span></a></div>
                  </div>
                </div>
              </div>
            </div>
            <?php endfor; ?>
          </div>
        </div>
      </div>
    </div>
    <div class="sgh_section sgh_loadmore">
      <div class="sgh_sectionInner">
        <a href="" class="btn btnTheme isHighlight isDown"><span>LOAD MORE <span class="ftlo-down"></span></span></a><div class="container">
      </div>
      </div>
    </div>
  </div>
  <?php include'_inc-footer.php'; ?>




  <?php include'_inc-globalJS.php'; ?>
  <script>
    // define page by classname
    document.querySelector('html').classList.add('productsPage');
  </script>
</body>

</html>
