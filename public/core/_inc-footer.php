<div class="sgh_footer">
  <div class="container">
    <div class="sgh_footerInner">
      <div class="sgh_footerGoToTop">
        <a href="#">
          <span class="ftlo-up"></span>
        </a>
      </div>
      <div class="row">
        <div class="col-md-5 col-sm-6">
          <div class="sgh_footerBrand">
            <a href=""><figure class="image"><img src="img/logo-singha.png"></figure></a>
          </div>
          <div class="sgh_footerAddress">
            <address>
              BOONRAWD TRADING INTERNATIONAL CO., LTD. <br>
              83 Amnuay-Songkran Road,Dusit ,  <br>
              Bangkok 10300 Thailand
            </address>
            <p>Tel: (662) 636-9600​    <br class="visible-xs"> Fax: (662) 669-2089 , 243-1740</p>
            <p><b><a href=""><span>SINGHA CORPORATION MAP</span><span class="ftlo-location"></span></a></b></p>
          </div>
        </div>
        <div class="col-md-7 col-sm-6">
          <div class="footerSocial">
            <div class="footerSocialInner">
              <a href=""><span><span class="ftlo-facebook"></span></span></a>
              <a href=""><span><span class="ftlo-twitter-bird"></span></span></a>
              <a href=""><span><span class="ftlo-instagram-filled"></span></span></a>          
            </div>
          </div>
          <div class="sgh_footerCopyright">
            <span class="sgh_footerCopyrightLinks"><a href="">Terms &amp; Conditions</a>  |  <a href="">Privacy Policy</a>  |  <a href="">Responsibility Statement </a></span>
            <span class="sgh_footerCopyrightText">© COPYRIGHTS 2014 SINGHA CORPORATION CO., LTD. ALL RIGHTS RESERVED.</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>